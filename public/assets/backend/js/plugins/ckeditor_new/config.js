﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	
	// %REMOVE_START%
	// The configuration options below are needed when running CKEditor from source files.
	config.plugins = 'dialogui,dialog,about,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,clipboard,button,panelbutton,panel,floatpanel,colorbutton,colordialog,templates,menu,contextmenu,div,resize,toolbar,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo,font,format,horizontalrule,htmlwriter,iframe,wysiwygarea,image,indent,indentblock,indentlist,smiley,justify,menubutton,language,link,list,liststyle,magicline,pagebreak,pastetext,pastefromword,preview,print,removeformat,selectall,showblocks,showborders,sourcearea,specialchar,scayt,stylescombo,tab,table,tabletools,undo,wsc,autocorrect,bgimage,backgrounds,balloonpanel,lineutils,widget,glyphicons,btbutton,btgrid,bootstrapVisibility,bootstrapTabs,widgetbootstrap,cleanuploader,widgetcommon,cssanim,image2,filetools,floating-tools,fixed,gg,iframedialog,imgbrowse,imagebrowser,imageuploader,symbol,leaflet,letterspacing,lightbox,lineheight,locationmap,oembed,tliyoutube,quicktable,simple-image-browser,notification,notificationaggregator,uploadwidget,uploadimage';
	//config.plugins = 'dialogui,dialog,about,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,clipboard,button,panelbutton,panel,floatpanel,colorbutton,colordialog,templates,menu,contextmenu,div,resize,toolbar,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo,font,forms,format,horizontalrule,htmlwriter,iframe,wysiwygarea,image,indent,indentblock,indentlist,smiley,justify,menubutton,language,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastetext,pastefromword,preview,print,removeformat,save,selectall,showblocks,showborders,sourcearea,specialchar,scayt,stylescombo,tab,table,tabletools,undo,wsc,allmedias,autocorrect,bgimage,backgrounds,balloonpanel,lineutils,widget,glyphicons,btbutton,btgrid,bootstrapVisibility,bootstrapTabs,widgetbootstrap,cleanuploader,widgetcommon,cssanim,image2,filetools,floating-tools,fixed,gg,iframedialog,imgupload,imgbrowse,imagebrowser,imageuploader,symbol,leaflet,letterspacing,lightbox,lineheight,locationmap,oembed,tliyoutube,quicktable,simple-image-browser,sourcedialog,notification,notificationaggregator,uploadwidget,uploadimage';
	config.skin = 'icy_orange';
	// %REMOVE_END%

	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};
