
/*========================================================================= */
/*  Preloader
/* ========================================================================= */

$(window).on("load", function() {
    $("#preloader").fadeOut("slow");

    $(".rsvp").selectbox();

    headerHeight();
    
});

function headerHeight(){
    var navbarHeight = $(".navbar-default").height();
    if (!$(".sticky-header").parent().is(".sticky-head-wrapper")) {
        $(".sticky-header").wrap('<div class="sticky-head-wrapper"></div>').parent().css("height", navbarHeight);
    }
}

$(document).ready(function() {

    // init bootstrap tooltip
    $(".sc_tooltip").tooltip();

    $("body").removeClass("bordered");

	//initiating wow js
    var wow = new WOW({
        mobile:       false
      }
    );
    wow.init();

    // getting window width
    //var windowWidth  = $(window).width();
    var windowWidth =  {
        value: $(window).width()
    }

    $(window).resize(function(event) {
        windowWidth.value = $(window).width();
        canvasResponsive();
        headerHeight();
    });

    //window height
    var bannerHeight = $(window).height();

    //store navigation height
    var navbarHeight = $(".navbar-default").height();

    //calculate window height without nav height
    var fitScreen = bannerHeight-navbarHeight;

    $(window).scroll(function () {
        if ($(window).scrollTop() >= 1) {
            $(".sticky-header").addClass("on");
        }else {
            $(".sticky-header").removeClass("on");
        }
    });

/*=================================================================
    Animated Header
=================================================================*/

    $(window).scroll(function () {
        if ($(window).scrollTop() > 200) {
            $(".navbar-default").addClass("animated");
            $(".slidedown-nav").slideDown();
            $(".page-menu").addClass("page-navbg");
            $(".top-head").slideUp();
            $(".header-mid").slideUp();
            $(".header2 .header-top").slideUp();
            $(".show-top-bar .header-top").slideUp();
            $(".header-top.opened").slideUp();
        } else {
            $(".navbar-default").removeClass('animated');
            $(".slidedown-nav").slideUp();
            $(".page-menu").removeClass("page-navbg");
            $(".top-head").slideDown();
            $(".header-mid").slideDown();
            $(".header2 .header-top").slideDown();
            $(".show-top-bar .header-top").slideDown();
            $(".header-top.opened").slideDown();
        }
    });

    $(".header-widget-toogle").on("click", function() {
        $(".header-widget-inner").slideToggle(600);
    });

    $('.navbar-nav>li>a').each(function(){
        if($(this).next().is('ul')){
            $(this).append('<i class="drpdownSign"></i>');
        }
    });

    $(".navbar-nav > li:not(.mega-menu) > ul li a").each(function() {
        if($(this).next().is("ul")){
            $(this).append('<i class="fa fa-angle-right"></i>');
        }
    });

    // drop down for mobile from 320
    if ( windowWidth.value <= 767 ) {
        $(".navbar-nav li a").each(function(){
            if($(this).next().is("ul")){
                //change the dropdown icon, right arrow to down arrow
                $(this).children(".fa-angle-right").addClass("fa-angle-down");
            }
        });
    }

    $(window).resize(function() {
        if ( windowWidth.value <= 767 ) {
            $(".navbar-nav li a").each(function(){
                if($(this).next().is("ul")){
                    //change the dropdown icon, right arrow to down arrow
                    $(this).children(".fa-angle-right").addClass("fa-angle-down");
                }
            });
        }
    });

    $(".navbar-nav li a").on("click", function(event) {
        if ( windowWidth.value < 768 ) {
            if($(this).next().is("ul")){
                event.preventDefault();
                // slide down menu in mobile on click
                $(this).next("ul").slideToggle(800)
            }
        }
    });

    $(".vertical-nav li>a").each(function(){
        if($(this).next().is("ul")){
            $(this).append("<i class=\"drpdownSign\"></i>");
        }
    });
	
    // nav toggle in tablet
    $(".navbar-toggle2").on('click', function() {
        $(".ac-nav .navbar-nav").slideToggle();
    });
    
    $('body').on("click", ".slide-top .hide-top", function() {
        $(".header-top").slideToggle();
    });

    //apply the height
    $(".fitscreen").css("height", fitScreen);
    $(".fullscreen").css("height", bannerHeight);

    $(window).resize(function() {
        $(".fitscreen").css("height", fitScreen);
        $(".fullscreen").css("height", bannerHeight);
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > fitScreen ) {
            $(".bottom-head").addClass('navbar-fixed-top');
        } else {
            $(".bottom-head").removeClass('navbar-fixed-top');
        }
    });

    // vertical dropdown 
    $(".vertical-nav>ul>li").hover(
        function () {
            $(this).children(".dropdown").stop(true,true).slideDown("medium");
            $(this).children("a").find(".drpdownSign").addClass("opened");
        }, 
        function () {
            $(this).children(".dropdown").stop(true,true).slideUp("medium");
            $(this).children("a").find(".drpdownSign").removeClass("opened");
        }
    );
    $(".vertical-nav .dropdown>li").hover(
        function () {
            $(this).children("ul").stop(true,true).slideDown("medium");
            $(this).children("a").find(".drpdownSign").addClass("opened");
        }, 
        function () {
            $(this).children("ul").stop(true,true).slideUp('medium');
            $(this).children("a").find(".drpdownSign").removeClass("opened");
        }
    );

    if($(window).width() <= 768){
        $(".vertical-selector").removeClass("vertical-inner-content");
    };

    $(".mobile-nav-toggle").on("click", function() {
        $(".vertical-nav").slideToggle("slow");
    });
    
    $(".nav-toggle").on('click', function(event) {
        event.preventDefault();
        $(".navbar-collapse .navbar-nav").slideToggle(400);
    });

    $(".side-nav").on("click", function(event) {
        event.preventDefault();
        $(".slide-menu-sec").toggleClass("opened");
    });

    $(".close-slide-nav").on("click", function(event) {
        event.preventDefault();
        $(".slide-menu-sec").removeClass("opened");
    });

    $(".sidebar-nav ul li").hover(
        function () {
            $(this).children("ul").stop(true,true).slideDown('medium');
        }, 
        function () {
            $(this).children("ul").stop(true,true).slideUp('medium');
        }
    );

    // home slider
    $("#home-carousel").carousel({
        interval: 4000
    });

    $('a.link[href^="#"]').on('click',function (e) {
        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+ $(".navbar-default").height()
        }, 900);
        return 0
    });


/*=================================================================
    home slide / index26
=================================================================*/

    $(".slide26").bxSlider({
        controls: true,
        autoHover: true,
        pager: false,
        touchEnabled: true,
        auto: false,
        onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            console.log(currentSlideHtmlObject);
            $('.slide26 .active').removeClass('active');
            $('.slide26 > .item').eq(currentSlideHtmlObject + 1).addClass('active')
        },
        onSliderLoad: function () {
            $('.slide26 > .item').eq(1).addClass('active')
        }
    });

    /*-------------------------------------------------*/
    /* =  Photography
    /*-------------------------------------------------*/

    //init kenburn slider
    canvasResponsive();

/*=================================================================
    widget-img-slideshow
=================================================================*/

    $(".widget-img-slideshow").bxSlider({
        pager: false,
        autoHover: true,
        touchEnabled: true,
        adaptiveHeight: true,
        auto: true
    });


/*=================================================================
    Accordion
=================================================================*/

    $('.eydia-ac .panel-heading .panel-title a').on('click', function() {
        $('.panel-heading').removeClass('active');
        $(this).parents('.panel-heading').addClass('active');
    });

    $('#faq .panel-heading a').on('click',function(e){
        if($(this).parents('.panel').children('.panel-collapse').hasClass('in')){
            e.preventDefault();
            e.stopPropagation();
        }
    });

/*=================================================================
    Remove Placeholder On click
=================================================================*/

    
    $('input').focusin(function(){
        $('input').data('holder',$(this).attr('placeholder'));
        $(this).attr('placeholder','');
    });
    $('input').focusout(function(){
        $(this).attr('placeholder',$(this).data('holder'));
    });
    $('textarea').focusin(function(){
        $('textarea').data('holder',$(this).attr('placeholder'));
        $(this).attr('placeholder','');
    });
    $('textarea').focusout(function(){
        $(this).attr('placeholder',$(this).data('holder'));
    });

/*=================================================================
    Back to top
=================================================================*/
    
    
    $(window).scroll(function () {
        if ($(window).scrollTop() > 400) {
            $("#go-top").fadeIn(500)
        } else {
            $("#go-top").fadeOut(500)
        }
    });
    $("#go-top").on('click', function () {
        $("html, body").stop().animate({
            scrollTop: 0
        }, 1500, "easeInOutExpo")
    });


    $("#members-order-by").selectbox();

    $(".search-tab-nav .toggle-form").on('click', function() {
        $(".tab-content").slideToggle();
        $(".search-tab-nav").toggleClass("opened");
        $(".toggle-form i").toggleClass("fa-angle-down");
    });

    $(".agency-prtflo").each(function(){
        var bgcolor = $(this).attr("data-hover-color");
        var contentcolor = $(this).attr("data-content-color");
        $(this).find(".portfolio-desc").css({
            "background-color": bgcolor
        });
        $(this).find(".portfolio-desc-inner").css({
            "color": contentcolor
        });
        $(this).find(".portfolio-desc span a").css({
            "color": contentcolor,
            "border-color": contentcolor
        });
    });

});

$(".social_animation a i").on("mouseover",function(){
    $(".social_animation a span").removeClass("current");
    $(".social_animation a").each(function(){
        if($(this).hasClass("prev_color")){
            $(this).children("span").css("z-index", 3);
        }
        else{
            $(this).children("span").css("z-index","1")
        }
    });
    $(this).prev().css({
        "z-index" : 4
    }).addClass("current");

    $(this).prev("span").css({
        "width" : 0,
        "height" : 0,
        "margin" : "auto",
        "background-color": $(this).prev("span").attr("data-color")
    })
   $(this).prev("span").animate({
        "margin-left" : -2500,
        "margin-top" : -2500,
        "width" : 5000,
        "height" : 5000
    },2500)
})

$(".social_animation a i").on("mouseleave",function(){
    $(".social_animation a").removeClass("prev_color");
    $(this).parent().addClass("prev_color");
})

$(".social_animation").on("mouseleave",function(){
    $(".social_animation a span").each(function(){
        if($(this).hasClass("current")){
            $(this).animate({
                "width" : 0,
                "height" : 0,
                "margin" : 0
            },700)
        }else{
           $(this).animate({
                "width" : 0,
                "height" : 0,
                "margin" : 0
            },0) 
        }
    })
});

/* Canvas responsive */
function canvasResponsive() {
    $(".canvas-responsive").attr({
        "width": $(window).width(),
        "height": $(window).innerHeight()
    })
}