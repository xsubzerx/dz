<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;


class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var $resource
     * @return array
     */
    public function transform($user)
    {
        if(!$user)
            return [];

        return [
            'id'            => $user->id,
            'firstname'     => $user->firstname,
            'lastname'      => $user->lastname,
            'email'         => $user->email,
            'phone'         => $user->phone,
            'passport'      => $user->passport,
            'address'       => $user->address,
            'section'       => $user->section,
            'town'          => $user->town,
            'state'         => $user->state,
            'website'       => $user->website,
            'facebook'      => $user->facebook,
            'license_number'=> $user->license_number,
            'license_year'  => $user->license_year,
            'type'          => $user->type,
            'status'        => $user->status,
        ];
    }
}
