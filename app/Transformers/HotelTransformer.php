<?php

namespace App\Transformers;

use App\Models\Hotel;
use App\Models\Setting;
use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;


class HotelTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['city', 'rooms', 'album'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var $resource
     * @return array
     */
    public function transform($hotel)
    {
        if(!$hotel)
            return [];

        return [
            'id'                    => $hotel->id,
            'city_id'               => $hotel->city_id,
            'name_ar'               => $hotel->name_ar,
            'name_fr'               => $hotel->name_fr,
            'description_ar'        => $hotel->description_ar,
            'description_fr'        => $hotel->description_fr,
            'address_ar'            => $hotel->address_ar,
            'address_fr'            => $hotel->address_fr,
            'map'                   => $hotel->map,
            'phone'                 => $hotel->phone,
            'stars'                 => $hotel->stars,
            'nearby_ar'             => $hotel->nearby_ar,
            'nearby_fr'             => $hotel->nearby_fr,
            'amenities'             => $hotel->amenities,
            'childes_phase1_age'    => $hotel->childes_phase1_age,
            'childes_phase1_price'  => $hotel->childes_phase1_price,
            'childes_phase2_age'    => $hotel->childes_phase2_age,
            'childes_phase2_price'  => $hotel->childes_phase2_price,
            'photo'                 => ($hotel->randPhoto) ? route('image', ['hotels', $hotel->randPhoto->photo]): asset('frontend/img/logo.jpg'),
            'min_price'             => $hotel->minPrice() * Setting::find(1)->currency_rate,
            'status'                => $hotel->status,
            'featured'              => $hotel->featured,
            'created_by'            => $hotel->created_by,
            'created_at'            => ($hotel->created_at) ? $hotel->created_at->format('Y-m-d') : '',
            'updated_at'            => ($hotel->updated_at) ? $hotel->updated_at->format('Y-m-d') : '',
        ];
    }

    public function includeCity(Hotel $hotel)
    {
        return $this->item($hotel->city, new CityTransformer);
    }

    public function includeRooms(Hotel $hotel)
    {
        return $this->collection($hotel->rooms, new RoomTransformer);
    }

    public function includeAlbum(Hotel $hotel)
    {
        return $this->collection($hotel->album, new HotelAlbumTransformer);
    }
}
