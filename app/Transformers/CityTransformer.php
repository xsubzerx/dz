<?php

namespace App\Transformers;

use App\Models\City;
use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;


class CityTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['country'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var $resource
     * @return array
     */
    public function transform($city)
    {
        if(!$city)
            return [];

        return [
            'id'        => $city->id,
            'country_id'=> $city->country_id,
            'slug'      => $city->slug,
            'name_ar'   => $city->name_ar,
            'name_fr'   => $city->name_fr,
            'status'    => $city->status,
        ];
    }

    public function includeCountry(City $city)
    {
        return $this->item($city->country, new CountryTransformer);
    }
}
