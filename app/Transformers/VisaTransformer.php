<?php

namespace App\Transformers;

use App\Models\Visa;
use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;


class VisaTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['country', 'documents'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var $resource
     * @return array
     */
    public function transform($visa)
    {
        if(!$visa)
            return [];

        return [
            'id'                => $visa->id,
            'country_id'        => $visa->country_id,
            'title_ar'          => $visa->title_ar,
            'title_fr'          => $visa->title_fr,
            'description_ar'    => $visa->description_ar,
            'description_fr'    => $visa->description_fr,
            'price'             => $visa->price,
            'has_shipping'      => $visa->has_shipping,
            'middle_price'      => $visa->middle_price,
            'east_west_price'   => $visa->east_west_price,
            'south_price'       => $visa->south_price,
            'status'            => $visa->status,
            'featured'          => $visa->featured,
        ];
    }

    public function includeCountry(Visa $visa)
    {
        return $this->item($visa->country, new CountryTransformer);
    }

    public function includeDocuments(Visa $visa)
    {
        return $this->collection($visa->documents, new DocumentTransformer);
    }
}
