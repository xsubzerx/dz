<?php

namespace App\Transformers;

use App\Models\Room;
use App\Models\Setting;
use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;


class RoomTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['hotel'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var $resource
     * @return array
     */
    public function transform($room)
    {
        if(!$room)
            return [];

        return [

            'id'            => $room->id,
            'hotel_id'      => $room->hotel_id,
            'title_ar'      => $room->title_ar,
            'title_fr'      => $room->title_fr,
            'description_ar'=> $room->description_ar,
            'description_fr'=> $room->description_fr,
            'image'         => ($room->photo) ? route('image', ['hotels', $room->photo]): asset('frontend/img/logo.jpg'),
            'adults_count'  => $room->adults_count,
            'childes_count' => $room->childes_count,
            'price'         => $room->price * Setting::find(1)->currency_rate,
        ];
    }

    public function includeHotel(Room $room)
    {
        return $this->item($room->hotel, new HotelTransformer);
    }
}
