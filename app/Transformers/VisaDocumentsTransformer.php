<?php

namespace App\Transformers;

use App\Models\Visa;
use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;


class VisaDocumentsTransformer extends TransformerAbstract
{

	/**
	 * List of resources to automatically include
	 *
	 * @var array
	 */
	protected $defaultIncludes = ['documents'];

    /**
     * Transform object into a generic array
     *
     * @var $resource
     * @return array
     */
    public function transform($visa)
    {
        return [
			'description'   => strip_tags($visa->description),
			'hasShipping'   => ($visa->has_shipping) ? true : false,
			'price'         => $visa->price,
        ];
    }

	public function includeDocuments($visa)
	{
		$documents = [];
		if ($visa->documents) {
			foreach ($visa->documents as $document) {
				$documents[] = [
					'id' 		  => $document->id,
					'title' 	  => $document->title,
					'application' => ($document->application) ? route('image', ['visas', $document->application]) : null,
				];
			}
		}

		return $this->primitive($documents);
	}
}
