<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;


class CountryTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var $resource
     * @return array
     */
    public function transform($country)
    {
        if(!$country)
            return [];
        
        return [
            'id'        => $country->id,
            'slug'      => $country->slug,
            'name_ar'   => $country->name_ar,
            'name_fr'   => $country->name_fr,
            'flag'      => ($country->flag) ? route('image', ['countries', $country->flag]): asset('frontend/img/logo.jpg'),
            'status'    => $country->status,
        ];
    }
}
