<?php

namespace App\Transformers;

use App\Models\Trip;
use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;


class TripTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['country', 'city', 'owner', 'album'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var $resource
     * @return array
     */
    public function transform($trip)
    {
        if(!$trip)
            return [];

        return [
            'id'                    => $trip->id,
            'country_id'            => $trip->country_id,
            'city_id'               => $trip->city_id,
            'title_ar'              => $trip->title_ar,
            'title_fr'              => $trip->title_fr,
            'description_ar'        => $trip->description_ar,
            'description_fr'        => $trip->description_fr,
            'has_ar'                => $trip->has_ar,
            'has_fr'                => $trip->has_fr,
            'has_not_ar'            => $trip->has_not_ar,
            'has_not_fr'            => $trip->has_not_fr,
            'single_price'          => $trip->single_price,
            'double_price'          => $trip->double_price,
            'triple_price'          => $trip->triple_price,
            'quad_price'            => $trip->quad_price,
            'quinary_price'         => $trip->quinary_price,
            'childes_first_price'   => $trip->childes_first_price,
            'childes_second_price'  => $trip->childes_second_price,
            'childes_third_price'   => $trip->childes_third_price,
            'fixed'                 => $trip->fixed,
            'start'                 => $trip->start,
            'end'                   => $trip->end,
            'photo'                 => ($trip->randPhoto) ? route('image', ['trips', $trip->randPhoto->photo]): asset('frontend/img/logo.jpg'),
            'status'                => $trip->status,
            'featured'              => $trip->featured,
            'created_by'            => $trip->created_by,
            'updated_by'            => $trip->updated_by,
            'created_at'            => ($trip->created_at) ? $trip->created_at->format('Y-m-d') : '',
            'updated_at'            => ($trip->updated_at) ? $trip->updated_at->format('Y-m-d') : '',
        ];
    }

    public function includeCity(Trip $trip)
    {
        return $this->item($trip->city, new CityTransformer);
    }

    public function includeCountry(Trip $trip)
    {
        return $this->item($trip->country, new CountryTransformer);
    }

    public function includeOwner(Trip $trip)
    {
        return $this->item($trip->owner, new UserTransformer);
    }

    public function includeAlbum(Trip $trip)
    {
        return $this->collection($trip->album, new TripAlbumTransformer);
    }
}
