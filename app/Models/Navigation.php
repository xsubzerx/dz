<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Navigation extends Model
{
    protected $fillable = [
        'title_ar', 'title_fr', 'parent', 'link', 'order', 'status', 'created_by', 'updated_by'
    ];

    public static $rules = [ 'title_ar' => 'required', 'title_fr' => 'required', 'link' => 'required' ];

    public function childrenItems()
    {
        return $this->hasMany('App\Models\Navigation', 'parent' );
    }

    public function parentItem()
    {
        return $this->belongsTo('App\Models\Navigation', 'parent');
    }

    public function getTitleAttribute()
    {
        return $this->{"title_".\LaraLocale::getCurrentLocale()};
    }
}
