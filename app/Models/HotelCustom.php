<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelCustom extends Model
{
    protected $guarded = ['id'];


    public function getChildesAgesListAttribute()
    {
        return json_decode($this->childes_ages);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
