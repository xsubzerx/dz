<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDocument extends Model
{
    protected $fillable = [ 'request_id', 'document_id', 'document' ];

    protected $rule = [ 'request_id' => 'required', 'document_id' => 'required', 'document' => 'required|image' ];

    public function request()
    {
        return $this->belongsTo('App\Models\UserVisa', 'request_id');
    }

    public function doc()
    {
        return $this->belongsTo('App\Models\Document', 'document_id');
    }
}
