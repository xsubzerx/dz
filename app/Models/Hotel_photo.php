<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotel_photo extends Model
{
    protected $guarded = ['id'];


    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($model)
        {
            if(!empty($model->photo)){
                \File::delete(storage_path('upload/hotels/'.$model->photo));
                \File::delete(storage_path('upload/hotels/thumbnail-'.$model->photo));
                \File::delete(storage_path('upload/hotels/small-'.$model->photo));
                \File::delete(storage_path('upload/hotels/medium-'.$model->photo));
                \File::delete(storage_path('upload/hotels/large-'.$model->photo));
            }
        });
    }
}
