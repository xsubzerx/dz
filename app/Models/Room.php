<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $guarded = ['id'];

    public static function rules()
    {
        $rules = ['adults_count' => 'required|min:1', 'childes_count' => 'required', 'price' => 'required'];
        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $rules['title_'.$localeCode] = 'required';
        }

        return $rules;
    }

    public function getTitleAttribute()
    {
        return $this->{"title_".\LaraLocale::getCurrentLocale()};
    }

    public function getDescriptionAttribute()
    {
        return $this->{"description_".\LaraLocale::getCurrentLocale()};
    }

    public function hotel()
    {
        return $this->belongsTo('App\Models\Hotel');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($model)
        {
            if(!empty($model->image)){
                \File::delete(storage_path('upload/hotels/'.$model->image));
                \File::delete(storage_path('upload/hotels/thumbnail-'.$model->image));
                \File::delete(storage_path('upload/hotels/small-'.$model->image));
                \File::delete(storage_path('upload/hotels/medium-'.$model->image));
                \File::delete(storage_path('upload/hotels/large-'.$model->image));
            }
        });
    }
}
