<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $guarded = ['id'];

    public static function rules(){
        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $rules['site_name_'.$localeCode] = 'required';
        }
        $rules['currency_rate'] = 'required';

        return $rules;
    }

    public static $messages = [
        'site_name_ar.required' => 'اسم الموقع العربي مطلوب!',
        'site_name_en.required' => 'اسم الموقع الإنجليزي مطلوب!',
        //'site_email.required'   => 'بريد الموقع الإلكتروني مطلوب!',
        //'site_email.email'      => 'البريد الإلكتروني غير صحيح!'
    ];

    public function getSiteNameAttribute()
    {
        return $this->{"site_name_".\LaraLocale::getCurrentLocale()};
    }

    public function getSiteMetakeyAttribute()
    {
        return $this->{"site_metakey_".\LaraLocale::getCurrentLocale()};
    }

    public function getSiteMetadescAttribute()
    {
        return $this->{"site_metadesc_".\LaraLocale::getCurrentLocale()};
    }

    public function getSiteAddressAttribute()
    {
        return $this->{"address_".\LaraLocale::getCurrentLocale()};
    }

    public function getContactPageAttribute()
    {
        return $this->{"contact_page_".\LaraLocale::getCurrentLocale()};
    }

    public function getVisasTermsAttribute()
    {
        return $this->{"visas_terms_".\LaraLocale::getCurrentLocale()};
    }

    public function getHotelsTermsAttribute()
    {
        return $this->{"hotels_terms_".\LaraLocale::getCurrentLocale()};
    }

    public function getTripsTermsAttribute()
    {
        return $this->{"trips_terms_".\LaraLocale::getCurrentLocale()};
    }

    public function getFlightsTermsAttribute()
    {
        return $this->{"flights_terms_".\LaraLocale::getCurrentLocale()};
    }

    public function getAgenciesTermsAttribute()
    {
        return $this->{"agencies_terms_".\LaraLocale::getCurrentLocale()};
    }
}
