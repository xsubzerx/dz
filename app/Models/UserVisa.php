<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVisa extends Model
{
    protected $guarded = ['id'];

    protected $rule = [ 'code' => 'required', 'user_id' => 'required', 'visa_id' => 'required' ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function visa()
    {
        return $this->belongsTo('App\Models\Visa');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\UserDocument', 'request_id');
    }

    public function payment()
    {
        return $this->hasOne('App\Models\Payment', 'action_id')->where('action', 'visa');
    }
}
