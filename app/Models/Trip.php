<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $guarded = ['id'];

    public static function rules()
    {
        $rules = [
            'single_price' => 'required', 'double_price' => 'required', 'triple_price' => 'required',
            'childes_first_price' => 'required', 'childes_second_price' => 'required', 'childes_third_price' => 'required'
        ];
        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $rules['title_'.$localeCode] = 'required';
            $rules['description_'.$localeCode] = 'required';
        }

        return $rules;
    }

    public function getTitleAttribute()
    {
        return $this->{"title_".\LaraLocale::getCurrentLocale()};
    }

    public function getDescriptionAttribute()
    {
        return $this->{"description_".\LaraLocale::getCurrentLocale()};
    }

    public function getHasAttribute()
    {
        return $this->{"has_".\LaraLocale::getCurrentLocale()};
    }

    public function getHasNotAttribute()
    {
        return $this->{"has_not_".\LaraLocale::getCurrentLocale()};
    }

    public function getStartDateAttribute()
    {
        if(\LaraLocale::getCurrentLocale() == 'ar')
            return self::dateArabic(strtotime($this->start), 'full');
        elseif(\LaraLocale::getCurrentLocale() == 'fr')
            return self::dateFrench(strtotime($this->start), 'full');

        return $this->start;
    }

    public function getEndDateAttribute()
    {
        if(\LaraLocale::getCurrentLocale() == 'ar')
            return self::dateArabic(strtotime($this->end), 'full');
        elseif(\LaraLocale::getCurrentLocale() == 'fr')
            return self::dateFrench(strtotime($this->end), 'full');

        return $this->end;
    }

    public function getStartDateShortAttribute()
    {
        if(\LaraLocale::getCurrentLocale() == 'ar')
            return self::dateArabic(strtotime($this->start), 'short');
        elseif(\LaraLocale::getCurrentLocale() == 'fr')
            return self::dateFrench(strtotime($this->start), 'short');

        return $this->start;
    }

    public function getEndDateShortAttribute()
    {
        if(\LaraLocale::getCurrentLocale() == 'ar')
            return self::dateArabic(strtotime($this->end), 'short');
        elseif(\LaraLocale::getCurrentLocale() == 'fr')
            return self::dateFrench(strtotime($this->end), 'short');

        return $this->end;
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

    public function randPhoto()
    {
        $featuredImg = $this->hasOne('App\Models\TripPhoto')->where('featured', 1);
        if($featuredImg->first())
            return $featuredImg;
        return $this->hasOne('App\Models\TripPhoto');
    }

    public function album()
    {
        return $this->hasMany('App\Models\TripPhoto');
    }

    protected function dateArabic($date)
    {
        $months = [
            "Jan" => "يناير",
            "Feb" => "فبراير",
            "Mar" => "مارس",
            "Apr" => "أبريل",
            "May" => "مايو",
            "Jun" => "يونيو",
            "Jul" => "يوليو",
            "Aug" => "أغسطس",
            "Sep" => "سبتمبر",
            "Oct" => "أكتوبر",
            "Nov" => "نوفمبر",
            "Dec" => "ديسمبر"
        ];
        $dateArabic = date('d', $date).' '.$months[date('M', $date)].' '.date('Y', $date);
        return $dateArabic;
    }

    protected function dateFrench($date, $format)
    {
        $monthsFull = [
            "Jan" => "janvier",
            "Feb" => "février",
            "Mar" => "mars",
            "Apr" => "avril",
            "May" => "mai",
            "Jun" => "juin",
            "Jul" => "juillet",
            "Aug" => "août",
            "Sep" => "septembre",
            "Oct" => "octobre",
            "Nov" => "novembre",
            "Dec" => "décembre"
        ];

        $monthsShort = [
            "Jan" => "janv",
            "Feb" => "févr",
            "Mar" => "mars",
            "Apr" => "avril",
            "May" => "mai",
            "Jun" => "juin",
            "Jul" => "juil",
            "Aug" => "août",
            "Sep" => "sept",
            "Oct" => "oct",
            "Nov" => "nov",
            "Dec" => "déc"
        ];

        $month = date('M', $date);
        if($format=='full')
            $month = $monthsFull[$month];
        elseif($format=='short')
            $month = $monthsShort[$month];

        $dateFrench = date('d', $date).' '.$month.' '.date('Y', $date);
        return $dateFrench;
    }
}
