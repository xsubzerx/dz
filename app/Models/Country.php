<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'slug', 'name_ar', 'name_fr', 'flag', 'status', 'created_by', 'updated_by'
    ];

    public static $rules = [
        'name_ar' => 'required', 'name_fr' => 'required'
    ];

    public function getNameAttribute()
    {
        return $this->{"name_".\LaraLocale::getCurrentLocale()};
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($model)
        {
            if(\Request::hasFile('flag') && !empty($model->flag)){
                \File::delete(storage_path('upload/countries/'.$model->flag));
                \File::delete(storage_path('upload/countries/thumbnail-'.$model->flag));
                \File::delete(storage_path('upload/countries/small-'.$model->flag));
                \File::delete(storage_path('upload/countries/medium-'.$model->flag));
                \File::delete(storage_path('upload/countries/large-'.$model->flag));
            }
        });
    }

    public function visas()
    {
        return $this->hasMany('App\Models\Visa');
    }

    public function visasList()
    {
        $visas = ['0' => trans('visas.select-visa')];
        $visas += $this->hasMany('App\Models\Visa')->whereStatus('1')->pluck('title_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        return $visas;
    }

    public function trips()
    {
        return $this->hasMany('App\Models\Trip');
    }

    public function cities()
    {
        return $this->hasMany('App\Models\City');
    }
}
