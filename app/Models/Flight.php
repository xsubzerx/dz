<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $guarded = ['id'];

    public function getTripsListAttribute()
    {
        return json_decode($this->trips);
    }

    public function getAdultsListAttribute()
    {
        return json_decode($this->adults);
    }

    public function getChildesListAttribute()
    {
        return json_decode($this->childes);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
