<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $guarded = ['id'];

    public static function rules()
    {
        $rules = ['childes_phase1_age','childes_phase1_price','childes_phase2_age','childes_phase2_price'];
        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $rules['name_'.$localeCode] = 'required';
            $rules['description_'.$localeCode] = 'required';
            $rules['address_'.$localeCode] = 'required';
        }

        return $rules;
    }

    public function getNameAttribute()
    {
        return $this->{"name_".\LaraLocale::getCurrentLocale()};
    }

    public function getDescriptionAttribute()
    {
        return $this->{"description_".\LaraLocale::getCurrentLocale()};
    }

    public function getAddressAttribute()
    {
        return $this->{"address_".\LaraLocale::getCurrentLocale()};
    }

    public function getNearbyAttribute()
    {
        return $this->{"nearby_".\LaraLocale::getCurrentLocale()};
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function randPhoto()
    {
        $featuredImg = $this->hasOne('App\Models\Hotel_photo')->where('featured', 1);
        if($featuredImg->first())
            return $featuredImg;
        return $this->hasOne('App\Models\Hotel_photo');
    }

    public function album()
    {
        return $this->hasMany('App\Models\Hotel_photo');
    }

    public function rooms()
    {
        return $this->hasMany('App\Models\Room');
    }

    public function minPrice()
    {
        return $this->hasOne('App\Models\Room')->min(\DB::raw('CONVERT(SUBSTRING_INDEX(price,"-",-1),UNSIGNED INTEGER)'));
    }

    public function owner()
    {
        $owner = $this->belongsTo('App\Models\User', 'created_by', 'id');
        return ($owner) ? $owner : ['id' => '0', 'firstname' => 'admin', 'lastname' => 'Admin'];
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($model)
        {
            if(!empty($model->image)){
                \File::delete(storage_path('upload/hotels/'.$model->image));
                \File::delete(storage_path('upload/hotels/thumbnail-'.$model->image));
                \File::delete(storage_path('upload/hotels/small-'.$model->image));
                \File::delete(storage_path('upload/hotels/medium-'.$model->image));
                \File::delete(storage_path('upload/hotels/large-'.$model->image));
            }
        });
    }
}
