<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function rules(){
        $rules['name'] = 'required';
        if(\Request::isMethod('post')){
            $rules['email'] = 'required|email|unique:admins';
            $rules['password'] = 'required|min:3';
        }elseif (!empty(\Request::get('password'))){
            $rules['password'] = 'required|min:3';
        }

        return $rules;
    }
}
