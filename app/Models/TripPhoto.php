<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TripPhoto extends Model
{
    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($model)
        {
            if(!empty($model->photo)){
                \File::delete(storage_path('upload/trips/'.$model->photo));
                \File::delete(storage_path('upload/trips/thumbnail-'.$model->photo));
                \File::delete(storage_path('upload/trips/small-'.$model->photo));
                \File::delete(storage_path('upload/trips/medium-'.$model->photo));
                \File::delete(storage_path('upload/trips/large-'.$model->photo));
            }
        });
    }
}
