<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHotel extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function hotel()
    {
        return $this->belongsTo('App\Models\Hotel');
    }

    public function payment()
    {
        return $this->hasOne('App\Models\Payment', 'action_id')->where('action', 'hotel');
    }

    public function setRoomsMembersAttribute($value)
	{
		$this->attributes['rooms_members'] = json_encode($value);
	}

    public function getRoomsMembersAttribute($value)
	{
		return json_decode($value);
	}
}
