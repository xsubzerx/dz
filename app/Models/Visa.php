<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visa extends Model
{
    protected $guarded = ['id'];

    public static $rules = [
        'title_ar' => 'required', 'title_fr' => 'required', 'price' => 'required'
    ];

    public function getTitleAttribute()
    {
        return $this->{"title_".\LaraLocale::getCurrentLocale()};
    }

    public function getDescriptionAttribute()
    {
        return $this->{"description_".\LaraLocale::getCurrentLocale()};
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Document');
    }
}
