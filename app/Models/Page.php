<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    protected $fillable = [
        'slug', 'title_ar', 'title_fr', 'image', 'content_ar', 'content_fr', 'status', 'created_by', 'updated_by'
    ];

    public static $rules = [
        'title_ar' => 'required', 'title_fr' => 'required', 'content_ar' => 'required', 'content_fr' => 'required'
    ];

    protected function getTitleAttribute()
    {
        return $this->{"title_".\LaraLocale::getCurrentLocale()};
    }

    protected function getContentAttribute()
    {
        return $this->{"content_".\LaraLocale::getCurrentLocale()};
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($model)
        {
            if(!empty($model->image)){
                \File::delete(storage_path('upload/pages/'.$model->image));
                \File::delete(storage_path('upload/pages/thumbnail-'.$model->image));
                \File::delete(storage_path('upload/pages/small-'.$model->image));
                \File::delete(storage_path('upload/pages/medium-'.$model->image));
                \File::delete(storage_path('upload/pages/large-'.$model->image));
            }
        });
    }
}
