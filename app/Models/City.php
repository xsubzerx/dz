<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'slug', 'country_id', 'name_ar', 'name_fr', 'status', 'created_by', 'updated_by'
    ];

    public static $rules = [
        'name_ar' => 'required', 'name_fr' => 'required'
    ];

    public function getNameAttribute()
    {
        return $this->{"name_".\LaraLocale::getCurrentLocale()};
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }
}
