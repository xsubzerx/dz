<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    protected $guarded = ['id'];

    public function getTitleAttribute()
    {
        return $this->{"title_".\LaraLocale::getCurrentLocale()};
    }
}
