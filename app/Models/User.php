<?php

namespace App\Models;

use App\Notifications\CustomResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullnameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public static function createRules()
    {
        $rules = [
            'firstname' => 'required|string|max:255',
            'lastname'  => 'required|string|max:255',
            'section'   => 'required|string|max:255',
            'town'      => 'required|string|max:255',
            'state'     => 'required|string|max:255',
            'address'   => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6|confirmed',
        ];

        if(\Request::get('type') == 'agency'){
            $rules['license_number'] = 'required';
            $rules['image'] = 'image';
        }

        return $rules;
    }

    public static function updateRules()
    {
        $rules = [
            'firstname' => 'required|string|max:255',
            'lastname'  => 'required|string|max:255',
            'section'   => 'required|string|max:255',
            'town'      => 'required|string|max:255',
            'state'     => 'required|string|max:255',
            'address'   => 'required|string|max:255',
        ];

        if(\Request::get('type') == 'agency'){
            $rules['license_number'] = 'required';
        }

        if(\Request::has('password') && !empty(\Request::get('password'))){
            $rules['password']  = 'required|string|min:6|confirmed';
        }

        return $rules;
    }

    public static function rulesMsgs()
    {
        return [
            'required' => trans('validation.required', ['attribute' => '']),
        ];
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPassword($token));
    }

    public function agencyTrips()
    {
        return $this->hasMany(Trip::class, 'created_by');
    }
}
