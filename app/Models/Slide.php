<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = [
        'label_ar', 'label_fr', 'image', 'link', 'status', 'created_by', 'updated_by'
    ];

    static protected $rules = [];

    public static function getCreateRules()
    {
        self::$rules['image'] = 'required|image';
        return self::$rules;
    }

    public static function getUpdateRules()
    {
        if(\Request::hasFile('image'))
            self::$rules['image'] = 'required|image';

        return self::$rules;
    }

    public function getLabelAttribute()
    {
        return $this->{"label_".\LaraLocale::getCurrentLocale()};
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($model)
        {
            if(!empty($model->image)){
                \File::delete(storage_path('upload/slides/'.$model->image));
                \File::delete(storage_path('upload/slides/thumbnail-'.$model->image));
                \File::delete(storage_path('upload/slides/small-'.$model->image));
                \File::delete(storage_path('upload/slides/medium-'.$model->image));
                \File::delete(storage_path('upload/slides/large-'.$model->image));
            }
        });
    }
}
