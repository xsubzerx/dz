<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'visa_id', 'title_ar', 'title_fr', 'has_app', 'application', 'created_by', 'updated_by'
    ];

    static protected $rules = [
        'title_ar' => 'required', 'title_fr' => 'required'
    ];

    public static function rules()
    {
        if(\Request::has('has_app') && \Request::get('has_app') == 1)
            self::$rules['image'] = 'required';
        return self::$rules;
    }

    public function visa()
    {
        return $this->belongsTo('App\Models\Visa', 'visa_id');
    }

    public function getTitleAttribute()
{
    return $this->{"title_".\LaraLocale::getCurrentLocale()};
}

    protected static function boot()
    {
        parent::boot();

        static::saved(function($model)
        {
            if($model->id){
                if(\Request::hasFile('image') && !empty($model->application)){
                    \File::delete(storage_path('upload/visas/'.$model->application));
                    \File::delete(storage_path('upload/visas/thumbnail-'.$model->application));
                    \File::delete(storage_path('upload/visas/small-'.$model->application));
                    \File::delete(storage_path('upload/visas/medium-'.$model->application));
                    \File::delete(storage_path('upload/visas/large-'.$model->application));
                }
            }
        });

        static::deleted(function ($model)
        {
            if(\Request::hasFile('image') && !empty($model->application)){
                \File::delete(storage_path('upload/visas/'.$model->application));
                \File::delete(storage_path('upload/visas/thumbnail-'.$model->application));
                \File::delete(storage_path('upload/visas/small-'.$model->application));
                \File::delete(storage_path('upload/visas/medium-'.$model->application));
                \File::delete(storage_path('upload/visas/large-'.$model->application));
            }
        });
    }
}
