<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTrip extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function trip()
    {
        return $this->belongsTo('App\Models\Trip');
    }

    public function payment()
    {
        return $this->hasOne('App\Models\Payment', 'action_id')->where('action', 'trip');
    }
}
