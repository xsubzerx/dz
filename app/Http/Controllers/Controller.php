<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Navigation;
use App\Models\Setting;
use App\Models\Slide;
use App\Models\UserHotel;
use App\Models\UserTrip;
use App\Models\UserVisa;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->getConfig();
        $this->getNav();
        $this->getBanner();
        $this->getCountries();

        $this->middleware(function ($request, $next) {
            $this->alertToPay();
            return $next($request);
        });
    }

    function uploadTemp(Request $request)
    {
        $inputName = key($request->all());
        $inputParts = explode('@', $inputName);
        $destinationPath = public_path('tempUpload/visa_'.$inputParts[1]);
        $extension = $request->file($inputName)->getClientOriginalExtension();
        $fileName = str_replace('file-', '', $inputParts[0]).'.'.$extension;
        \File::makeDirectory($destinationPath, $mode = 0777, true, true);
        $request->file($inputName)->move($destinationPath, $fileName);
        return $fileName;
    }

    function upload($request, $dir){
        $destinationPath = storage_path('upload/'.$dir);
        $extension = $request->file('image')->getClientOriginalExtension();
        $fileName = $dir."_".time().'.'.$extension;
        $request->file('image')->move($destinationPath, $fileName);
        if(in_array(strtolower($extension), ['jpeg', 'jpg', 'png', 'gif'])){
            \Image::make($destinationPath."/".$fileName)->resize(150, 150)->blur()->save($destinationPath."/thumbnail-".$fileName);
            \Image::make($destinationPath."/".$fileName)->resize(250, 250)->blur()->save($destinationPath."/small-".$fileName);
            \Image::make($destinationPath."/".$fileName)->resize(400, 350)->blur()->save($destinationPath."/medium-".$fileName);
            \Image::make($destinationPath."/".$fileName)->resize(800, 600)->blur()->save($destinationPath."/large-".$fileName);
        }
        return $fileName;
    }

    function getImage($dir, $filename)
    {
        $path = storage_path('upload/'.$dir.'/'.$filename);
        if(!\File::exists($path)){
            $path = 'assets/frontend/img/logo.jpg';
            //abort(404);
        }
        $file = \File::get($path);
        $type = \File::mimeType($path);
        return \Response::make($file, 200)->header("Content-Type", $type);
    }

    protected function getConfig()
    {
        return view()->share('config', Setting::find(1));
    }

    protected function getNav()
    {
        return view()->composer('frontend.common.partials._nav', function($view) {
            $view->with('mainNav', Navigation::whereStatus('1')->whereParent('0')->orderBy('order', 'ASC')->get());
            if (auth()->guard('user')->check()){
                //$view->with('notifications', Notification::where('user_id', auth()->guard('user')->user()->id)->where('status', '0')->take(5)->orderBy('id', 'DESC')->get());
            }
        });
    }

    protected function getBanner()
    {
        return view()->composer('frontend.common.partials._slider', function($view) {
            $view->with('banner', Slide::whereStatus('1')->orderBy('id', 'ASC')->get());
        });
    }

    protected function getCountries()
    {
        return view()->share('countries', Country::whereStatus('1')->orderBy('name_'.\LaraLocale::getCurrentLocale())->get());
    }

    protected function alertToPay()
    {
        if(\auth()->guard('user')->check() && \auth()->guard('user')->user()->type == 'user'){
            $hotels = UserHotel::where('user_id', \auth()->guard('user')->user()->id)->whereDoesntHave('payment')->count();
            $visas = UserVisa::where('user_id', \auth()->guard('user')->user()->id)->whereDoesntHave('payment')->count();
            $trips = UserTrip::where('user_id', \auth()->guard('user')->user()->id)->whereDoesntHave('payment')->count();

            if(($hotels + $visas + $trips) > 0)
                return view()->share('alertToPay', true);
            return view()->share('alertToPay', false);
        }
        return view()->share('alertToPay', false);
    }
}
