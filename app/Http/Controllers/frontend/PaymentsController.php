<?php

namespace App\Http\Controllers\frontend;

use App\Models\Payment;
use App\Models\UserHotel;
use App\Models\UserTrip;
use App\Models\UserVisa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{
    public function paymentUpload($action, $id)
    {
        $checkExist = Payment::where('action', $action)->where('action_id', $id)->first();
        if($checkExist && $checkExist->payment)
            return redirect(route('image', ['payments', $checkExist->payment]));

        $getAction = [];
        if($action == 'visa') {
            $query = UserVisa::where('user_id', auth()->guard('user')->user()->id)->where('id', $id)->first();
            $getAction = [
                'code'      => $query->code,
                'action'    => $action,
                'action_id' => $id,
                'title'     => trans('visas.request-action', ['visa' => $query->visa->country->name.' - '.$query->visa->title]),
                'count'     => $query->count,
                'price'     => $query->total,
                'created_at'=> $query->created_at
            ];
        }elseif($action == 'hotel') {
            $query = UserHotel::where('user_id', auth()->guard('user')->user()->id)->where('id', $id)->first();
            $getAction = [
                'code'      => $query->code,
                'action'    => $action,
                'action_id' => $id,
                'title'     => trans('hotels.request-action', ['hotel' => $query->hotel->name]),
                'count'     => array_sum(array_values(json_decode($query->rooms, true))).' '.trans('hotels.rooms'),
                'price'     => $query->total,
                'created_at'=> $query->created_at
            ];
        }elseif($action == 'flight') {
            $getAction = [];
        }elseif($action == 'trip') {
            $query = UserTrip::where('user_id', auth()->guard('user')->user()->id)->where('id', $id)->first();
            $getAction = [
                'code'      => $query->code,
                'action'    => $action,
                'action_id' => $id,
                'title'     => trans('trips.request-action', ['trip' => $query->trip->title]),
                'price'     => $query->total,
                'created_at'=> $query->created_at
            ];
        }

        return view('frontend.payments.upload', compact('getAction'));
    }

    public function paymentUploadHandle(Request $request)
    {
        $this->validate($request, ['image' => 'required'], ['image.required' => trans('validation.required', ['attribute' => trans('common.payment-reset')])]);

        $payment = new Payment();
        $payment->action = $request->action;
        $payment->action_id = $request->action_id;
        $payment->payment = $this->upload($request, 'payments');
        if($payment->save()) {
            if($request->action == 'visa'){
                UserVisa::where('id', $request->action_id)->update(['status' => 'progress']);
                return redirect(route('site.my-visas'));
            }elseif($request->action == 'hotel'){
                UserHotel::where('id', $request->action_id)->update(['status' => 'progress']);
                return redirect(route('site.my-hotels'));
            }elseif($request->action == 'trip'){
                UserTrip::where('id', $request->action_id)->update(['status' => 'progress']);
                return redirect(route('site.my-trips'));
            }elseif($request->action == 'flight'){

            }
        }

        return back()->with('msg', trans('common.update-failed-alert'));
    }
}
