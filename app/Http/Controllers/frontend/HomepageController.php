<?php

namespace App\Http\Controllers\frontend;

use App\Models\City;
use App\Models\Contact;
use App\Models\Country;
use App\Models\Hotel;
use App\Models\Page;
use App\Models\Trip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomepageController extends Controller
{
    public function index()
    {
        $visaCountries = Country::whereStatus(1)->whereHas('visas', function ($q){
            $q->whereStatus(1)->whereFeatured(1);
        })->get();
        $featuredHotels = Hotel::whereStatus(1)->whereFeatured(1)->take(4)->get();
        $featuredTrips = Trip::where(function ($query) {
            $query->where('start', '>=', date('Y-m-d'))->whereFixed('0')->whereStatus(1)->whereFeatured(1);
        })->orWhere(function ($query) {
            $query->whereFixed('1')->whereStatus(1)->whereFeatured(1);
        })->take(4)->get();
        return view('frontend.home', compact('visaCountries', 'featuredHotels', 'featuredTrips'));
    }

    public function showPage($slug)
    {
        $pageData = Page::whereSlug($slug)->firstOrFail();
        return view('frontend.page', compact('pageData'));
    }

    public function showContactUS()
    {
        return view('frontend.contact');
    }

    public function handleContactUS(Request $request)
    {
        $this->validate($request, ['sender' => 'required', 'email' => 'required|email', 'subject' => 'required', 'message' => 'required']);

        $contact = new Contact;
        if($contact->create($request->only(['sender','email','phone','subject','message'])))
            return back()->with('msg', trans('contact.sent-success'));
        return back()->with('msg', trans('contact.sent-fail'));
    }

    public function getCountryCities($country, $currCity='')
    {
        $citiesList = '<option value="">'.trans('common.all').'</option>';
        $country = Country::where('slug', $country)->first();

        if(count($country->cities)){
            foreach ($country->cities as $city){
                if(!empty($currCity) && $currCity == $city->slug)
                    $citiesList .= '<option value="'.$city->slug.'" selected="selected">'.$city->name.'</option>';
                else
                    $citiesList .= '<option value="'.$city->slug.'">'.$city->name.'</option>';
            }
        }
        return  response()->json(compact('citiesList'));
    }

    public function getCountryCitiesById($country, $currCity='')
    {
        $citiesList = '<option value="">'.trans('common.all').'</option>';
        $country = Country::find($country);

        if(count($country->cities)){
            foreach ($country->cities as $city){
                if(!empty($currCity) && $currCity == $city->id)
                    $citiesList .= '<option value="'.$city->id.'" selected="selected">'.$city->name.'</option>';
                else
                    $citiesList .= '<option value="'.$city->id.'">'.$city->name.'</option>';
            }
        }
        return  response()->json(compact('citiesList'));
    }

    public function countriesAPI()
	{
		$countries = Country::select('id', 'slug', 'name_fr', 'name_ar')->whereStatus('1')->get();
		return response()->json($countries);
	}

    public function citiesAPI($country)
	{
		$cities = City::select('id', 'slug', 'name_fr', 'name_ar')->whereStatus('1')->where('country_id', $country)->get();
		return response()->json($cities);
	}
}
