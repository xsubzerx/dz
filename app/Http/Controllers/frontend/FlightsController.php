<?php

namespace App\Http\Controllers\frontend;

use App\Models\Flight;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FlightsController extends Controller
{
    public function index()
    {
        return view('frontend.flight');
    }

    public function booking(Request $request)
    {
        $this->validate($request, ['type' => 'required', 'adults_count' => 'required', 'from' => 'required','to' => 'required','checkin' => 'required']);

        $inputs = $request->only(['type', 'direct', 'adults_count', 'childes_count', 'note']);
        if($request->type == 'one-way'){
            $inputs['trips'] = json_encode(['from' => $request->from, 'to' =>$request->to, 'checkin' => $request->checkin]);
        }elseif($request->type == 'return'){
            $inputs['trips'] = json_encode(['from' => $request->from, 'to' =>$request->to, 'checkin' => $request->checkin, 'checkout' => $request->checkout]);
        }elseif($request->type == 'multiple' && count($request->from)){
            $trips = [];
            foreach ($request->from as $k=>$fr){
                if(isset($request->to[$k]) && isset($request->checkin[$k]))
                    $trips[] = ['from' => $fr, 'to' => @$request->to[$k], 'checkin' => @$request->checkin[$k]];
            }
            $inputs['trips'] = json_encode($trips);
        }

        $inputs['adults'] = json_encode(array_combine($request->adults_title, $request->adults_name));
        $inputs['user_id'] = auth()->guard('user')->user()->id;

        if($request->childes_count > 0 && $request->has('childes_birthdate')){
            $inputs['childes'] = json_encode($request->childes_birthdate);
        }
        $inputs['code'] = time().auth()->guard('user')->user()->id;

        $flight = Flight::create($inputs);
        if($flight){
            Flight::where('id', $flight->id)->update(['code' => 'FL-'.$flight->id]);
            $inputs['code'] = time().auth()->guard('user')->user()->id;
            return view('frontend.thanks-page', ['msg' => trans('flights.request-success')]);
        }
        return back()->with('msg', trans('common.request-error'));
    }

    public function myFlightsRequests()
    {
        $requests = Flight::where('user_id', auth()->guard('user')->user()->id)->get();
        return view('frontend.users.flights', compact('requests'));
    }

    public function show($id)
    {
        $flight = Flight::where('user_id', auth()->guard('user')->user()->id)->where('id', $id)->first();
        return view('frontend.users.flight', compact('flight'));
    }
}
