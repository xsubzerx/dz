<?php

namespace App\Http\Controllers\frontend;

use App\Models\Country;
use App\Models\UserDocument;
use App\Models\UserVisa;
use App\Models\Visa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisasController extends Controller
{
    public function index()
    {
        $allVisas = Country::whereStatus(1)->whereHas('visas', function ($q){
            $q->whereStatus(1);
        })->paginate(16);
        return view('frontend.visas.list', compact('allVisas'));
    }

    public function getCountryVisas($country)
    {
        $country = Country::whereSlug($country)->firstOrFail();
        $title = trans('visas.request') . ': ' . $country->name;
        return view('frontend.visas.visa', compact('country', 'title'));
    }

    public function getVisaDocs(Request $request)
    {
        $visa = Visa::findOrFail($request->visa);
        return response([
            'documents'     => view('frontend.common.partials._documents', ['documents' => $visa->documents, 'count' => $request->count])->render(),
            'description'   => '<p>'.$visa->description.'</p><div class="divider margin5 hr6"><span></span></div>',
            'shipping'      => ($visa->has_shipping) ? '<h5>'.trans('visas.shipping-info').'</h5><div class="divider margin5 hr4 hr_'.\LaraLocale::getCurrentLocale().'"><span></span></div><div class="form-group col-md-6 col-sm-12"><label for="state">'.trans('users.state').'</label>'.\Form::select('state', ['' => trans('visas.select-state')]+trans('visas.middle.states')+trans('visas.east-west.states')+trans('visas.south.states'), null, ['id' => 'state', 'class' => 'form-control', 'required']).'</div><div class="form-group col-md-6 col-sm-12"><label for="address">'.trans('visas.address').'</label>'.\Form::text('address', null, ['id'=>'address', 'class'=>'form-control', 'required']).'</div>' : '',
            'is_logged_in'  => auth()->guard('user')->check(),
            'total'         => $visa->price * $request->count,
            'ids'           => $visa->documents()->pluck('id')
        ], 200);
    }

    public function getVisaPrice(Request $request)
    {
        $price = 0;
        $visa = Visa::findOrFail($request->visa);
        $price = $visa->price * $request->count;
        if($request->has('state')){
            if(array_key_exists($request->state, trans('visas.middle.states')))
                $price += $visa->middle_price;
            elseif(array_key_exists($request->state, trans('visas.east-west.states')))
                $price += $visa->east_west_price;
            elseif(array_key_exists($request->state, trans('visas.south.states')))
                $price += $visa->south_price;
        }
        return response(compact('price'), 200);
    }

    public function visaRequest(Request $request)
    {
        if($request->has('visa') && $request->visa > 0 && $request->has('code')) {
            $visaRequest = new UserVisa;
            $visaRequest->code = time().auth()->guard('user')->user()->id;
            $visaRequest->user_id = auth()->guard('user')->user()->id;
            $visaRequest->visa_id = $request->visa;
            $visaRequest->count = $request->count;
            $visaRequest->total = $request->total;
            if($request->has('state'))
                $visaRequest->shipping_state = $request->state;
            if($request->has('address'))
                $visaRequest->shipping_address = $request->address;
            $visaRequest->save();
            $bookingID = $visaRequest->id;
            if(\File::isDirectory(public_path('tempUpload/visa_'.$request->code))){
                $files = \File::allFiles(public_path('tempUpload/visa_'.$request->code));
                foreach ($files as $file){
                    $fileExtention = '.'.pathinfo($file, PATHINFO_EXTENSION);
                    $fileParams = explode('-', str_replace($fileExtention, '', $file));
                    $fileName = rand(11111,99999).$fileExtention;
                    if($visaRequest->documents()->save(new UserDocument(['document_id' => $fileParams[1], 'document' => $fileName]))){
                        \File::move($file, storage_path('/upload/visas/'.$fileName));
                    }
                }
                \File::deleteDirectory(public_path('tempUpload/visa_'.$request->code));
            }
            UserVisa::where('id', $bookingID)->update(['code' => 'VS-'.$bookingID]);
            return view('frontend.thanks-page', ['msg' => trans('visas.request-success', ['link' => route('site.payments.upload', ['visa', $bookingID])]), 'country' => $visaRequest->visa->country]);
        }else{
            return back()->with('msg', trans('common.request-error'));
        }
    }

    public function myVisasRequests()
    {
        $requests = UserVisa::where('user_id', auth()->guard('user')->user()->id)->get();
        return view('frontend.users.visas', compact('requests'));
    }
}
