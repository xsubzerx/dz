<?php

namespace App\Http\Controllers\frontend;

use App\Models\Country;
use App\Models\Visa;
use App\Transformers\CountryTransformer;
use App\Transformers\VisaDocumentsTransformer;
use App\Transformers\VisaTransformer;
use Cyvelnet\Laravel5Fractal\Facades\Fractal;
use App\Http\Controllers\Controller;

class VisasApiController extends Controller
{
    public function index()
    {
        $allVisas = Country::whereStatus(1)->whereHas('visas', function ($q){
            $q->whereStatus(1);
        })->get();

        return Fractal::collection($allVisas, new CountryTransformer);
    }

    public function show(Visa $visa)
    {
        return Fractal::includes(['country', 'documents'])->item($visa, new VisaTransformer);
    }

    public function getCountryVisas($country)
    {
        $country = Country::whereSlug($country)->firstOrFail();
        return response()->json($country->visas, 200);
    }

	public function getVisaDocs($id)
	{
		return Fractal::item(Visa::findOrFail($id), new VisaDocumentsTransformer());
	}
}
