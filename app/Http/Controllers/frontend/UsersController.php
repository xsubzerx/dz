<?php

namespace App\Http\Controllers\frontend;

use App\Models\City;
use App\Models\Country;
use App\Models\Trip;
use App\Models\TripPhoto;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function profile()
    {
        $years = [];
        foreach(range(date('Y'), 1950) as $yr){ $years[$yr] = $yr; }
        return view('frontend.users.profile', compact('years'));
    }

    public function profileHandle(Request $request)
    {
        $this->validate($request, User::updateRules(), User::rulesMsgs());

        $inputs = $request->only('firstname', 'lastname', 'phone', 'address', 'state');

        if(auth()->guard('user')->user()->type == 'agency'){
            $inputs['section'] = $request->section;
            $inputs['town'] = $request->town;
            $inputs['website'] = $request->website;
            $inputs['facebook'] = $request->facebook;
            $inputs['license_number'] = $request->license_number;
            $inputs['license_year'] = $request->license_year;
        }else{
            $inputs['passport'] = $request->passport;
        }

        $inputs['updated_by'] = auth()->guard('user')->user()->id;

        if($request->has('password') && !empty($request->password))
            $inputs['password'] = bcrypt($request->password);

        $user = User::find(auth()->guard('user')->user()->id);
        if($user->update($inputs))
            return back()->with('msg', trans('common.update-success-alert'));

        return back()->with('msg', trans('common.update-failed-alert'));
    }

    public function trips()
    {
        return view('frontend.agencies.list');
    }

    public function createTrip(Request $request)
    {
        $title = trans('trips.create-trip');
        if($request->isMethod('post')){
            $this->validate($request, Trip::rules());

            $inputs = $this->getInputs($request);
            $inputs['created_by'] = auth()->guard('user')->user()->id;

            if(Trip::insert($inputs))
                return redirect(route('agency.trips'))->with('msg', trans('common.add-success'));
            return back()->with('msg', trans('common.add-failed'));
        }
        $countriesList = Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        return view('frontend.agencies.form', compact('title', 'countriesList'));
    }

    public function editTrip(Request $request, $id)
    {
        $title = trans('trips.update-trip');

        if($request->isMethod('put')){
            $this->validate($request, Trip::rules());

            $inputs = $this->getInputs($request);
            $inputs['updated_by'] = auth()->guard('user')->user()->id;

            $trip = Trip::find($id);
            if($trip->update($inputs))
                return redirect(route('agency.trips'))->with('msg', trans('common.update-success'));
            return redirect(route('agency.trips'))->with('msg', trans('common.update-failed'));
        }
        $trip = Trip::find($id);
        $countriesList = Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        return view('frontend.agencies.form', compact('title', 'trip', 'countriesList'));
    }

    public function destroyTrip($id)
    {
        if(Trip::whereId($id)->where('created_by', auth()->guard('user')->user()->id)->delete())
            return back()->with('msg', trans('common.delete-success-alert'));
        return back()->with('msg', trans('common.delete-failed-alert'));
    }

    public function getInputs($request)
    {
        $inputs = $request->only([
            'country_id', 'city_id', 'fixed', 'start', 'end', 'status', 'featured', 'single_price', 'double_price', 'commission',
            'triple_price', 'quad_price', 'quinary_price', 'childes_first_price', 'childes_second_price', 'childes_third_price'
        ]);

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['title_'.$localeCode] = $request->get('title_'.$localeCode);
            $inputs['description_'.$localeCode] = $request->get('description_'.$localeCode);
            $inputs['has_'.$localeCode] = $request->get('has_'.$localeCode);
            $inputs['has_not_'.$localeCode] = $request->get('has_not_'.$localeCode);
        }
        return $inputs;
    }

    public function tripAlbum(Request $request, $tID)
    {
        if($request->isMethod('post')){
            $imgName = $this->upload($request, 'trips');
            if($imgName){
                $photo = new TripPhoto;
                $photo->create(['trip_id' => $tID, 'photo' => $imgName]);
            }
        }elseif($request->ajax()){
            $photos = [];
            $album = TripPhoto::where('trip_id', $tID)->get();
            foreach ($album as  $ph){
                $photos[$ph->photo] = ['size' => \Image::make(storage_path('upload/trips/'.$ph->photo))->filesize(), 'featured' => $ph->featured];
            }
            return response()->json($photos);
        }
    }

    public function destroyPhoto($photo)
    {
        if(TripPhoto::where('photo', $photo)->delete()){
            \File::delete(storage_path('upload/trips/'.$photo));
            \File::delete(storage_path('upload/trips/thumbnail-'.$photo));
            \File::delete(storage_path('upload/trips/small-'.$photo));
            \File::delete(storage_path('upload/trips/medium-'.$photo));
            \File::delete(storage_path('upload/trips/large-'.$photo));
        }
    }

    public function makeFeatured($photo)
    {
        if(\DB::table('trip_photos')->where('photo', $photo)->update(['featured' => 1])){
            \DB::table('trip_photos')->where('photo', '!=', $photo)->update(['featured' => 0]);
            return response(['success' => true], 200);
        }
        return response(['success' => false], 200);
    }
}
