<?php

namespace App\Http\Controllers\frontend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('user-guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('frontend.auth.login');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if($this->guard()->user()->status) {
            return $this->authenticated($request, $this->guard()->user()) ?: redirect()->intended($this->redirectPath());
        }

        $this->guard()->logout();
        return redirect(route('login'))->with('msg', trans('auth.inactive-account'));
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('user');
    }

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
	 *
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function loginApi(Request $request)
	{
		$this->validateLogin($request);

		try {
			$http = new \GuzzleHttp\Client();
			$response = $http->post(config('app.url').'/oauth/token', [
				'form_params' => $this->getLoginInputs($request)
			]);

			return json_decode((string) $response->getBody(), true);
		} catch (\Exception $e) {
			return response()->json(['success' => false, 'msg' => trans('auth.failed')], 400);
		}
	}

	public function refreshTokenApi(Request $request)
	{
		$request->validate(['refresh_token' => 'required|string']);

		try {
			$http = new \GuzzleHttp\Client;
			$response = $http->post(config('app.url').'/oauth/token', [
				'form_params' => $this->getLoginInputs($request, 'refresh_token')
			]);
			return json_decode((string) $response->getBody(), true);
		} catch (\Exception $e) {
			return response()->json(['success' => false, 'msg' => trans('auth.failed')], 400);
		}
	}

	public function logoutApi()
	{
		$user = auth()->guard('api')->user();

		if ($user) {
			foreach ($user->tokens() as $token) {
				$token->revoke();
			}
			return response()->json(['success' => true, 'msg' => '']);
		}

		return response()->json(['success' => false, 'msg' => trans('common.error')], 400);
	}

	private function getLoginInputs($request, $grantType = 'password')
	{
		$client = Client::where('password_client', 1)->first();

		$data = [
			'grant_type'    => $grantType,
			'client_id'     => $client->id,
			'client_secret' => $client->secret,
		];

		if ($grantType == 'password') {
			$data['username'] = $request->email;
			$data['password'] = $request->password;
		} else {
			$data['refresh_token'] = $request->refresh_token;
		}

		return $data;
	}
}
