<?php

namespace App\Http\Controllers\frontend\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $agency = false;
        foreach(range(date('Y'), 1950) as $yr){ $years[$yr] = $yr; }
        return view('frontend.auth.register', compact('agency', 'years'));
    }

    public function showAgencyRegistrationForm()
    {
        $agency = true;
        foreach(range(date('Y'), 1950) as $yr){ $years[$yr] = $yr; }
        return view('frontend.auth.register', compact('agency', 'years'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, User::createRules(), User::rulesMsgs());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create($data)
    {
        $inputs = [
            'firstname'     => $data->firstname,
            'lastname'      => $data->lastname,
            'email'         => $data->email,
            'password'      => bcrypt($data->password),
            'phone'         => $data->phone,
            'section'       => $data->section,
            'town'          => $data->town,
            'address'       => $data->address,
            'state'         => $data->state,
            'type'          => $data->type,
            'created_by'    => 0,
        ];

        if($data['type'] == 'user'){
            $inputs['passport'] = $data->passport;
            $inputs['status']   = 1;
        }else{
            $inputs['website']        = $data->website;
            $inputs['facebook']       = $data->facebook;
            $inputs['license_number'] = $data->license_number;
            $inputs['license_year']   = $data->license_year;
            $inputs['status']         = 0;

            if($data->hasFile('image')){
				$inputs['photo'] = $this->upload($data, 'users');
			}
        }
        return User::create($inputs);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request)));

        if($request->type != 'user')
            return $this->registered($request, $user) ?: redirect($this->redirectPath())
                ->with('msg', trans('auth.register-confirm-msg'));
        $this->guard()->login($user);
        return $this->registered($request, $user) ?: redirect($this->redirectPath());

    }

    public function registerApi(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ? response()->json(['success' => false], 200) : response()->json(['success' => true], 200);

    }

}
