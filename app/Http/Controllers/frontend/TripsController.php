<?php

namespace App\Http\Controllers\frontend;

use App\Models\Country;
use App\Models\Trip;
use App\Models\UserTrip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TripsController extends Controller
{
    public function index()
    {
        $trips = Trip::where(function ($query) {
            $query->whereFixed('0')->whereStatus(1);
            if(\Request::has('country')){
                if(\Request::has('city')){
                    $query->whereHas('city', function ($q){ $q->where('slug', \Request::get('city')); });
                }else{
                    $query->whereHas('country', function ($q){ $q->where('slug', \Request::get('country')); });
                }
            }
            if(\Request::has('date')){
                $date = explode('-', \Request::get('date'));
                $query->whereMonth('start', $date[1])->whereYear('start', $date[0]);
            }else{
                $query->where('start', '>=', date('Y-m-d'));
            }
        })->orWhere(function ($query) {
            $query->whereFixed('1')->whereStatus(1);
            if(\Request::has('country')){
                if(\Request::has('city')){
                    $query->whereHas('city', function ($q){ $q->where('slug', \Request::get('city')); });
                }else{
                    $query->whereHas('country', function ($q){ $q->where('slug', \Request::get('country')); });
                }
            }
        })->paginate(12);

        $countries = ['' => trans('common.all')];
        $countries += Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'slug')->all();

        $lastTripDate = Trip::where('start', '>=', date('Y-m-d'))->whereFixed('0')->whereStatus(1)->orderBy('end', 'desc')->first();
        $dates = ['' => trans('common.all')];
        if(count($lastTripDate)) {
            for ($y = date('Y', time()); $y <= date('Y', strtotime($lastTripDate->end)); $y++) {
                for ($m = 1; $m <= 12; $m++) {
                    if ($m < date('n', time()) && $y == date('Y', time())) {
                        continue;
                    }
                    if ($m > date('n', strtotime($lastTripDate->end)) && $y == date('Y', strtotime($lastTripDate->end))) {
                        break;
                    }
                    $dates[$y . '-' . $m] = $this->getMonthName($m) . ' ' . $y;
                }
            }
        }

        return view('frontend.trips.list', compact('trips', 'countries', 'lastTripDate', 'dates'));
    }

    public function show($id)
    {
        $trip = Trip::whereStatus(1)->where('id', $id)->first();
        return view('frontend.trips.trip', compact('trip'));
    }

    public function bookingConfirm(Request $request, $id)
    {
        if($request->single > 0 || $request->double > 0 || $request->triple > 0 || $request->quad > 0 || $request->quinary > 0){
            $booking                = new UserTrip();
            $booking->code          = time().auth()->guard('user')->user()->id;
            $booking->trip_id       = $id;
            $booking->single        = $request->single;
            $booking->double        = $request->double;
            $booking->triple        = $request->triple;
            $booking->quad          = ($request->has('quad')) ? $request->quad : 0;
            $booking->quinary       = ($request->has('quinary')) ? $request->quinary : 0;
            $booking->childes_first = $request->childes_first_count;
            $booking->childes_second= $request->childes_second_count;
            $booking->childes_third = $request->childes_third_count;
            $booking->total         = $request->total;
            $booking->user_id       = auth()->guard('user')->user()->id;
            if($booking->save()){
                UserTrip::where('id', $booking->id)->update(['code' => 'TR-'.$booking->id]);
                $trip = Trip::find($id);
                return view('frontend.thanks-page', ['msg' => trans('trips.request-success', ['link' => route('site.payments.upload', ['trip', $booking->id])]), 'country' => $trip->country]);
            }
        }
        return back()->with('msg', trans('common.request-error'));
    }

    public function myTripsRequests()
    {
        $requests = UserTrip::where('user_id', auth()->guard('user')->user()->id)->get();
        return view('frontend.users.trips', compact('requests'));
    }

    public function agencyTripsRequests()
    {
        $requests = UserTrip::whereHas('trip', function($q){
            $q->where('created_by', auth()->guard('user')->user()->id);
        })->get();
        return view('frontend.users.trips', compact('requests'));
    }

    public function getMonthName($month)
    {
        $months = [
            'ar' => [
                "1" => "جانفي",
                "2" => "فيفري",
                "3" => "مارس",
                "4" => "افريل",
                "5" => "ماي",
                "6" => "جوان",
                "7" => "جولية",
                "8" => "اوت",
                "9" => "سبتمبر",
                "10" => "أكتوبر",
                "11" => "نوفمبر",
                "12" => "ديسمبر"
            ],
            'fr' => [
                "1" => "janvier",
                "2" => "février",
                "3" => "mars",
                "4" => "avril",
                "5" => "mai",
                "6" => "juin",
                "7" => "juillet",
                "8" => "août",
                "9" => "septembre",
                "10" => "octobre",
                "11" => "novembre",
                "12" => "décembre"
            ]
        ];
        return $months[\LaraLocale::getCurrentLocale()][$month];
    }
}
