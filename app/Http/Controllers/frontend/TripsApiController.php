<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Trip;
use App\Transformers\TripTransformer;
use Cyvelnet\Laravel5Fractal\Facades\Fractal;
use Illuminate\Http\Request;

class TripsApiController extends Controller
{
    public function index()
    {
        $trips = Trip::where(function ($query) {
            $query->whereFixed('0')->whereStatus(1);
            if(\Request::has('country')){
                if(\Request::has('city')){
                    $query->whereHas('city', function ($q){ $q->where('slug', \Request::get('city')); });
                }else{
                    $query->whereHas('country', function ($q){ $q->where('slug', \Request::get('country')); });
                }
            }
            if(\Request::has('date')){
                $date = explode('-', \Request::get('date'));
                $query->whereMonth('start', $date[1])->whereYear('start', $date[0]);
            }else{
                $query->where('start', '>=', date('Y-m-d'));
            }
        })->orWhere(function ($query) {
            $query->whereFixed('1')->whereStatus(1);
            if(\Request::has('country')){
                if(\Request::has('city')){
                    $query->whereHas('city', function ($q){ $q->where('slug', \Request::get('city')); });
                }else{
                    $query->whereHas('country', function ($q){ $q->where('slug', \Request::get('country')); });
                }
            }
        })->get();

        return Fractal::includes(['country', 'city', 'owner'])->collection($trips, new TripTransformer());
    }

    public function show(Trip $trip)
    {
        return Fractal::includes(['country', 'city', 'owner', 'album'])->item($trip, new TripTransformer);
    }

    public function tripsDatesFilter()
	{
		$lastTripDate = Trip::where('start', '>=', date('Y-m-d'))->whereFixed('0')->whereStatus(1)->orderBy('end', 'desc')->first();
		$dates = [];
		if($lastTripDate) {
			for ($y = date('Y', time()); $y <= date('Y', strtotime($lastTripDate->end)); $y++) {
				for ($m = 1; $m <= 12; $m++) {
					if ($m < date('n', time()) && $y == date('Y', time())) {
						continue;
					}
					if ($m > date('n', strtotime($lastTripDate->end)) && $y == date('Y', strtotime($lastTripDate->end))) {
						break;
					}
					$dates[$y . '-' . $m] = $this->getMonthName($m) . ' ' . $y;
				}
			}
		}

		return response()->json($dates);
	}

	public function getMonthName($month)
	{
		$months = [
			'ar' => [
				"1" => "جانفي",
				"2" => "فيفري",
				"3" => "مارس",
				"4" => "افريل",
				"5" => "ماي",
				"6" => "جوان",
				"7" => "جولية",
				"8" => "اوت",
				"9" => "سبتمبر",
				"10" => "أكتوبر",
				"11" => "نوفمبر",
				"12" => "ديسمبر"
			],
			'fr' => [
				"1" => "janvier",
				"2" => "février",
				"3" => "mars",
				"4" => "avril",
				"5" => "mai",
				"6" => "juin",
				"7" => "juillet",
				"8" => "août",
				"9" => "septembre",
				"10" => "octobre",
				"11" => "novembre",
				"12" => "décembre"
			]
		];
		return $months[\LaraLocale::getCurrentLocale()][$month];
	}
}
