<?php

namespace App\Http\Controllers\frontend;

use App\Models\Amenity;
use App\Models\Country;
use App\Models\Hotel;
use App\Models\HotelCustom;
use App\Models\UserHotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelsController extends Controller
{
    public function index()
    {
        $hotels = Hotel::whereStatus(1);
        if(\Request::has('country')){
            if(\Request::has('city')){
                $hotels = $hotels->whereHas('city', function ($q){
                    $q->where('slug', \Request::get('city'));
                });
            }else{
                $hotels = $hotels->whereHas('city', function ($q){
                    $q->whereHas('country', function ($q){
                        $q->where('slug', \Request::get('country'));
                    });
                });
            }
        }
        $hotels = $hotels->paginate(9);
        $countries = ['' => trans('common.all')];
        $countries += Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'slug')->all();
        return view('frontend.hotels.list', compact('hotels', 'countries'));
    }

    public function show($id)
    {
        $hotel = Hotel::whereStatus(1)->where('id', $id)->first();
        $amenities = [];
        if(!empty($hotel->amenities) && count(json_decode($hotel->amenities))){
            $amenities = Amenity::whereIn('id', json_decode($hotel->amenities))->get();
        }
        return view('frontend.hotels.hotel', compact('hotel', 'amenities'));
    }

    public function booking(Request $request, $id)
    {
        $subtotal = 0;
        $booking = [
            'checkin' => $request->checkin,
            'checkout' => $request->checkout,
            'days' => floor((strtotime($request->checkout) - strtotime($request->checkin)) / (60 * 60 * 24)),
            'childes' => $request->childes_ages ?? []
        ];

        $hotel = Hotel::find($id);

        foreach ($hotel->rooms as $room) {
            if ($request->has('count_' . $room->id) && $request->get('count_' . $room->id) > 0) {
                $booking['rooms'][$room->id] = $request->get('count_' . $room->id);
                $subtotal += $room->price;
            }
        }
        if($subtotal == 0)
            return back()->with('msg', trans('common.request-error'));

        if ($request->childes_ages && $request->childes_ages > 0){
            foreach ($request->childes_ages as $age) {
                if($age <= $hotel->childes_phase1_age)
                    $subtotal += $hotel->childes_phase1_price;
                elseif($age <= $hotel->childes_phase2_age)
                    $subtotal += $hotel->childes_phase2_price;
            }
        }

        $booking['subtotal'] = $subtotal;

        return view('frontend.hotels.booking', compact('hotel','booking'));
    }

    public function bookingConfirm(Request $request)
    {
        $this->validate($request, ['hotel_id' => 'required','checkin' => 'required','checkout' => 'required','rooms' => 'required','childes' => 'required','total' => 'required']);

        $booking            = new UserHotel;
        $booking->code      = time().auth()->guard('user')->user()->id;
        $booking->user_id   = auth()->guard('user')->user()->id;
        $booking->hotel_id  = $request->hotel_id;
        $booking->rooms     = $request->rooms;
        $booking->rooms_members = $request->members;
        $booking->childes   = $request->childes;
        $booking->checkin   = date('Y-m-d', strtotime($request->checkin));
        $booking->checkout  = date('Y-m-d', strtotime($request->checkout));
        $booking->total     = $request->total;
        if($booking->save()){
            UserHotel::where('id', $booking->id)->update(['code' => 'HT-'.$booking->id]);
            $hotel = Hotel::find($request->hotel_id);
            return view('frontend.thanks-page', ['msg' => trans('hotels.request-success', ['link' => route('site.payments.upload', ['hotel', $booking->id])]), 'country' => $hotel->city->country]);
        }

        return back();
    }

    public function myHotelsRequests()
    {
        $requests = UserHotel::where('user_id', auth()->guard('user')->user()->id)->get();
        return view('frontend.users.hotels', compact('requests'));
    }

    public function custom(Request $request)
    {
        if($request->isMethod('post')){
            $this->validate($request, ['destination' => 'required', 'checkin' => 'required', 'checkout' => 'required']);

            $inputs = $request->only(['destination', 'hotel', 'checkin', 'checkout', 'budget', 'adults_count', 'childes_count', 'rooms_count', 'note']);
            if($request->childes_count > 0 && $request->has('childes_ages')){
                $inputs['childes_ages'] = json_encode($request->childes_ages);
            }
            $inputs['user_id'] = auth()->guard('user')->user()->id;
            $inputs['code'] = time().auth()->guard('user')->user()->id;
            if(HotelCustom::create($inputs)){
                return view('frontend.thanks-page', ['msg' => trans('hotels.custom-request-success')]);
            }
            return back()->with('msg', trans('common.request-error'));
        }

        return view('frontend.hotels.custom');
    }

    public function myHotelsRequestsCustom()
    {
        $requests = HotelCustom::where('user_id', auth()->guard('user')->user()->id)->get();
        return view('frontend.users.hotels-custom', compact('requests'));
    }
}
