<?php

namespace App\Http\Controllers\frontend;

use App\Models\Hotel;
use App\Models\Setting;
use App\Models\UserHotel;
use App\Transformers\HotelTransformer;
use Cyvelnet\Laravel5Fractal\Facades\Fractal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelsApiController extends Controller
{
    public function index()
    {
        $hotels = Hotel::whereStatus(1);
        if(\Request::has('country')){
            if(\Request::has('city')){
                $hotels = $hotels->whereHas('city', function ($q){
                    $q->where('slug', \Request::get('city'));
                });
            }else{
                $hotels = $hotels->whereHas('city', function ($q){
                    $q->whereHas('country', function ($q){
                        $q->where('slug', \Request::get('country'));
                    });
                });
            }
        }
        $hotels = $hotels->get();

        return Fractal::includes('city')->collection($hotels, new HotelTransformer);
    }

    public function show(Hotel $hotel)
    {
        return Fractal::includes(['city', 'rooms', 'album'])->item($hotel, new HotelTransformer);
    }

    public function booking(Request $request)
    {
        $this->validate($request, ['hotel_id' => 'required','checkin' => 'required','checkout' => 'required','childes' => 'required']);

        $subtotal = 0;
        $rooms = [];
        $days = floor((strtotime($request->checkout) - strtotime($request->checkin)) / (60 * 60 * 24));

        $hotel = Hotel::find($request->hotel_id);

        foreach ($hotel->rooms as $room) {
            if ($request->has('count_' . $room->id) && $request->get('count_' . $room->id) > 0) {
                $rooms[$room->id] = $request->get('count_' . $room->id);
                $subtotal += $room->price;
            }
        }
        if($subtotal == 0)
            return response()->json(['success' => false, 'msg' => trans('common.request-error')]);

        if ($request->childes_ages && $request->childes_ages > 0){
            foreach ($request->childes_ages as $age) {
                if($age <= $hotel->childes_phase1_age)
                    $subtotal += $hotel->childes_phase1_price;
                elseif($age <= $hotel->childes_phase2_age)
                    $subtotal += $hotel->childes_phase2_price;
            }
        }

        $booking            = new UserHotel;
        $booking->user_id   = auth()->guard('api')->user()->id;
        $booking->hotel_id  = $request->hotel_id;
        $booking->rooms     = json_encode($rooms);
        $booking->childes   = json_encode($request->childes_ages);
        $booking->checkin   = date('Y-m-d', strtotime($request->checkin));
        $booking->checkout  = date('Y-m-d', strtotime($request->checkout));
        $booking->total     = $subtotal * Setting::find(1)->currency_rate * $days;
        if($booking->save()){
            UserHotel::where('id', $booking->id)->update(['code' => 'HT-'.$booking->id]);
            return response()->json(['success' => true, 'hotel_id' => $booking->id]);
        }
        return response()->json(['success' => false, 'msg' => strip_tags(trans('common.request-error'))]);
    }
}
