<?php

namespace App\Http\Controllers\backend;

use App\Models\Navigation;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NavigationsController extends Controller
{
    protected $viewPath = 'backend.navigations.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allItems = Navigation::all();
        return view($this->viewPath.'list', compact('allItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['parents'] = ['0' => trans('common.none')];
        $this->data['parents'] += Navigation::pluck('title_'.\LaraLocale::getCurrentLocale(), 'id')->all();

        $this->data['menuList'] = [
            '#'                 => trans('common.none'),
            '/'                 => trans('common.homepage'),
            'contact-us.html'   => trans('contact.contact-us'),
            'visas'             => trans('visas.visas'),
            'hotels'            => trans('hotels.hotel'),
            'trips'             => trans('trips.trips'),
            'flights'           => trans('flights.flights'),
            'hotels/special'    => trans('hotels.hotel-custom-requests'),
        ];
        $this->data['menuList'] += Page::pluck('title_'.\LaraLocale::getCurrentLocale(), \DB::raw("CONCAT(slug,'.html')  AS pageUrl"))->all();

        $this->data['title'] = trans('navigations.create-item');

        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Navigation::$rules);

        $nav = new Navigation();
        if($nav->insert($this->getInputs($request))){
            return redirect(route('navigations.index'))->with('msg', trans('common.add-success'));
        }else{
            return back()->with('msg', trans('common.add-failed'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['parents'] = ['0' => trans('common.none')];
        $this->data['parents'] += Navigation::pluck('title_'.\LaraLocale::getCurrentLocale(), 'id')->all();

        $this->data['menuList'] = [
            '#'                 => trans('common.none'),
            '/'                 => trans('common.homepage'),
            'contact-us.html'   => trans('contact.contact-us'),
            'visas'             => trans('visas.visas'),
            'hotels'            => trans('hotels.hotel'),
            'trips'             => trans('trips.trips'),
            'flights'           => trans('flights.flights'),
            'hotels/special'    => trans('hotels.hotel-custom-requests'),
        ];
        $this->data['menuList'] += Page::pluck('title_'.\LaraLocale::getCurrentLocale(), \DB::raw("CONCAT(slug,'.html')  AS pageUrl"))->all();

        $this->data['title'] = trans('navigations.update-item');

        $this->data['itemData'] = Navigation::find($id);

        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Navigation::$rules);

        $nav = Navigation::find($id);
        if($nav->update($this->getInputs($request))){
            return redirect(route('navigations.index'))->with('msg', trans('common.update-success'));
        }else{
            return back()->with('msg', trans('common.update-failed'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Navigation::destroy($id);
        if($item){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    protected function getInputs($request)
    {
        $inputs = $request->only(['parent', 'link', 'order', 'status']);

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['title_'.$localeCode] = $request->get('title_'.$localeCode);
        }

        if($request->isMethod('put'))
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
        else
            $inputs['created_by'] = \Auth::guard('admin')->user()->id;

        return $inputs;
    }
}