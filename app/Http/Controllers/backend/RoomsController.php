<?php

namespace App\Http\Controllers\backend;

use App\Models\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoomsController extends Controller
{
    protected $viewPath = 'backend.rooms.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hID)
    {
        $allRooms = Room::where('hotel_id', $hID)->get();
        $hotelID = $hID;
        return view($this->viewPath.'list', compact('allRooms', 'hotelID'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hID)
    {
        $title = trans('hotels.create-room');
        $hotelID = $hID;
        return view($this->viewPath.'form', compact('title', 'hotelID'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Room::rules());

        $room = new Room;
        if($room->insert($this->getInputs($request)))
            return redirect(route('rooms.index', [$request->hotel_id]))->with('msg', trans('common.add-success'));

        return back()->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = trans('hotels.update-room');
        $roomData = Room::find($id);
        return view($this->viewPath.'form', compact('title', 'roomData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Room::rules());

        $room = Room::find($id);
        if($room->update($this->getInputs($request)))
            return redirect(route('rooms.index', [$room->hotel_id]))->with('msg', trans('common.update-success'));

        return redirect(route('rooms.index', [$room->hotel_id]))->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Room::destroy($id)){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    protected function getInputs($request)
    {
        $inputs = $request->only([
            'hotel_id', 'adults_count', 'price', 'childes_count'
        ]);

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['title_'.$localeCode] = $request->get('title_'.$localeCode);
            $inputs['description_'.$localeCode] = $request->get('description_'.$localeCode);
        }

        if ($request->hasFile('image')) {
            $inputs['image'] = $this->upload($request, 'hotels');
        }

        return $inputs;
    }

    public function removeImg(Request $request)
    {
        if($request->has('rID') && $request->has('img')){
            $room = Room::find($request->rID);
            if($room->update(['image' => ''])){
                \File::delete(storage_path('upload/hotels/'.$request->img));
                \File::delete(storage_path('upload/hotels/thumbnail-'.$request->img));
                \File::delete(storage_path('upload/hotels/small-'.$request->img));
                \File::delete(storage_path('upload/hotels/medium-'.$request->img));
                \File::delete(storage_path('upload/hotels/large-'.$request->img));
                return response(['success' => true], 200);
            }
            return response(['success' => false], 200);
        }
    }
}
