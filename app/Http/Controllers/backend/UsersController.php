<?php

namespace App\Http\Controllers\backend;

use App\Models\User_phone;
use App\Models\User_skill;
use App\Models\City;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    protected $viewPath = 'backend.users.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUsers = User::select();
        if(\Request::has('type'))
            $allUsers->where('type', \Request::get('type'));
        $allUsers = $allUsers->get();
        return view($this->viewPath.'list', compact('allUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agency = false;
        $title = trans('users.create-user');
        return view($this->viewPath.'form', compact('title', 'agency'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, User::createRules());

        $user = new User();
        if($user->insert($this->getInputs($request)))
            return redirect(route('users.index'))->with('msg', trans('common.add-success'));

        return back()->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = trans('users.update-user');
        $userData = User::find($id);
        $agency = ($userData->type == 'agency') ? true : false;
        return view($this->viewPath.'form', compact('title', 'userData', 'agency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, User::updateRules());

        $user = User::find($id);
        if($user->update($this->getInputs($request)))
            return redirect(route('users.index'))->with('msg', trans('common.update-success'));

        return back()->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(User::destroy($id)){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    protected function getInputs($request)
    {
        $inputs = $request->only([
            'firstname', 'lastname', 'email', 'phone', 'section', 'town', 'state', 'address', 'type', 'status'
        ]);

        if($request->type == 'agency'){
            $inputs['website'] = $request->website;
            $inputs['facebook'] = $request->facebook;
            $inputs['license_number'] = $request->license_number;
            $inputs['license_year'] = $request->license_year;
        }else{
            $inputs['passport'] = $request->passport;
        }

        if($request->isMethod('put')) {
            if($request->has('password') && $request->password)
                $inputs['password'] = bcrypt($request->password);
        }else {
            $inputs['password'] = bcrypt($request->password);
        }

        $inputs['created_by'] = 0;
        $inputs['updated_by'] = 0;

        return $inputs;
    }

    public function generate()
    {
        $users = User::where('type', 'user')->get();
        $data = [];
        if(count($users)){
            foreach ($users as $user){
                $data[] = [
                    '#' => $user->id,
                    'الاسم الأول' => $user->firstname,
                    'الاسم الثاني' => $user->lastname,
                    'البريد الإلكتروني' => $user->email,
                    'الهاتف' => $user->phone,
                    'الدائرة' => $user->section,
                    'البلدية' => $user->town,
                    'الولاية' => $user->state,
                    'اسم الشارع' => $user->address,
                ];
            }
        }
        \Excel::create('users_'.date('Y-m-d'), function($excel) use($data) {
            $excel->sheet('Users List', function($sheet) use($data) {
                $sheet->fromArray($data);

                $sheet->cells('A1:I1', function($cells) {
                    $cells->setFontWeight('bold')->setAlignment('center')->setBackground('#00cc00')->setFontSize(16);
                });
            })->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        })->download('xlsx');

    }
}
