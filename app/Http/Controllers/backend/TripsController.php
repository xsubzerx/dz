<?php

namespace App\Http\Controllers\backend;

use App\Models\Country;
use App\Models\Trip;
use App\Models\TripPhoto;
use App\Models\User;
use App\Models\UserTrip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TripsController extends Controller
{
    protected $viewPath = 'backend.trips.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allTrips = Trip::select('*');
        if(\Request::has('country_id')){
            if(\Request::has('city_id')){
                $allTrips = $allTrips->where('city_id', \Request::get('city_id'));
            }else{
                $allTrips = $allTrips->where('country_id', \Request::get('country_id'));
            }
        }
        if(\Request::has('status')){
            $allTrips = $allTrips->where('status', \Request::get('status'));
        }
        if(\Request::has('owner')){
            $allTrips = $allTrips->where('created_by', \Request::get('owner'));
        }
        $allTrips = $allTrips->get();
        $countries = ['' => trans('common.all')];
        $countries += Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        $agencies = ['' => trans('common.all')];
        $agencies += User::where('type', 'agency')->pluck('firstname', 'id')->all();
        return view($this->viewPath.'list', compact('allTrips', 'countries', 'agencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = trans('trips.create-trip');
        $countries = Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        return view($this->viewPath.'form', compact('title', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Trip::rules());

        $inputs = $this->getInputs($request);
        $inputs['updated_by'] = 0;
        $inputs['created_by'] = 0;

        $trip = new Trip;
        if($trip->insert($inputs))
            return redirect(route('trips.index'))->with('msg', trans('common.add-success'));

        return back()->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = trans('trips.update-trip');
        $countries = Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        $tripData = Trip::find($id);
        return view($this->viewPath.'form', compact('title', 'countries', 'tripData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Trip::rules());

        $inputs = $this->getInputs($request);
        $inputs['updated_by'] = 0;

        $trip = Trip::find($id);
        if($trip->update($inputs))
            return redirect(route('trips.index'))->with('msg', trans('common.update-success'));

        return redirect(route('trips.index'))->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Trip::destroy($id)){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    protected function getInputs($request)
    {
        $inputs = $request->only([
            'country_id', 'city_id', 'fixed', 'start', 'end', 'status', 'featured', 'single_price', 'double_price', 'commission',
            'triple_price', 'quad_price', 'quinary_price', 'childes_first_price', 'childes_second_price', 'childes_third_price'
        ]);

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['title_'.$localeCode] = $request->get('title_'.$localeCode);
            $inputs['description_'.$localeCode] = $request->get('description_'.$localeCode);
            $inputs['has_'.$localeCode] = $request->get('has_'.$localeCode);
            $inputs['has_not_'.$localeCode] = $request->get('has_not_'.$localeCode);
        }

        return $inputs;
    }

    public function requests()
    {
        $this->data['allRequests'] = UserTrip::all();
        return view($this->viewPath.'requests', $this->data);
    }

    public function showRequest($id)
    {
        $request = UserTrip::find($id);
        return view($this->viewPath.'request', compact('request'));
    }

    public function updateRequest(Request $request)
    {
        $req = UserTrip::findOrFail($request->get('request'));
        $inputs = $request->only(['status', 'admin_comment']);
        if($req->update($inputs))
            return back()->with('msg', trans('common.update-success'));

        return back()->with('msg', trans('common.update-failed'));
    }

    public function album(Request $request, $tID)
    {
        if($request->isMethod('post')){
            $imgName = $this->upload($request, 'trips');
            if($imgName){
                $photo = new TripPhoto;
                $photo->create(['trip_id' => $tID, 'photo' => $imgName]);
            }
        }elseif($request->ajax()){
            $photos = [];
            $album = TripPhoto::where('trip_id', $tID)->get();
            foreach ($album as  $ph){
                $photos[$ph->photo] = ['size' => \Image::make(storage_path('upload/trips/'.$ph->photo))->filesize(), 'featured' => $ph->featured];
            }
            return response()->json($photos);
        }

        return view($this->viewPath.'album', ['tID' => $tID]);
    }

    public function destroyPhoto($photo)
    {
        if(TripPhoto::where('photo', $photo)->delete()){
            \File::delete(storage_path('upload/trips/'.$photo));
            \File::delete(storage_path('upload/trips/thumbnail-'.$photo));
            \File::delete(storage_path('upload/trips/small-'.$photo));
            \File::delete(storage_path('upload/trips/medium-'.$photo));
            \File::delete(storage_path('upload/trips/large-'.$photo));
        }
    }

    public function makeFeatured($photo)
    {
        if(\DB::table('trip_photos')->where('photo', $photo)->update(['featured' => 1])){
            \DB::table('trip_photos')->where('photo', '!=', $photo)->update(['featured' => 0]);
            return response(['success' => true], 200);
        }
        return response(['success' => false], 200);
    }
}
