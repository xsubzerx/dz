<?php

namespace App\Http\Controllers\backend;

use App\Models\City;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CitiesController extends Controller
{
    protected $viewPath = 'backend.cities.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['allCities'] = City::all();
        return view($this->viewPath.'list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = trans('cities.create-city');
        $this->data['countries'] = Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, City::$rules);

        $city = new City();
        if($city->insert($this->getInputs($request)))
            return redirect(route('cities.index'))->with('msg', trans('common.add-success'));

        return back()->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = trans('cities.update-city');
        $this->data['countries'] = Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        $this->data['cityData'] = City::find($id);
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, City::$rules);

        $city = City::find($id);
        if($city->update($this->getInputs($request)))
            return redirect(route('cities.index'))->with('msg', trans('common.update-success'));

        return redirect(route('cities.index'))->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(City::destroy($id)){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    protected function getInputs($request)
    {
        $inputs = $request->only(['country_id', 'status']);

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['name_'.$localeCode] = $request->get('name_'.$localeCode);
        }

        if($request->isMethod('put')) {
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
        }else {
            $inputs['slug'] = str_slug($request->name_fr);
            $inputs['created_by'] = \Auth::guard('admin')->user()->id;
        }

        return $inputs;
    }

    public function country($countryID, $cityID=0)
    {
        $citiesList = '';
        $cities = City::where('country_id', $countryID)->get();
        if(count($cities)){
            foreach ($cities as $city){
                if($city->id == $cityID)
                    $citiesList .= '<option value="'.$city->id.'" selected="selected">'.$city->name.'</option>';
                $citiesList .= '<option value="'.$city->id.'">'.$city->name.'</option>';
            }
        }
        return  response()->json(compact('citiesList'));
    }
}
