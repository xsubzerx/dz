<?php

namespace App\Http\Controllers\backend;

use App\Jobs\SendEmailJob;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsletterController extends Controller
{
    public function index()
	{
		return view('backend.newsletter');
	}

	public function send(Request $request)
	{
		$data = [
			'subject'	=> $request->subject,
			'message'	=> $request->message,
		];
		$emails = User::select('email');
		if($request->receiver == 'users'){
			$emails->whereType('user');
		}elseif($request->receiver == 'agencies'){
			$emails->whereType('agency');
		}
		$data['emails'] = $emails->pluck('email')->toArray();

		dispatch(new SendEmailJob($data));

		return back()->with(['msg' => trans('common.message-alert', ['alert' => 'success', 'msg' => 'Newsletter Sent...'])]);
	}
}
