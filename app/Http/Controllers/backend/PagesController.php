<?php

namespace App\Http\Controllers\backend;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManager;

class PagesController extends Controller
{
    protected $viewPath = 'backend.pages.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['allPages'] = Page::all();
        return view($this->viewPath.'list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = trans('pages.create-page');
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Page::$rules);

        $page = new Page();
        if($page->insert($this->getInputs($request)))
            return redirect(route('pages.index'))->with('msg', trans('common.add-success'));

        return redirect(route('pages.index'))->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = trans('pages.update-page');
        $this->data['pageData'] = Page::find($id);
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Page::$rules);

        $page = Page::find($id);
        $oldImg = $page->image;
        if($page->update($this->getInputs($request))) {
            if($request->hasFile('image') && !empty($oldImg)){
                \File::delete([
                    storage_path('upload/pages/'.$oldImg),
                    storage_path('upload/pages/thumbnail-'.$oldImg),
                    storage_path('upload/pages/small-'.$oldImg),
                    storage_path('upload/pages/medium-'.$oldImg),
                    storage_path('upload/pages/large-'.$oldImg)
                ]);
            }
            return redirect(route('pages.index'))->with('msg', trans('common.update-success'));
        }

        return redirect(route('pages.index'))->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::destroy($id);
        if($page)
            return back()->with('msg', trans('common.delete-success'));

        return back()->with('msg', trans('common.delete-failed'));
    }

    protected function getInputs($request)
    {
        $inputs['status'] = $request->status;

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['title_'.$localeCode] = $request->get('title_'.$localeCode);
            $inputs['content_'.$localeCode] = $request->get('content_'.$localeCode);
        }

        if ($request->hasFile('image')) {
            $inputs['image'] = $this->upload($request, 'pages');
        }

        if($request->isMethod('put')) {
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
        }else {
            $inputs['slug'] = str_slug($request->title_fr);
            $inputs['created_by'] = \Auth::guard('admin')->user()->id;
        }

        return $inputs;
    }
}
