<?php

namespace App\Http\Controllers\backend;

use App\Models\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocumentsController extends Controller
{
    protected $viewPath = 'backend.documents.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($visa)
    {
        $this->data['allDocs'] = Document::where('visa_id', $visa)->get();
        $this->data['visa_id'] = $visa;
        return view($this->viewPath.'list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($visa)
    {
        $this->data['title'] = trans('visas.application');
        $this->data['visa_id'] = $visa;
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Document::rules());

        $doc = new Document();
        if($doc->insert($this->getInputs($request)))
            return redirect(route('documents.index', [$request->visa_id]))->with('msg', trans('common.add-success'));

        return redirect(route('documents.index', [$request->visa_id]))->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = trans('visas.application');
        $this->data['docData'] = Document::find($id);
        $this->data['visa_id'] = $this->data['docData']->visa_id;
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Document::rules());

        $doc = Document::find($id);
        if($doc->update($this->getInputs($request)))
            return redirect(route('documents.index', [$doc->visa_id]))->with('msg', trans('common.update-success'));

        return redirect(route('documents.index', [$doc->visa_id]))->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Document::destroy($id)){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    protected function getInputs($request)
    {
        $inputs = $request->only(['visa_id', 'has_app']);

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['title_'.$localeCode] = $request->get('title_'.$localeCode);
        }

        if ($request->hasFile('image') && $request->has_app) {
            $inputs['application'] = $this->upload($request, 'visas');
        }

        if($request->isMethod('put')) {
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
        }else {
            $inputs['created_by'] = \Auth::guard('admin')->user()->id;
        }

        return $inputs;
    }
}
