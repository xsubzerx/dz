<?php

namespace App\Http\Controllers\backend;

use App\Models\Amenity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allAmenities = Amenity::all();
        return view('backend.hotels.amenities', compact('allAmenities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title_ar' => 'required', 'title_fr' => 'required']);

        $country = new Amenity();
        if($country->insert($request->only(['title_ar', 'title_fr', 'icon'])))
            return redirect(route('amenities.index'))->with('msg', trans('common.add-success'));

        return redirect(route('amenities.index'))->with('msg', trans('common.add-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Amenity::destroy($id))
            return back()->with('msg', trans('common.delete-success'));

        return back()->with('msg', trans('common.delete-failed'));
    }
}
