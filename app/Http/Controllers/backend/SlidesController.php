<?php

namespace App\Http\Controllers\backend;

use App\Models\Page;
use App\Models\Slide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlidesController extends Controller
{
    protected $viewPath = 'backend.slides.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['allSlides'] = Slide::all();
        return view($this->viewPath.'list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = trans('slides.create-slide');
        $this->data['menuList'] = [
            '#'                 => trans('common.none'),
            '/'                 => trans('common.homepage'),
            'contact-us.html'   => trans('contact.contact-us'),
            'visas'             => trans('visas.visas'),
            'hotels'            => trans('hotels.hotel'),
            'trips'             => trans('trips.trips'),
            'flights'           => trans('flights.flights'),
            'hotels/special'    => trans('hotels.hotel-custom-requests'),
        ];
        $this->data['menuList'] += Page::pluck('title_'.\LaraLocale::getCurrentLocale(), \DB::raw("CONCAT(slug,'.html')  AS pageUrl"))->all();
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Slide::getCreateRules());

        $slide = new Slide();
        if($slide->insert($this->getInputs($request)))
            return redirect(route('slides.index'))->with('msg', trans('common.add-success'));

        return redirect(route('slides.index'))->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = trans('sliders.update-slide');
        $this->data['menuList'] = [
            '#'                 => trans('common.none'),
            '/'                 => trans('common.homepage'),
            'contact-us.html'   => trans('contact.contact-us'),
            'visas'             => trans('visas.visas'),
            'hotels'            => trans('hotels.hotel'),
            'trips'             => trans('trips.trips'),
            'flights'           => trans('flights.flights'),
            'hotels/special'    => trans('hotels.hotel-custom-requests'),
        ];
        $this->data['menuList'] += Page::pluck('title_'.\LaraLocale::getCurrentLocale(), \DB::raw("CONCAT(slug,'.html')  AS pageUrl"))->all();
        $this->data['slideData'] = Slide::find($id);
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Slide::getUpdateRules());

        $slide = Slide::find($id);
        $oldImg = $slide->image;

        if($slide->update($this->getInputs($request))){
            if($request->hasFile('image') && !empty($oldImg)){
                \File::delete([
                    storage_path('upload/slides/'.$oldImg),
                    storage_path('upload/slides/thumbnail-'.$oldImg),
                    storage_path('upload/slides/small-'.$oldImg),
                    storage_path('upload/slides/medium-'.$oldImg),
                    storage_path('upload/slides/large-'.$oldImg)
                ]);
            }
            return redirect(route('slides.index'))->with('msg', trans('common.update-success'));
        }

        return redirect(route('slides.index'))->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Slide::destroy($id)){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    protected function getInputs($request)
    {
        $inputs = $request->only(['link', 'status']);

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['label_'.$localeCode] = $request->get('label_'.$localeCode);
        }

        if ($request->hasFile('image')) {
            $inputs['image'] = $this->upload($request, 'slides');
        }

        if($request->isMethod('put')) {
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
        }else {
            $inputs['created_by'] = \Auth::guard('admin')->user()->id;
        }

        return $inputs;
    }
}
