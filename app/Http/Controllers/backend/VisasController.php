<?php

namespace App\Http\Controllers\backend;

use App\Models\Country;
use App\Models\UserVisa;
use App\Models\Visa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisasController extends Controller
{
    protected $viewPath = 'backend.visas.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['allVisas'] = Visa::all();
        return view($this->viewPath.'list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = trans('visas.create-visa');
        $this->data['countries'] = Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Visa::$rules);

        $visa = new Visa();
        if($visa->insert($this->getInputs($request)))
            return redirect(route('visa.index'))->with('msg', trans('common.add-success'));

        return back()->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = trans('visas.update-visa');
        $this->data['countries'] = Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        $this->data['visaData'] = Visa::find($id);
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Visa::$rules);

        $visa = Visa::find($id);
        if($visa->update($this->getInputs($request)))
            return redirect(route('visa.index'))->with('msg', trans('common.update-success'));

        return redirect(route('visa.index'))->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Visa::destroy($id)){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    protected function getInputs($request)
    {
        $inputs = $request->only(['country_id', 'price', 'has_shipping', 'status', 'featured']);

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['title_'.$localeCode] = $request->get('title_'.$localeCode);
            $inputs['description_'.$localeCode] = $request->get('description_'.$localeCode);
        }

        $inputs['middle_price'] = ($request->has('middle_price') && $request->middle_price) ? $request->get('middle_price') : 0;
        $inputs['east_west_price'] = ($request->has('east_west_price') && $request->east_west_price) ? $request->get('east_west_price') : 0;
        $inputs['south_price'] = ($request->has('south_price') && $request->south_price) ? $request->get('south_price') : 0;

        if($request->isMethod('put')) {
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
        }else {
            $inputs['created_by'] = \Auth::guard('admin')->user()->id;
        }

        return $inputs;
    }

    public function requests()
    {
        $this->data['allRequests'] = UserVisa::all();
        return view($this->viewPath.'requests', $this->data);
    }
    public function showRequest($id)
    {
        $request = UserVisa::find($id);
        return view($this->viewPath.'request', compact('request'));
    }

    public function updateRequest(Request $request)
    {
        $req = UserVisa::findOrFail($request->get('request'));
        $inputs = $request->only(['status', 'admin_comment']);
        if($req->update($inputs))
            return back()->with('msg', trans('common.update-success'));

        return back()->with('msg', trans('common.update-failed'));
    }
}
