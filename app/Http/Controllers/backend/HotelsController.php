<?php

namespace App\Http\Controllers\backend;

use App\Models\Amenity;
use App\Models\Country;
use App\Models\Hotel;
use App\Models\Hotel_photo;
use App\Models\HotelCustom;
use App\Models\UserHotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelsController extends Controller
{
    protected $viewPath = 'backend.hotels.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allHotels = Hotel::select('*');

        if(\Request::has('country_id')){
            if(\Request::has('city_id')){
                $allHotels = $allHotels->where('city_id', \Request::get('city_id'));
            }else{
                $allHotels = $allHotels->whereHas('city', function ($q){
                    $q->where('country_id', \Request::get('country_id'));
                });
            }
        }
        if(\Request::has('status')){
            $allHotels = $allHotels->where('status', \Request::get('status'));
        }
        $allHotels = $allHotels->get();
        $countries = ['' => trans('common.all')];
        $countries += Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        return view($this->viewPath.'list', compact('allHotels', 'countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = trans('hotels.create-hotel');
        $countries = Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        $amenities = Amenity::pluck('title_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        $this->map();
        return view($this->viewPath.'form', compact('title', 'countries', 'amenities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Hotel::rules());

        $hotel = new Hotel();
        if($hotel->insert($this->getInputs($request)))
            return redirect(route('hotels.index'))->with('msg', trans('common.add-success'));

        return back()->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = trans('hotels.update-hotel');
        $countries = Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        $amenities = Amenity::pluck('title_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        $hotelData = Hotel::find($id);
        $currMap = ($hotelData->map) ? explode(',', $hotelData->map) : [0,0];
        $currLat = (is_array($currMap) && !empty($currMap[0])) ? $currMap[0] : '0';
        $currLng = (is_array($currMap) && !empty($currMap[1])) ? $currMap[1] : '0';
        $locate = ($currLat==0 || $currLng==0) ? true : false;
        $this->map($currLat, $currLng, $locate);
        return view($this->viewPath.'form', compact('title', 'countries', 'hotelData', 'amenities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Hotel::rules());

        $hotel = Hotel::find($id);
        if($hotel->update($this->getInputs($request)))
            return redirect(route('hotels.index'))->with('msg', trans('common.update-success'));

        return redirect(route('hotels.index'))->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Hotel::destroy($id)){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    protected function getInputs($request)
    {
        $inputs = $request->only([
            'city_id', 'map', 'phone', 'stars', 'status', 'featured', 'childes_phase1_age',
            'childes_phase1_price', 'childes_phase2_age', 'childes_phase2_price'
        ]);

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['name_'.$localeCode] = $request->get('name_'.$localeCode);
            $inputs['description_'.$localeCode] = $request->get('description_'.$localeCode);
            $inputs['address_'.$localeCode] = $request->get('address_'.$localeCode);
            $inputs['nearby_'.$localeCode] = $request->get('nearby_'.$localeCode);
        }

        if($request->isMethod('put')) {
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
        }else {
            $inputs['created_by'] = \Auth::guard('admin')->user()->id;
        }

        $inputs['amenities'] = ($request->has('amenities')) ? json_encode($request->amenities) : '';

        return $inputs;
    }

    public function album(Request $request, $hID)
    {
        if($request->isMethod('post')){
            $imgName = $this->upload($request, 'hotels');
            if($imgName){
                $photo = new Hotel_photo;
                $photo->create(['hotel_id' => $hID, 'photo' => $imgName]);
            }
        }elseif($request->ajax()){
            $photos = [];
            $album = Hotel_photo::where('hotel_id', $hID)->get();
            foreach ($album as  $ph){
                $photos[$ph->photo] = ['size' => \Image::make(storage_path('upload/hotels/'.$ph->photo))->filesize(), 'featured' => $ph->featured];
            }
            return response()->json($photos);
        }

        return view($this->viewPath.'album', ['hID' => $hID]);
    }

    public function destroyPhoto($photo)
    {
        if(Hotel_photo::where('photo', $photo)->delete()){
            \File::delete(storage_path('upload/hotels/'.$photo));
            \File::delete(storage_path('upload/hotels/thumbnail-'.$photo));
            \File::delete(storage_path('upload/hotels/small-'.$photo));
            \File::delete(storage_path('upload/hotels/medium-'.$photo));
            \File::delete(storage_path('upload/hotels/large-'.$photo));
        }
    }

    public function requests()
    {
        $allRequests = UserHotel::select('*');

        if(\Request::has('country_id')){
            if(\Request::has('city_id')){
                $allRequests = $allRequests->whereHas('hotel', function ($q){
                    $q->where('city_id', \Request::get('city_id'));
                });
            }else{
                $allRequests = $allRequests->whereHas('hotel', function ($h){
                    $h->whereHas('city', function ($q){
                        $q->where('country_id', \Request::get('country_id'));
                    });
                });
            }
        }
        if(\Request::has('status')){
            $allRequests = $allRequests->where('status', \Request::get('status'));
        }
        if(\Request::has('payment') && \Request::get('payment') > 0){
            $allRequests = $allRequests->has('payment');
        }elseif(\Request::has('payment') && \Request::get('payment') == 0){
            $allRequests = $allRequests->doesnthave('payment');
        }

        $allRequests = $allRequests->get();

        $countries = ['' => trans('common.all')];
        $countries += Country::pluck('name_'.\LaraLocale::getCurrentLocale(), 'id')->all();
        return view($this->viewPath.'requests', compact('allRequests', 'countries'));
    }

    public function showRequest($id)
    {
        $request = UserHotel::find($id);
        return view($this->viewPath.'request', compact('request'));
    }

    public function updateRequest(Request $request)
    {
        $req = UserHotel::findOrFail($request->get('request'));
        $inputs = $request->only(['status', 'admin_comment']);
        if($req->update($inputs))
            return back()->with('msg', trans('common.update-success'));

        return back()->with('msg', trans('common.update-failed'));
    }

    public function requestsCustom()
    {
        $this->data['allRequests'] = HotelCustom::all();
        return view($this->viewPath.'requests-custom', $this->data);
    }

    public function showRequestCustom($id)
    {
        $request = HotelCustom::find($id);
        return view($this->viewPath.'request-custom', compact('request'));
    }

    public function updateRequestCustom(Request $request)
    {
        $req = HotelCustom::findOrFail($request->get('request'));
        $inputs = $request->only(['status', 'admin_comment']);
        if($req->update($inputs))
            return back()->with('msg', trans('common.update-success'));

        return back()->with('msg', trans('common.update-failed'));
    }

    protected function map($lat=0, $lng=0, $locate=true)
    {
        \Mapper::map($lat, $lng, [
            'marker' => true,
            'locate' => $locate,
            'animation' => 'DROP',
            'zoom' => 16,
            'draggable' => true,
            'eventDragEnd' => 'document.getElementById("map").value=event.latLng.lat()+","+event.latLng.lng();',
        ]);
    }

    public function makeFeatured($photo)
    {

        if(\DB::table('hotel_photos')->where('photo', $photo)->update(['featured' => 1])){
            \DB::table('hotel_photos')->where('photo', '!=', $photo)->update(['featured' => 0]);
            return response(['success' => true], 200);
        }
        return response(['success' => false], 200);
    }
}
