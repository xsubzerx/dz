<?php

namespace App\Http\Controllers\backend;

use App\Models\Flight;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FlightsController extends Controller
{
    protected $viewPath = 'backend.flights.';

    public function requests()
    {
        $allRequests = Flight::all();
        return view($this->viewPath.'requests', compact('allRequests'));
    }

    public function showRequest($id)
    {
        $request = Flight::find($id);
        return view($this->viewPath.'request', compact('request'));
    }

    public function updateRequest(Request $request)
    {
        $req = Flight::findOrFail($request->get('request'));
        $inputs = $request->only(['status', 'admin_comment']);
        if($req->update($inputs))
            return back()->with('msg', trans('common.update-success'));

        return back()->with('msg', trans('common.update-failed'));
    }
}
