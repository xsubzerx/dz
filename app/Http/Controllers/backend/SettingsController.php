<?php

namespace App\Http\Controllers\backend;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function index(Request $request)
    {
        $settings = Setting::find(1);

        if($request->isMethod('post')){
            $this->validate($request, Setting::rules());

            $inputs = $request->only([
                'site_name_ar', 'site_name_fr', 'site_metakey_ar', 'site_metakey_fr', 'site_metadesc_ar',
                'site_metadesc_fr', 'facebook', 'twitter', 'youtube', 'instagram', 'currency_rate', 'status'
            ]);
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
            if($settings->update($inputs))
                return back()->with('msg', trans('common.update-success'));
            else
                return back()->with('msg', trans('common.update-failed'));
        }
        return view('backend.settings.form', compact('settings'));
    }

    public function terms(Request $request)
    {
        $settings = Setting::find(1);

        if($request->isMethod('post')){
            $inputs = $request->only([
                'visas_terms_ar', 'visas_terms_fr',
                'hotels_terms_ar', 'hotels_terms_fr',
                'trips_terms_ar', 'trips_terms_fr',
                'flights_terms_ar', 'flights_terms_fr',
                'agencies_terms_ar', 'agencies_terms_fr'
            ]);
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
            if($settings->update($inputs))
                return back()->with('msg', trans('common.update-success'));
            else
                return back()->with('msg', trans('common.update-failed'));
        }
        return view('backend.settings.terms', compact('settings'));
    }

    public function contacts(Request $request)
    {
        $settings = Setting::find(1);

        if($request->isMethod('post')){
            $this->validate($request, ['site_email' => 'required|email', 'site_phone' => 'required']);

            $inputs = $request->only([
                'site_email', 'site_phone', 'address_ar', 'address_fr', 'contact_page_ar', 'contact_page_fr'
            ]);
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
            if($settings->update($inputs))
                return back()->with('msg', trans('common.update-success'));
            else
                return back()->with('msg', trans('common.update-failed'));
        }
        return view('backend.settings.contacts', compact('settings'));
    }
}
