<?php

namespace App\Http\Controllers\backend;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminsController extends Controller
{
    protected $viewPath = 'backend.admins.';
    protected $data  = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['allUsers'] = Admin::all();
        return view($this->viewPath.'list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = trans('users.create-admin');
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Admin::rules());

        $admin = new Admin();
        if($admin->insert($this->getInputs($request)))
            return redirect(route('admins.index'))->with('msg', trans('common.add-success'));

        return redirect(route('admins.index'))->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = trans('users.update-admin');
        $this->data['userData'] = Admin::find($id);
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [ 'name' => 'required' ];
        if(!empty($request->password))
            $rules['password'] = 'required|min:3';
        $this->validate($request, Admin::rules());

        $admin = Admin::find($id);
        if($admin->update($this->getInputs($request)))
            return redirect(route('admins.index'))->with('msg', trans('common.update-success'));

        return redirect(route('admins.index'))->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Admin::destroy($id)){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    protected function getInputs($request)
    {
        $inputs = $request->only(['name', 'email', 'phone']);

        if($request->isMethod('put')) {
            $inputs['password'] = Hash::make($request->password);
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
        }else {
            if(!empty($request->password))
                $inputs['password'] = Hash::make($request->password);

            $inputs['created_by'] = \Auth::guard('admin')->user()->id;
        }

        return $inputs;
    }
}
