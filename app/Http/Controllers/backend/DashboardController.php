<?php

namespace App\Http\Controllers\backend;

use App\Models\Contact;
use App\Models\Flight;
use App\Models\HotelCustom;
use App\Models\UserHotel;
use App\Models\UserTrip;
use App\Models\UserVisa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $latestVisaRequests = UserVisa::whereStatus('pending')->orWhere('status', 'progress')->orderBy('id')->take(5)->get();
        $latestHotelsRequests = UserHotel::whereStatus('pending')->orWhere('status', 'progress')->orderBy('id')->take(5)->get();
        $latestTripsRequests = UserTrip::whereStatus('pending')->orWhere('status', 'progress')->orderBy('id')->take(5)->get();
        $latestFlightsRequests = Flight::whereStatus('pending')->orderBy('id')->take(5)->get();
        $latestHotelsCustomRequests = HotelCustom::whereStatus('pending')->orderBy('id')->take(5)->get();
        return view('backend.dashboard', compact('latestVisaRequests', 'latestHotelsRequests', 'latestTripsRequests', 'latestFlightsRequests', 'latestHotelsCustomRequests'));
    }

    public function contact()
    {
        $emails = Contact::all();
        return view('backend.contact-us', compact('emails'));
    }
}
