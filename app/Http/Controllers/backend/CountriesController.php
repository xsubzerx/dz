<?php

namespace App\Http\Controllers\backend;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountriesController extends Controller
{
    protected $viewPath = 'backend.countries.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['allCountries'] = Country::all();
        return view($this->viewPath.'list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = trans('countries.create-country');
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Country::$rules);

        $country = new Country();
        if($country->insert($this->getInputs($request)))
            return redirect(route('countries.index'))->with('msg', trans('common.add-success'));

        return redirect(route('countries.index'))->with('msg', trans('common.add-failed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = trans('countries.update-country');
        $this->data['countryData'] = Country::find($id);
        return view($this->viewPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Country::$rules);

        $country = Country::find($id);
        $oldImg = $country->flag;
        if($country->update($this->getInputs($request))){
            if($request->hasFile('flag') && !empty($oldImg)){
                \File::delete([
                    storage_path('upload/countries/'.$oldImg),
                    storage_path('upload/countries/thumbnail-'.$oldImg),
                    storage_path('upload/countries/small-'.$oldImg),
                    storage_path('upload/countries/medium-'.$oldImg),
                    storage_path('upload/countries/large-'.$oldImg)
                ]);
            }
            return redirect(route('countries.index'))->with('msg', trans('common.update-success'));
        }

        return redirect(route('countries.index'))->with('msg', trans('common.update-failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Country::destroy($id))
            return back()->with('msg', trans('common.delete-success'));

        return back()->with('msg', trans('common.delete-failed'));
    }

    protected function getInputs($request)
    {
        $inputs['status'] = $request->status;

        foreach(\LaraLocale::getSupportedLocales() as $localeCode => $properties){
            $inputs['name_'.$localeCode] = $request->get('name_'.$localeCode);
        }

        if ($request->hasFile('image')) {
            $inputs['flag'] = $this->upload($request, 'countries');
        }

        if($request->isMethod('put')) {
            $inputs['updated_by'] = \Auth::guard('admin')->user()->id;
        }else {
            $inputs['slug'] = str_slug($request->name_fr);
            $inputs['created_by'] = \Auth::guard('admin')->user()->id;
        }

        return $inputs;
    }
}
