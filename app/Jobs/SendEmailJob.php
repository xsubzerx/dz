<?php

namespace App\Jobs;

use App\Mail\Newsletter;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $details, $users;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->users = $data['emails'];
		$this->details = collect($data)->reject(function ($itm, $key){
			return $key == 'emails';
		});
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		if (is_array($this->users) && count($this->users) > 1) {
			foreach ($this->users as $user) {
				Mail::to($user)->queue(new Newsletter($this->details));
			}
		} else {
			Mail::to($this->users)->queue(new Newsletter($this->details));
		}
	}

}
