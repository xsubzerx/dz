<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <!-- meta character set -->
    <meta charset="utf-8">
    <!-- Site Title -->
    <title>Error 404, Page Not Found!</title>
    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/favicon.png') }}">
    <!--
    Google Fonts
    ============================================= -->

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,700italic,300italic">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,300,700">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arvo:400,700">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Dosis:800,700">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Great+Vibes">
    <!--
    CSS
    ============================================= -->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/frontend/css/font-awesome.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css') }}">
    <!-- Main Stylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/frontend/css/main.css') }}">
    <link id="themeColorChangeLink" type="text/css" rel="stylesheet" href="{{ asset('assets/frontend/css/colors/c10.css') }}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Preloader Two -->
    <div id="preloader">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>

    <div id="wrapper" class="main-wrapper">
        <!--
        Navigation
        ==================================== -->
        <header id="head" class="navbar-default sticky-header">
            <div class="container">
                <div class="mega-menu-wrapper border clearfix">
                    <div class="navbar-header">
                        <!-- responsive nav button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- /responsive nav button -->
                        <!-- logo -->
                        <a class="navbar-brand" href="{{ route('site.home') }}">
                            <img src="{{ asset('assets/frontend/img/logo.png') }}" alt="Voyagedz" height="80">
                        </a>
                        <!-- /logo -->
                    </div>
                    <!-- main nav -->
                    <nav class="collapse navbar-collapse navbar-right">
                        <ul class="nav navbar-nav">
                            <li class="current mega-menu"><a href="{{ route('site.home') }}">{{ trans('common.back-home') }}</a></li>
                        </ul>
                    </nav>
                    <!-- /main nav -->
                </div>
            </div>
        </header>
        <!--
        End Navigation
        ==================================== -->

        <div class="pages error404">
            <div class="container">
                <div class="error-inner">
                    <h2>404!</h2>
                    <h4>{{ trans('common.page-not-found') }}</h4>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <!--
        footer
        ==================================== -->
        <footer class="footer copyright-multi" style="position: absolute;width: 100%;left: 0;bottom: 0;">
            <div class="container text-center">
                <p>{{ trans('common.copyrights', ['VoyageDZ']) }}</p>
            </div>
        </footer>
        <!--
        End footer
        ==================================== -->

        <!-- back to top -->
        <a href="javascript:;" id="go-top">
            <i class="fa fa-angle-up"></i>
            top
        </a>
        <!-- end #go-top -->
    </div>
    <!--
    JavaScripts
    ========================== -->
    <!-- main jQuery -->
    <script type="text/javascript" src="{{ asset('assets/frontend/js/vendor/modernizr-2.6.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/frontend/js/vendor/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/frontend/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/frontend/js/wow.min.js') }}"></script>
    <script type="application/javascript">
        $(window).on("load", function() {
            $("#preloader").fadeOut("slow");
        });
    </script>
</body>
</html>