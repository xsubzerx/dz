@extends('backend.common.template')

@section('title'){{ trans('hotels.hotel-requests')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('hotels.destination') }}</th>
                            <th>{{ trans('hotels.hotel') }}</th>
                            <th>{{ trans('hotels.budget') }}</th>
                            <th>{{ trans('hotels.rooms-count') }}</th>
                            <th>{{ trans('hotels.adults-count') }}</th>
                            <th>{{ trans('hotels.childes-count') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allRequests as $request)
                            <tr>
                                <td>{{ $request->code }}</td>
                                <td>@if($request->user)<button class="btn-link" data-toggle="tooltip" data-original-title="{{ $request->user->phone }}">{{ $request->user->fullname }}</button>@endif</td>
                                <td>{{ $request->destination }}</td>
                                <td>{{ $request->hotel }}</td>
                                <td>{{ $request->budget }}</td>
                                <td>{{ $request->rooms_count }}</td>
                                <td>{{ $request->adults_count }}</td>
                                <td>{{ $request->childes_count }}</td>
                                <td>{!! trans('flights.statuses.'.$request->status.'.style') !!}</td>
                                <td><a href="{{ route('hotels.request.custom', [$request->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('hotels.destination') }}</th>
                            <th>{{ trans('hotels.hotel') }}</th>
                            <th>{{ trans('hotels.budget') }}</th>
                            <th>{{ trans('hotels.rooms-count') }}</th>
                            <th>{{ trans('hotels.adults-count') }}</th>
                            <th>{{ trans('hotels.childes-count') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
        });
    </script>
@stop