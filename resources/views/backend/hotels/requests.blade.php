@extends('backend.common.template')

@section('title'){{ trans('hotels.hotel-requests')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            {{ Form::open(['method' => 'GET']) }}
            <div class="box box-success">
                <div class='box-header'>
                    <h3 class="box-title">{{ trans('common.filters')}}</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        {{ Form::submit(trans('common.filter'), ['id' => 'filter', 'class' => 'btn btn-sm btn-success']) }}
                        <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class='box-body pad'>
                    <div class="form-group col-md-3">
                        {{ Form::label('country_id', trans('countries.country').':', ['class' => 'col-md-4']) }}
                        <div class="col-md-8 no-padding">
                            {{ Form::select('country_id', $countries, old('country_id'), ['id' => 'country_id', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('city_id', trans('cities.city').':', ['class' => 'col-md-4']) }}
                        <div class="col-md-8 no-padding">
                            {{ Form::select('city_id', ['' => trans('common.all')], old('city_id'), ['id' => 'city_id', 'class' => 'form-control']) }}
                            {{ Form::hidden('currCity', old('city_id'), ['id' => 'currCity']) }}
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('status', trans('common.status').':', ['class' => 'col-md-4']) }}
                        <div class="col-md-8 no-padding">
                            {{ Form::select('status', [''=>trans('common.all'), 'pending'=>trans('hotels.statuses.pending.label'), 'progress'=>trans('hotels.statuses.progress.label') ,'confirmed'=>trans('hotels.statuses.confirmed.label') ,'canceled'=>trans('hotels.statuses.canceled.label')], old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('payment', trans('common.payment-reset').':', ['class' => 'col-md-6']) }}
                        <div class="col-md-6 no-padding">
                            {{ Form::select('payment', [''=>trans('common.all'), '0'=>trans('common.no'), '1'=>trans('common.yes')], old('payment'), ['id' => 'payment', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            {{ Form::close() }}

            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('hotels.name') }}</th>
                            <th>{{ trans('cities.city') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('common.payment-reset') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.created-at') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allRequests as $request)
                            <tr>
                                <td>{{ $request->code }}</td>
                                <td>{{ $request->hotel->name_ar }}<br/>{{ $request->hotel->name_fr }}</td>
                                <td>{{ $request->hotel->city->name_ar }}<br/>{{ $request->hotel->city->name_fr }}</td>
                                <td>{{ $request->hotel->city->country->name_ar }}<br/>{{ $request->hotel->city->country->name_fr }}</td>
                                <td><button class="btn-link" data-toggle="tooltip" data-original-title="{{ $request->user->phone }}">{{ $request->user->fullname }}</button></td>
                                <td>{!! ($request->payment) ? '<a href="'.route('image', ['payments', $request->payment->payment]).'" class="image-link"><img src="'.route('image.thumbnail', ['payments', $request->payment->payment]).'" height="80"></a>' : '<img src="'.asset('assets/backend/img/status_0.png').'" />' !!}</td>
                                <td>{!! trans('hotels.statuses.'.$request->status.'.style') !!}</td>
                                <td>{{ $request->created_at }}</td>
                                <td><a href="{{ route('hotels.request', [$request->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('hotels.name') }}</th>
                            <th>{{ trans('cities.city') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('common.payment-reset') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.created-at') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/magnific-popup.css') }}">
@append

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    {{ Html::script('assets/frontend/js/jquery.magnific-popup.min.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();

            $('.image-link').magnificPopup({type:'image'});

            $("form").submit(function() {
                $(this).find(':input').filter(function() { return !this.value; }).attr('disabled', 'disabled');
                return true;
            });

            var countryID = $('#country_id').val();
            var cityID = $('#currCity').val();
            getCountryCities(countryID, cityID);

            $('#country_id').on('change', function () {
                countryID = $(this).val();
                getCountryCities(countryID, cityID);
            });

            $('#city_id').on('change', function () {
                $('#currCity').val($(this).val());
            });

            function getCountryCities(co,ct) {
                $.ajax({
                    url: '/backend/cities/country/'+co+'/'+ct,
                    method: 'GET',
                    success: function (response) {
                        if(response.citiesList != ''){
                            $('#city_id').html('<option value="" selected="selected">{{trans('common.all')}}</option>'+response.citiesList);
                        }else{
                            $('#city_id').html('<option value="" selected="selected">{{trans('common.all')}}</option>');
                        }
                    },
                    error: function () {
                        $('#city_id').html('<option value="" selected="selected">{{trans('common.all')}}</option>');
                    }
                });
            }
        });
    </script>
@stop