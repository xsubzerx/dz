@extends('backend.common.template')

@section('title'){{ $title }}@stop

@section('content')
    {{ (isset($hotelData)) ? Form::model($hotelData, array('url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('hotels.update', [$hotelData->id])), 'method' => 'PUT')) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('hotels.store'))]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif

            <div class="col-md-8">
                @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                    <div class='box box-info'>
                        <div class='box-header'>
                            <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class='box-body pad'>
                            <div class="form-group">
                                {{ Form::label('name_'.$localeCode, trans('hotels.name').':') }}
                                {{ Form::text('name_'.$localeCode, old('name_'.$localeCode), ['class' => 'form-control', 'placeholder' => trans('hotels.name'), 'required']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description_'.$localeCode, trans('hotels.description').':') }}
                                {{ Form::textarea('description_'.$localeCode, old('description_'.$localeCode), ['class' => 'form-control', 'placeholder' => trans('hotels.description')]) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('address_'.$localeCode, trans('hotels.address').':') }}
                                {{ Form::text('address_'.$localeCode, old('address_'.$localeCode), ['class' => 'form-control', 'placeholder' => trans('hotels.address')]) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('nearby_'.$localeCode, trans('hotels.nearby').':') }}
                                {{ Form::textarea('nearby_'.$localeCode, old('nearby_'.$localeCode), ['class' => 'form-control', 'placeholder' => trans('hotels.nearby')]) }}
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="box box-info">
                    <div class='box-header'>
                        <h3 class='box-title'><i class="fa fa-map"></i> {{ trans('hotels.map') }}</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="row">
                            <div class="col-md-12" style="height: 400px;">
                                {!! Mapper::render() !!}
                            </div>
                            {{ Form::hidden('map', old('map'), ['id' => 'map']) }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('country_id', trans('countries.title').':') }}
                            {{ Form::select('country_id', $countries, old('country_id'), ['id' => 'country_id', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('city_id', trans('cities.city').':') }}
                            {{ Form::select('city_id', [], old('city_id'), ['id' => 'city_id', 'class' => 'form-control']) }}
                            {{ Form::hidden('currCity', (isset($hotelData) && $hotelData->city_id) ? $hotelData->city_id : null, ['id' => 'currCity']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('stars', trans('hotels.stars').':') }}
                            {{ Form::select('stars', [1=>1,2=>2,3=>3,4=>4,5=>5], old('stars'), ['id' => 'stars', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('childes_phase1_age', trans('hotels.childes-phase1-age').':') }}
                            <div class="clearfix"></div>
                            <div class="col-md-5 col-sm-5 no-padding">
                                {{ Form::number('childes_phase1_age_start', 0, ['id' => 'childes_phase1_age_start', 'class' => 'form-control', 'min' => 0, 'readonly']) }}
                            </div>
                            <div class="col-md-2 col-sm-2"><strong>{{ trans('common.to') }}</strong></div>
                            <div class="col-md-5 col-sm-5 no-padding">
                                {{ Form::number('childes_phase1_age', old('childes_phase1_age'), ['id' => 'childes_phase1_age', 'class' => 'form-control', 'placeholder' => trans('hotels.childes-phase1-age'), 'min' => 0, 'required']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('childes_phase1_price', trans('hotels.childes-phase1-price').':') }}
                            {{ Form::text('childes_phase1_price', old('childes_phase1_price'), ['id' => 'childes_phase1_price', 'class' => 'form-control', 'placeholder' => trans('hotels.childes-phase1-price'), 'min' => 0, 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('childes_phase2_age', trans('hotels.childes-phase2-age').':') }}
                            <div class="clearfix"></div>
                            <div class="col-md-5 col-sm-5 no-padding">
                                {{ Form::number('childes_phase2_age_start', 0, ['id' => 'childes_phase2_age_start', 'class' => 'form-control', 'min' => 0, 'readonly']) }}
                            </div>
                            <div class="col-md-2 col-sm-2"><strong>{{ trans('common.to') }}</strong></div>
                            <div class="col-md-5 col-sm-5 no-padding">
                                {{ Form::number('childes_phase2_age', old('childes_phase2_age'), ['id' => 'childes_phase2_age', 'class' => 'form-control', 'min' => 0, 'placeholder' => trans('hotels.childes-phase2-age'), 'required']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('childes_phase2_price', trans('hotels.childes-phase2-price').':') }}
                            {{ Form::text('childes_phase2_price', old('childes_phase2_price'), ['id' => 'childes_phase2_price', 'class' => 'form-control', 'placeholder' => trans('hotels.childes-phase2-price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('amenities', trans('hotels.amenities').':') }}
                            <select name="amenities[]" id="amenities" class="form-control" multiple="multiple">
                                @foreach($amenities as $key=>$amenity)
                                    <option value="{{$key}}" @if(@in_array($key, json_decode($hotelData->amenities)))) selected="selected" @endif>{{ $amenity }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            {{ Form::label('status', trans('common.status').':') }}
                            {{ Form::select('status', ['1' => trans('common.active'),'0' => trans('common.deactive')], old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('featured', trans('common.featured').':') }}
                            {{ Form::select('featured', ['0' => trans('common.no'),'1' => trans('common.yes')], old('featured'), ['id' => 'featured', 'class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>
        </div>
    </div>
    {{ Form::close() }}
@stop

@section('scripts')
    <script src="//cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#amenities').select2();

            var countryID = $('#country_id').val();
            var cityID = $('#currCity').val();
            getCountryCities(countryID, cityID);
            $('#country_id').on('change', function () {
                countryID = $(this).val();
                getCountryCities(countryID, cityID);
            });

            function getCountryCities(co,ct) {
                $.ajax({
                    url: '/backend/cities/country/'+co+'/'+ct,
                    method: 'GET',
                    success: function (response) {
                        if(response.citiesList != ''){
                            $('#city_id').html(response.citiesList);
                        }
                    }
                });
            }

            var childes_phase1_age = $('#childes_phase1_age').val();
            $('#childes_phase2_age_start').val(parseInt(childes_phase1_age)+1);

            var childes_phase2_age = $('#childes_phase2_age').val();
            $('#childes_phase3_age_start').val(parseInt(childes_phase2_age)+1);

            $('#childes_phase1_age').on('change', function(){
                childes_phase1_age = $(this).val();
                $('#childes_phase2_age_start').val(parseInt(childes_phase1_age)+1);
                $('#childes_phase2_age').val(parseInt(childes_phase1_age)+1);
            });
        });
    </script>
    @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
        <script type="text/javascript">
            $(function() {
                CKEDITOR.replace('description_{{$localeCode}}', {language: "{{$localeCode}}"});
                CKEDITOR.replace('nearby_{{$localeCode}}', {language: "{{$localeCode}}"});
            });
        </script>
    @endforeach
@stop