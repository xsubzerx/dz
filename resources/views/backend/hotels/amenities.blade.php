@extends('backend.common.template')

@section('title'){{ trans('hotels.amenities') }}@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif

            <div class="col-md-8">
                <table id="example1" class="table table-bordered table-striped text-center">
                    <thead>
                    <tr>
                        <th>{{ trans('hotels.amenity') }}</th>
                        <th>{{ trans('hotels.icon') }}</th>
                        <th>{{ trans('common.delete') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($allAmenities as $amenity)
                        <tr>
                            <td>{{ $amenity->title_ar }}<br/>{{ $amenity->title_fr }}</td>
                            <td><i class="fa icon glyphicons {{$amenity->icon}}"></i></td>
                            <td>
                                {{ Form::open(['url' => route('amenities.destroy', [$amenity->id]), 'method' => 'DELETE']) }}
                                <button type="submit" class="btn btn-danger btn-sm" onclick="confirm('{{trans('common.delete-confirm')}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>{{ trans('hotels.amenity') }}</th>
                        <th>{{ trans('hotels.icon') }}</th>
                        <th>{{ trans('common.delete') }}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                    {{ Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('amenities.store'))]) }}
                        @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                            <div class="form-group">
                                {{ Form::label('title_'.$localeCode, trans('hotels.amenity').': ') }}{!! trans('common.'.$localeCode.'-flag') !!}
                                {{ Form::text('title_'.$localeCode, old('title_'.$localeCode), ['class' => 'form-control', 'placeholder' => trans('hotels.amenity')]) }}
                            </div>
                        @endforeach
                            <div class="form-group">
                                {{ Form::label('icon', trans('hotels.icon').': ') }}
                                {{ Form::text('icon', old('icon'), ['class' => 'form-control', 'placeholder' => trans('hotels.icon')]) }}
                                <p>Icons Reference <a href="http://fontawesome.io/icons/" target="_blank">HERE</a> Or <a href="http://ionicons.com/" target="_blank">HERE</a> OR <a href="https://getbootstrap.com/docs/3.3/components/" target="_blank">HERE</a></p>
                            </div>
                        {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
