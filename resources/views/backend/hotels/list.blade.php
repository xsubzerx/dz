@extends('backend.common.template')

@section('title'){{ trans('hotels.hotels')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            {{ Form::open(['method' => 'GET']) }}
            <div class="box box-success">
                <div class='box-header'>
                    <h3 class="box-title">{{ trans('common.filters')}}</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        {{ Form::submit(trans('common.filter'), ['id' => 'filter', 'class' => 'btn btn-sm btn-success']) }}
                        <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class='box-body pad'>
                    <div class="form-group col-md-4">
                        {{ Form::label('country_id', trans('countries.title').':', ['class' => 'col-md-4']) }}
                        <div class="col-md-8">
                            {{ Form::select('country_id', $countries, old('country_id'), ['id' => 'country_id', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        {{ Form::label('city_id', trans('cities.city').':', ['class' => 'col-md-4']) }}
                        <div class="col-md-8">
                            {{ Form::select('city_id', ['' => trans('common.all')], old('city_id'), ['id' => 'city_id', 'class' => 'form-control']) }}
                            {{ Form::hidden('currCity', old('city_id'), ['id' => 'currCity']) }}
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        {{ Form::label('status', trans('common.status').':', ['class' => 'col-md-4']) }}
                        <div class="col-md-8">
                            {{ Form::select('status', [''=>trans('common.all'), '1'=>trans('common.active'), '0'=>trans('common.deactive')], old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            {{ Form::close() }}

            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('hotels.name') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('cities.city') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('hotels.rooms') }}</th>
                            <th>{{ trans('hotels.album') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allHotels as $hotel)
                            <tr>
                                <td>{{ $hotel->name_ar }}<br/>{{ $hotel->name_fr }}</td>
                                <td>{{ $hotel->city->country->name_ar }}<br/>{{ $hotel->city->country->name_fr }}</td>
                                <td>{{ $hotel->city->name_ar }}<br/>{{ $hotel->city->name_fr }}</td>
                                <td><img src="{{ asset('assets/backend/img/status_'.$hotel->status.'.png') }}" /></td>
                                <td><a href="{{ route('rooms.index', [$hotel->id]) }}"><img src="{{ asset('assets/backend/img/room.png') }}"></a></td>
                                <td><a href="{{ route('hotels.album', [$hotel->id]) }}"><i class="fa fa-picture-o" aria-hidden="true"></i></a></td>
                                <td><a href="{{ route('hotels.edit', [$hotel->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                <td>
                                    {{ Form::open(['url' => route('hotels.destroy', [$hotel->id]), 'method' => 'DELETE']) }}
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="confirm('{{trans('common.delete-confirm')}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('hotels.name') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('cities.city') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('hotels.rooms') }}</th>
                            <th>{{ trans('hotels.album') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();

            $("form").submit(function() {
                $(this).find(':input').filter(function() { return !this.value; }).attr('disabled', 'disabled');
                return true;
            });

            var countryID = $('#country_id').val();
            var cityID = $('#currCity').val();
            getCountryCities(countryID, cityID);

            $('#country_id').on('change', function () {
                countryID = $(this).val();
                getCountryCities(countryID, cityID);
            });

            $('#city_id').on('change', function () {
                $('#currCity').val($(this).val());
            });

            function getCountryCities(co,ct) {
                $.ajax({
                    url: '/backend/cities/country/'+co+'/'+ct,
                    method: 'GET',
                    success: function (response) {
                        if(response.citiesList != ''){
                            $('#city_id').html('<option value="" selected="selected">{{trans('common.all')}}</option>'+response.citiesList);
                        }else{
                            $('#city_id').html('<option value="" selected="selected">{{trans('common.all')}}</option>');
                        }
                    },
                    error: function () {
                        $('#city_id').html('<option value="" selected="selected">{{trans('common.all')}}</option>');
                    }
                });
            }
        });
    </script>
@stop