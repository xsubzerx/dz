@extends('backend.common.template')

@section('title'){{ trans('hotels.hotel-custom-requests')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="col-md-12">
                {{ Form::open(['url' => route('hotels.request.custom.edit'), 'class' => 'form-horizontal']) }}
                    <label class="col-md-3 text-center">{{ trans('common.update').' '.trans('common.status') }}</label>
                    <div class="col-md-6">
                        {{ Form::select('status', ['pending'=>trans('flights.statuses.pending.label') ,'confirmed'=>trans('flights.statuses.confirmed.label') ,'canceled'=>trans('flights.statuses.canceled.label')], $request->status, ['class' => 'form-control']) }}
                    </div>
                    {{ Form::hidden('request', $request->id) }}
                    {{ Form::submit(trans('common.update'), ['class' => 'btn btn-primary']) }}
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-12">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
            </div>
            <div class="col-md-8">
                <div class="box box-info">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                            <tr>
                                <th>{{ trans('hotels.destination') }}</th>
                                <th>{{ trans('hotels.hotel') }}</th>
                                <th>{{ trans('hotels.checkin') }}</th>
                                <th>{{ trans('hotels.checkout') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $request->destination }}</td>
                                <td>{{ $request->hotel }}</td>
                                <td>{{ $request->checkin }}</td>
                                <td>{{ $request->checkout }}</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('hotels.destination') }}</th>
                                <th>{{ trans('hotels.hotel') }}</th>
                                <th>{{ trans('hotels.checkin') }}</th>
                                <th>{{ trans('hotels.checkout') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('hotels.note') }}</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        {!! $request->note !!}
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <table class="table">
                            <tr>
                                <th>{{ trans('common.request-code') }}</th>
                                <td>{{ $request->code }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('hotels.budget') }}</th>
                                <td>{{ $request->budget }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('hotels.rooms-count') }}</th>
                                <td>{{ $request->rooms_count }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('hotels.adults-count') }}</th>
                                <td>{{ $request->adults_count }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('hotels.childes-count') }}</th>
                                <td>{{ $request->childes_count }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('hotels.childes-ages') }}</th>
                                <td>@if(is_array($request->childes_ages_list0) && count($request->childes_ages_list))({{ implode(', ', $request->childes_ages_list) }})@endif</td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.status') }}</th>
                                <td>{!! trans('flights.statuses.'.$request->status.'.style') !!}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.created-at') }}</th>
                                <td>{{ $request->created_at }}</td>
                            </tr>
                        </table>
                    </div>
                </div>

                {{ Form::open(['url' => route('hotels.request.custom.edit'), 'class' => 'form-horizontal']) }}
                <div class="box box-success">
                    <div class='box-header'>
                        <h3 class="box-title">{{ trans('common.admin-comment') }}</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body'>
                        <div>
                            {{ Form::textarea('admin_comment', $request->admin_comment, ['id' => 'admin_comment', 'class' => 'form-control']) }}
                        </div>
                        <div class="clearfix"></div><br>
                        {{ Form::hidden('request', $request->id) }}
                        {{ Form::submit(trans('common.update'), array('class' => 'btn btn-primary col-md-12')) }}
                        <div class="clearfix"></div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop