@extends('backend.common.template')

@section('title'){{ trans('hotels.hotels')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="col-md-12">
                {{ Form::open(['url' => route('hotels.request.edit'), 'class' => 'form-horizontal']) }}
                    <label class="col-md-3 text-center">{{ trans('common.update').' '.trans('common.status') }}</label>
                    <div class="col-md-6">
                        {{ Form::select('status', ['pending'=>trans('hotels.statuses.pending.label'), 'progress'=>trans('hotels.statuses.progress.label') ,'confirmed'=>trans('hotels.statuses.confirmed.label') ,'canceled'=>trans('hotels.statuses.canceled.label')], $request->status, ['class' => 'form-control']) }}
                    </div>
                    {{ Form::hidden('request', $request->id) }}
                    {{ Form::submit(trans('common.update'), ['class' => 'btn btn-primary']) }}
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-12">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
            </div>
            <div class="col-md-8">
                <div class="box box-info">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                            <tr>
                                <th>{{ trans('hotels.name') }}</th>
                                <th>{{ trans('hotels.title') }}</th>
                                <th>{{ trans('hotels.price') }}</th>
                                <th>{{ trans('hotels.quantity') }}</th>
                                <th>{{ trans('hotels.total') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($request->hotel->rooms))
                                @foreach($request->hotel->rooms as $room)
                                    @if(!in_array($room->id, array_keys(json_decode($request->rooms, true)))) @continue @endif
                                    <tr>
                                        <td>{{ $request->hotel->name_ar }}<br/>{{ $request->hotel->name_fr }}</td>
                                        <td>{{ $room->title_ar }}<br/>{{ $room->title_fr }}</td>
                                        <td>{{ $room->price }}</td>
                                        <td>{{ json_decode($request->rooms, true)[$room->id] }}</td>
                                        <td>{{ $room->price * json_decode($request->rooms, true)[$room->id] }}</td>
                                    </tr>
                                    @if($request->rooms_members && key_exists($room->id, collect($request->rooms_members)->all()))
                                    <tr>
                                        <td colspan="5">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>{{ trans('users.fullname') }}</th>
                                                    <th>{{ trans('users.nickname') }}</th>
                                                    <th>{{ trans('users.birthdate') }}</th>
                                                </tr>
                                                @for($i=0;$i<count($request->rooms_members->{$room->id}->birthdate);$i++)
                                                <tr>
                                                    <td>{{ $request->rooms_members->{$room->id}->name[$i] }}</td>
                                                    <td>{{ $request->rooms_members->{$room->id}->nickname[$i] }}</td>
                                                    <td>{{ $request->rooms_members->{$room->id}->birthdate[$i] }}</td>
                                                </tr>
                                                @endfor
                                            </table>
                                        </td>
                                        <td style="display: none"></td>
                                        <td style="display: none"></td>
                                        <td style="display: none"></td>
                                        <td style="display: none"></td>
                                    </tr>
                                    @endif
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('hotels.name') }}</th>
                                <th>{{ trans('hotels.title') }}</th>
                                <th>{{ trans('hotels.price') }}</th>
                                <th>{{ trans('hotels.quantity') }}</th>
                                <th>{{ trans('hotels.total') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <table class="table">
                            <tr>
                                <th>{{ trans('common.request-code') }}</th>
                                <td>{{ $request->code }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('hotels.checkin') }}</th>
                                <td>{{ $request->checkin }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('hotels.checkout') }}</th>
                                <td>{{ $request->checkout }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('hotels.childes-count') }}</th>
                                <td>{{ count(json_decode($request->childes)) }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('hotels.childes-ages') }}</th>
                                <td>
                                    @if(count(json_decode($request->childes)))
                                        @foreach(json_decode($request->childes) as $k=>$child)
                                            @if($k>0) , @endif{{ $child }}
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>{{ trans('hotels.total') }}</th>
                                <td>{{ $request->total.' '.trans('common.dzd') }}</td>
                            </tr>
                        </table>
                    </div>
                </div>

                {{ Form::open(['url' => route('hotels.request.edit'), 'class' => 'form-horizontal']) }}
                <div class="box box-success">
                    <div class='box-header'>
                        <h3 class="box-title">{{ trans('common.admin-comment') }}</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body'>
                        <div>
                            {{ Form::textarea('admin_comment', $request->admin_comment, ['id' => 'admin_comment', 'class' => 'form-control']) }}
                        </div>
                        <div class="clearfix"></div><br>
                        {{ Form::hidden('request', $request->id) }}
                        {{ Form::submit(trans('common.update'), array('class' => 'btn btn-primary col-md-12')) }}
                        <div class="clearfix"></div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/magnific-popup.css') }}">
@append

@section('scripts')
    {{ Html::script('assets/frontend/js/jquery.magnific-popup.min.js') }}
    <script type="text/javascript">
        $(function() {
            $('.image-link').magnificPopup({type:'image'});
        });
    </script>
@stop