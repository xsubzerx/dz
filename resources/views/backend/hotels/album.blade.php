@extends('backend.common.template')

@section('title', trans('hotels.album'))

@section('styles')
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/frontend/css/dropzone.min.css') }}">
@append

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif

            <div class="col-md-12">
                <div class="box box-info">
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        {{ Form::open(['class' => 'dropzone', 'id' => 'dropzone']) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/frontend/js/dropzone.min.js') }}"></script>

    <script type="text/javascript">
        Dropzone.options.dropzone = {
            paramName: "image",
            maxFilesize: 2,
            addRemoveLinks: true,
            removedfile: function (file) {
                var name = file.name;
                $.ajax({
                    url: '/{{LaraLocale::getCurrentLocale()}}/backend/hotels/photo/'+name,
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
            init: function () {
                var thisDropzone = this;
                $.get('{{route('hotels.album', [$hID])}}', function (data) {
                    if (data == null) {
                        return;
                    }
                    $.each(data, function (key, value) {
                        var mockFile = { name: key, size: value.size };
                        thisDropzone.emit("addedfile", mockFile);
                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, '/image/hotels/thumbnail-' + key);
                        thisDropzone.emit("complete", mockFile);

                        var featuredBtn = document.createElement('a');
                        featuredBtn.setAttribute('href',"#");
                        featuredBtn.setAttribute('class', 'dz-featured');
                        if(value.featured > 0){
                            featuredBtn.innerHTML = '<i class="fa fa-star"></i>';
                        }else{
                            featuredBtn.innerHTML = '<i class="fa fa-star-o"></i>';
                        }
                        featuredBtn.setAttribute('data-filename', key);
                        mockFile.previewTemplate.appendChild(featuredBtn);
                    });
                });
            },
            success: function (file) {
                var featuredBtn = document.createElement('a');
                featuredBtn.setAttribute('href',"#");
                featuredBtn.setAttribute('data-filename', file.fullname);
                featuredBtn.setAttribute('class', 'dz-featured');
                featuredBtn.innerHTML = '<i class="fa fa-star-o"></i>';
                file.previewTemplate.appendChild(featuredBtn);
            }
        };

        $(document).ready(function () {
            $(document).on('click', '.dz-featured', function (e) {
                e.preventDefault();
                var thisItm = $(this);
                var filename = $(this).attr('data-filename');
                var url = '{{ route('hotels.album.featured', [':filename']) }}';
                $.get(url.replace(':filename', filename), function (data) {
                    if(data.success){
                        $('.dz-featured i').removeClass('fa-star').addClass('fa-star-o');
                        thisItm.children('i').removeClass('fa-star-o').addClass('fa-star');
                    }
                });
            });
        });
    </script>
@stop