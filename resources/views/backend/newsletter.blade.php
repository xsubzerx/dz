@extends('backend.common.template')

@section('title', trans('contact.newsletter'))

@section('content')

    <div class="row">
        <!-- left column -->
        {{ Form::open() }}
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
        @endif

        <!-- left column -->
            <div class="col-md-8">
                <div class='box box-info'>
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('subject', trans('contact.subject').':') }}
                            {{ Form::text('subject', old('subject'), ['class' => 'form-control', 'placeholder' => trans('contact.subject'), 'required' => true]) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('message', trans('contact.message').':') }}
                            {{ Form::textarea('message', old('message'), ['class' => 'form-control editor', 'placeholder' => trans('contact.message'), 'required' => true]) }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('receiver', trans('contact.receiver').':') }}
                            {{ Form::select('receiver', ['all' => trans('common.all'), 'users' => trans('users.users'), 'agencies' => trans('users.agencies')], old('receiver'), ['id' => 'receiver', 'class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                {{ Form::submit(trans('contact.send'), array('class' => 'btn btn-primary')) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>

@stop

@section('scripts')
    <script src="//cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
    <script type="text/javascript">
        $(function() {
            CKEDITOR.replace('message');
        });
    </script>
@stop