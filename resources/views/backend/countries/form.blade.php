@extends('backend.common.template')

@section('title', $title)

@section('content')

    <div class="row">
        <!-- left column -->
        {{ (isset($countryData)) ? Form::model($countryData, array('url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('countries.update', [$countryData->id])), 'method' => 'PUT', 'files'=> true)) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('countries.store')), 'files'=> true]) }}
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif

            <!-- left column -->
            <div class="col-md-8">
                @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                    <div class='box box-info'>
                        <div class='box-header'>
                            <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class='box-body pad'>
                            <div class="form-group">
                                {{ Form::label('name_'.$localeCode, trans('countries.title').':') }}
                                {{ Form::text('name_'.$localeCode, old('name_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('countries.title'))) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('status', trans('common.status').':') }}
                            {{ Form::select('status', array('1' => trans('common.active'),'0' => trans('common.deactive')), old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('image', trans('countries.flag').':') }}
                            {{ Form::file('image', array('class' => 'form-control', 'placeholder' => trans('countries.flag'))) }}
                            <div class="clearfix"></div>
                            <hr>
                            <img src="{{ (isset($countryData) && $countryData->flag) ? route('image.thumbnail', ['countries', $countryData->flag]) : asset('assets/backend/img/no-image-found.jpg') }}" class="img-thumbnail img-responsive center-block" height="100" />
                        </div>
                    </div>
                </div>

                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
    
@stop