@extends('backend.common.template')

@section('title'){{ trans('visas.documents') }}@stop

@section('content')
    {{ (isset($docData)) ? Form::model($docData, array('url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('documents.update', [$docData->id])), 'method' => 'PUT', 'files' => true)) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('documents.store')), 'files' => true]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif

            <div class="col-md-8">
                @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                    <div class='box box-info'>
                        <div class='box-header'>
                            <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class='box-body pad'>
                            <div class="form-group">
                                {{ Form::label('title_'.$localeCode, trans('visas.doc-title').':') }}
                                {{ Form::text('title_'.$localeCode, old('title_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('visas.doc-title'))) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('has_app', trans('visas.has-app').':') }}
                            {{ Form::select('has_app', array('0' => trans('common.no'),'1' => trans('common.yes')), old('has_app'), ['id' => 'has_app', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group @if(!isset($docData) || !$docData->has_app) hidden @endif" id="application">
                            {{ Form::label('image', trans('visas.application').':') }}
                            {{ Form::file('image', array('class' => 'form-control', 'placeholder' => trans('visas.application'))) }}
                            <br>
                            <hr>
                            <img src="{{ (isset($docData) && $docData->application) ? route('image.thumbnail', ['visas', $docData->application]) : asset('assets/backend/img/no-image-found.jpg') }}" class="img-thumbnail img-responsive center-block" height="100" />
                        </div>
                    </div>
                </div>
                {{ Form::hidden('visa_id', $visa_id) }}
                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>
        </div>
    </div>
    {{ Form::close() }}
@stop

@section('scripts')
    <script type="text/javascript">
        $(function() {
            $('select[name="has_app"]').on('change', function () {
                if($(this).val() > 0)
                    $('#application').removeClass('hidden');
                else
                    $('#application').addClass('hidden');
            });
        });
    </script>
@stop