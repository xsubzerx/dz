@extends('backend.common.template')

@section('title'){{ trans('pages.pages-list') }} @stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg'))
                       {!! Session::get('msg') !!}
                    @endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                            <tr>
                                <th>{{ trans('pages.title') }}</th>
                                <th>{{ trans('pages.image') }}</th>
                                <th>{{ trans('common.status') }}</th>
                                <th>{{ trans('common.update') }}</th>
                                <th>{{ trans('common.delete') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($allPages as $page)
                            <tr>
                                <td>{{ $page->title_ar }}<br/>{{ $page->title_fr }}</td>
                                <td><img src="{{ ($page->image) ? route('image.thumbnail', ['pages', $page->image] ) : asset('assets/admin/img/no-image-found.jpg') }}" height="60" /></td>
                                <td><img src="{{ asset('assets/backend/img/status_'.$page->status.'.png') }}" /></td>
                                <td><a href="{{ route('pages.edit', [$page->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                <td>
                                    {{ Form::open(['url' => route('pages.destroy', [$page->id]), 'method' => 'DELETE']) }}
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="confirm('{{trans('common.delete-confirm')}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('pages.title') }}</th>
                            <th>{{ trans('pages.image') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
        });
    </script>
@stop