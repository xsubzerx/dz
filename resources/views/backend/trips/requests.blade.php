@extends('backend.common.template')

@section('title'){{ trans('trips.trips')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('trips.title') }}</th>
                            <th>{{ trans('cities.city') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('common.payment-reset') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allRequests as $request)
                            <tr>
                                <td>{{ $request->code }}</td>
                                <td><a href="{{ route('trips.edit', $request->trip->id) }}" target="_blank">{{ $request->trip->title_ar }}<br/>{{ $request->trip->title_fr }}</a></td>
                                <td>{{ @$request->trip->city->name_ar }}<br/>{{ @$request->trip->city->name_fr }}</td>
                                <td>{{ $request->trip->country->name_ar }}<br/>{{ $request->trip->country->name_fr }}</td>
                                <td><button class="btn-link" data-toggle="tooltip" data-original-title="{{ $request->user->phone }}">{{ $request->user->fullname }}</button></td>
                                <td>{!! ($request->payment) ? '<a href="'.route('image', ['payments', $request->payment->payment]).'" class="image-link"><img src="'.route('image.thumbnail', ['payments', $request->payment->payment]).'" height="80"></a>' : '<img src="'.asset('assets/backend/img/status_0.png').'" />' !!}</td>
                                <td>{!! trans('trips.statuses.'.$request->status.'.style') !!}</td>
                                <td><a href="{{ route('trips.request', [$request->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('trips.title') }}</th>
                            <th>{{ trans('cities.city') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('common.payment-reset') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/magnific-popup.css') }}">
@append

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    {{ Html::script('assets/frontend/js/jquery.magnific-popup.min.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();

            $('.image-link').magnificPopup({type:'image'});
        });
    </script>
@stop