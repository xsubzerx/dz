@extends('backend.common.template')

@section('title'){{ trans('trips.trips')}} @stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            {{ Form::open(['url' => route('trips.request.edit'), 'class' => 'form-horizontal']) }}
            <label class="col-md-3 text-center">{{ trans('common.update').' '.trans('common.status') }}</label>
            <div class="col-md-6">
                {{ Form::select('status', ['pending'=>trans('trips.statuses.pending.label'), 'progress'=>trans('trips.statuses.progress.label') ,'confirmed'=>trans('trips.statuses.confirmed.label'), 'canceled'=>trans('trips.statuses.canceled.label')], $request->status, ['class' => 'form-control']) }}
            </div>
            {{ Form::hidden('request', $request->id) }}
            {{ Form::submit(trans('common.update'), ['class' => 'btn btn-primary']) }}
            {{ Form::close() }}
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-xs-8">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('hotels.name') }}</th>
                            <th>{{ trans('cities.city') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('hotels.quantity') }}</th>
                            <th>{{ trans('hotels.childes-count') }}</th>
                            <th>{{ trans('common.price') }}</th>
                            <th>{{ trans('common.created-at') }}</th>
                            <th>{{ trans('common.status') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $request->code }}</td>
                            <td>{{ $request->user->fullname }}</td>
                            <td>{{ $request->trip->title_ar }}<br/>{{ $request->trip->title_fr }}</td>
                            <td>{{ @$request->trip->city->name_ar }}<br/>{{ @$request->trip->city->name_fr }}</td>
                            <td>{{ @$request->trip->country->name_ar }}<br/>{{ @$request->country->city->name_fr }}</td>
                            <td>
                                @if($request->single)<p>( {{ $request->single }} ) {{ trans('trips.single-room') }}</p>@endif
                                @if($request->double)<p>( {{ $request->double }} ) {{ trans('trips.double-room') }}</p>@endif
                                @if($request->triple)<p>( {{ $request->triple }} ) {{ trans('trips.triple-room') }}</p>@endif
                                @if($request->quad)<p>( {{ $request->quad }} ) {{ trans('trips.quad-room') }}</p>@endif
                                @if($request->quinary)<p>( {{ $request->quinary }} ) {{ trans('trips.quinary-room') }}</p>@endif
                            </td>
                            <td>
                                @if($request->single)<p>( {{ $request->childes_first }} ) {{ trans('trips.childes-first') }}</p>@endif
                                @if($request->single)<p>( {{ $request->childes_second }} ) {{ trans('trips.childes-second') }}</p>@endif
                                @if($request->single)<p>( {{ $request->childes_third }} ) {{ trans('trips.childes-third') }}</p>@endif
                            </td>
                            <td>{{ $request->total.' '.trans('common.dzd') }}</td>
                            <td>{{ $request->created_at }}</td>
                            <td>{!! trans('trips.statuses.'.$request->status.'.style') !!}</td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('hotels.name') }}</th>
                            <th>{{ trans('cities.city') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('hotels.quantity') }}</th>
                            <th>{{ trans('hotels.childes-count') }}</th>
                            <th>{{ trans('common.price') }}</th>
                            <th>{{ trans('common.created-at') }}</th>
                            <th>{{ trans('common.status') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>

        <div class="col-xs-4">
            {{ Form::open(['url' => route('trips.request.edit'), 'class' => 'form-horizontal']) }}
            <div class="box box-success">
                <div class='box-header'>
                    <h3 class="box-title">{{ trans('common.admin-comment') }}</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div>
                        {{ Form::textarea('admin_comment', $request->admin_comment, ['id' => 'admin_comment', 'class' => 'form-control']) }}
                    </div>
                    <div class="clearfix"></div><br>
                    {{ Form::hidden('request', $request->id) }}
                    {{ Form::submit(trans('common.update'), array('class' => 'btn btn-primary col-md-12')) }}
                    <div class="clearfix"></div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>

@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/magnific-popup.css') }}">
@append

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    {{ Html::script('assets/frontend/js/jquery.magnific-popup.min.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();

            $('.image-link').magnificPopup({type:'image'});
        });
    </script>
@stop