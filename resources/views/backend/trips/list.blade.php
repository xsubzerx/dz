@extends('backend.common.template')

@section('title'){{ trans('trips.trips')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            {{ Form::open(['method' => 'GET']) }}
            <div class="box box-success">
                <div class='box-header'>
                    <h3 class="box-title">{{ trans('common.filters')}}</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        {{ Form::submit(trans('common.filter'), ['id' => 'filter', 'class' => 'btn btn-sm btn-success']) }}
                        <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class='box-body pad'>
                    <div class="form-group col-md-3">
                        {{ Form::label('country_id', trans('countries.title').':', ['class' => 'col-md-4']) }}
                        <div class="col-md-8">
                            {{ Form::select('country_id', $countries, old('country_id'), ['id' => 'country_id', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('city_id', trans('cities.city').':', ['class' => 'col-md-4']) }}
                        <div class="col-md-8">
                            {{ Form::select('city_id', ['' => trans('common.all')], old('city_id'), ['id' => 'city_id', 'class' => 'form-control']) }}
                            {{ Form::hidden('currCity', old('city_id'), ['id' => 'currCity']) }}
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('status', trans('common.status').':', ['class' => 'col-md-4']) }}
                        <div class="col-md-8">
                            {{ Form::select('status', [''=>trans('common.all'), '1'=>trans('common.active'), '0'=>trans('common.deactive')], old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('owner', trans('trips.owner').':', ['class' => 'col-md-4']) }}
                        <div class="col-md-8">
                            {{ Form::select('owner', $agencies, old('owner'), ['id' => 'owner', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            {{ Form::close() }}

            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('trips.title') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('cities.city') }}</th>
                            <th>{{ trans('trips.album') }}</th>
                            <th>{{ trans('trips.owner') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.featured') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allTrips as $trip)
                            <tr>
                                <td>{{ $trip->title_ar }}<br/>{{ $trip->title_fr }}</td>
                                <td>{{ $trip->country->name_ar }}<br/>{{ $trip->country->name_fr }}</td>
                                <td>{{ @$trip->city->name_ar }}<br/>{{ @$trip->city->name_fr }}</td>
                                <td><a href="{{ route('trips.album', [$trip->id]) }}"><i class="fa fa-picture-o" aria-hidden="true"></i></a></td>
                                <td>{!! ($trip->created_by > 0) ? '<a href="'.route('users.edit', @$trip->owner->id).'" target="_blank">'.@$trip->owner->firstname.'</a>' : $config->site_name !!}</td>
                                <td><img src="{{ asset('assets/backend/img/status_'.$trip->status.'.png') }}" /></td>
                                <td><img src="{{ asset('assets/backend/img/status_'.$trip->featured.'.png') }}" /></td>
                                <td><a href="{{ route('trips.edit', [$trip->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                <td>
                                    {{ Form::open(['url' => route('trips.destroy', [$trip->id]), 'method' => 'DELETE']) }}
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="confirm('{{trans('common.delete-confirm')}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('trips.title') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('cities.city') }}</th>
                            <th>{{ trans('trips.album') }}</th>
                            <th>{{ trans('trips.owner') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.featured') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();

            $("form").submit(function() {
                $(this).find(':input').filter(function() { return !this.value; }).attr('disabled', 'disabled');
                return true;
            });

            var countryID = $('#country_id').val();
            var cityID = $('#currCity').val();
            getCountryCities(countryID, cityID);

            $('#country_id').on('change', function () {
                countryID = $(this).val();
                getCountryCities(countryID, cityID);
            });

            $('#city_id').on('change', function () {
                $('#currCity').val($(this).val());
            });

            function getCountryCities(co,ct) {
                $.ajax({
                    url: '/backend/cities/country/'+co+'/'+ct,
                    method: 'GET',
                    success: function (response) {
                        if(response.citiesList != ''){
                            $('#city_id').html('<option value="" selected="selected">{{trans('common.all')}}</option>'+response.citiesList);
                        }else{
                            $('#city_id').html('<option value="" selected="selected">{{trans('common.all')}}</option>');
                        }
                    },
                    error: function () {
                        $('#city_id').html('<option value="" selected="selected">{{trans('common.all')}}</option>');
                    }
                });
            }
        });
    </script>
@stop