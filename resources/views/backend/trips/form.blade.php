@extends('backend.common.template')

@section('title'){{ $title }}@stop

@section('content')
    {{ (isset($tripData)) ? Form::model($tripData, ['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('trips.update', [$tripData->id])), 'method' => 'PUT', 'files' => true]) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('trips.store')), 'files' => true]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif

            <div class="col-md-8">
                @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                    <div class='box box-info'>
                        <div class='box-header'>
                            <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class='box-body pad'>
                            <div class="form-group">
                                {{ Form::label('title_'.$localeCode, trans('trips.title').':') }}
                                {{ Form::text('title_'.$localeCode, old('title_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('trips.title'), 'required')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description_'.$localeCode, trans('trips.description').':') }}
                                {{ Form::textarea('description_'.$localeCode, old('description_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('trips.description'))) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('has_'.$localeCode, trans('trips.has').':') }}
                                {{ Form::textarea('has_'.$localeCode, old('has_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('trips.has'))) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('has_not_'.$localeCode, trans('trips.has-not').':') }}
                                {{ Form::textarea('has_not_'.$localeCode, old('has_not_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('trips.has-not'))) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('country_id', trans('countries.title').':') }}
                            {{ Form::select('country_id', $countries, old('country_id'), ['id' => 'country_id', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('city_id', trans('cities.city').':') }}
                            {{ Form::select('city_id', [], old('city_id'), ['id' => 'city_id', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('single_price', trans('trips.single-price').':') }}
                            {{ Form::text('single_price', old('single_price'), ['class' => 'form-control', 'placeholder' => trans('trips.single-price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('double_price', trans('trips.double-price').':') }}
                            {{ Form::text('double_price', old('double_price'), ['class' => 'form-control', 'placeholder' => trans('trips.double-price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('triple_price', trans('trips.triple-price').':') }}
                            {{ Form::text('triple_price', old('triple_price'), ['class' => 'form-control', 'placeholder' => trans('trips.triple-price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('quad_price', trans('trips.quad-price').':') }}
                            {{ Form::text('quad_price', old('quad_price'), ['class' => 'form-control', 'placeholder' => trans('trips.quad-price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('quinary_price', trans('trips.quinary-price').':') }}
                            {{ Form::text('quinary_price', old('quinary_price'), ['class' => 'form-control', 'placeholder' => trans('trips.quinary-price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('childes_first_price', trans('trips.childes-first-price').':') }}
                            {{ Form::text('childes_first_price', old('childes_first_price'), ['class' => 'form-control', 'placeholder' => trans('trips.childes-first-price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('childes_second_price', trans('trips.childes-second-price').':') }}
                            {{ Form::text('childes_second_price', old('childes_second_price'), ['class' => 'form-control', 'placeholder' => trans('trips.childes-second-price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('childes_third_price', trans('trips.childes-third-price').':') }}
                            {{ Form::text('childes_third_price', old('childes_third_price'), ['class' => 'form-control', 'placeholder' => trans('trips.childes-third-price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('commission', trans('trips.commission').':') }}
                            {{ Form::text('commission', old('commission'), ['class' => 'form-control', 'placeholder' => trans('trips.commission')]) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('fixed', trans('trips.fixed').':') }}
                            {{ Form::select('fixed', array('0' => trans('common.no'),'1' => trans('common.yes')), old('fixed'), ['id' => 'fixed', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group fixedTrip">
                            {{ Form::label('start', trans('trips.start').':') }}
                            {{ Form::text('start', old('start'), array('class' => 'form-control datepicker', 'id' => 'start', 'placeholder' => trans('trips.start'))) }}
                        </div>
                        <div class="form-group fixedTrip">
                            {{ Form::label('end', trans('trips.end').':') }}
                            {{ Form::text('end', old('end'), array('class' => 'form-control datepicker', 'id' => 'end', 'placeholder' => trans('trips.end'))) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('status', trans('common.status').':') }}
                            {{ Form::select('status', array('1' => trans('common.active'),'0' => trans('common.deactive')), old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('featured', trans('common.featured').':') }}
                            {{ Form::select('featured', array('0' => trans('common.no'),'1' => trans('common.yes')), old('featured'), ['id' => 'featured', 'class' => 'form-control']) }}
                        </div>
                        @if(isset($tripData))
                        <div class="form-group">
                            {{ Form::label('owner', trans('trips.owner').':') }}
                            <h3>{!! ($tripData->created_by > 0) ? '<a href="'.route('users.edit', @$tripData->owner->id).'" target="_blank">'.@$tripData->owner->firstname.'</a>' : $config->site_name !!}</h3>
                        </div>
                        @endif
                    </div>
                </div>

                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>
        </div>
    </div>
    {{ Form::close() }}
@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/backend/css/datepicker/datepicker.css') }}" />
@append

@section('scripts')
    <script src="{{ asset('assets/backend/js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
    @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
        <script type="text/javascript">
            $(function() { CKEDITOR.replace('description_{{$localeCode}}', {language: "{{$localeCode}}"}); });
            $(function() { CKEDITOR.replace('has_{{$localeCode}}', {language: "{{$localeCode}}"}); });
            $(function() { CKEDITOR.replace('has_not_{{$localeCode}}', {language: "{{$localeCode}}"}); });
        </script>
    @endforeach
    <script type="text/javascript">
        $(document).ready(function() {
            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

            var start = $('#start').datepicker({
                onRender: function(date) {return date.valueOf() < now.valueOf() ? 'disabled' : '';},
                format: 'yyyy-mm-dd'
            }).on('changeDate', function(ev) {
                if (ev.date.valueOf() > end.date.valueOf()) {
                    var newDate = new Date(ev.date)
                    newDate.setDate(newDate.getDate() + 1);
                    end.setValue(newDate);
                }
                start.hide();
                $('#end')[0].focus();
            }).data('datepicker');

            var end = $('#end').datepicker({
                onRender: function(date) {return date.valueOf() <= start.date.valueOf() ? 'disabled' : '';},
                format: 'yyyy-mm-dd'
            }).on('changeDate', function(ev) {
                end.hide();
            }).data('datepicker');




            $('#fixed').on('change', function(){
                if($(this).val() > 0){
                    $('.fixedTrip').fadeOut();
                }else{
                    $('.fixedTrip').fadeIn();
                }
            });

            var countryID = $('#country_id').val();
            var cityID = $('#city_id').val();
            getCountryCities(countryID, cityID);
            $('#country_id').on('change', function () {
                countryID = $(this).val();
                getCountryCities(countryID, cityID);
            });

            function getCountryCities(co,ct) {
                $.ajax({
                    url: '/backend/cities/country/'+co+'/'+ct,
                    method: 'GET',
                    success: function (response) {
                        if(response.citiesList != ''){
                            $('#city_id').html('<option value="">{{trans('common.all')}}</option>'+response.citiesList);
                        }
                    }
                });
            }
        });
    </script>
@stop