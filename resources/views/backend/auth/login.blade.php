<!DOCTYPE html>
<html class="bg-black">
<head>
    <title>{{ trans('auth.cpanel-login') }}</title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- bootstrap 3.0.2 -->
    <link rel="stylesheet" href="{{ asset('assets/backend/css/bootstrap.min.css') }}" />
    @if(LaraLocale::getCurrentLocale() == 'ar')
        <link rel="stylesheet" href="{{ asset('assets/backend/css/bootstrap-rtl.min.css') }}" />
    @endif
    <!-- font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/backend/css/font-awesome.min.css') }}" />
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/backend/css/AdminLTE-rtl.css') }}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script>window.Laravel = <?php echo json_encode([ 'csrfToken' => csrf_token(), ]); ?></script>
</head>
<body class="bg-black">
<div class="form-box" id="login-box">
    <div class="header">{{ trans('auth.signin') }}</div>
    <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.login') }}">
        {{ csrf_field() }}
        <div class="body bg-gray">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
                @if ($errors->has('email'))
                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control" name="password">
                @if ($errors->has('password'))
                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                @endif
            </div>
            <div class="form-group">
                <input type="checkbox" name="remember"/> {{ trans('auth.remember') }}
            </div>
        </div>
        <div class="footer">
            <button type="submit" class="btn bg-olive btn-block">{{ trans('auth.sign-me-in') }}</button>
        </div>
    </form>
</div>
<br />
@if(!empty($errors->all()))
    <ul class="alert alert-danger alert-dismissable" style="margin: auto;max-width: 600px;">
        <i class="fa fa-ban"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
        @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!} @endforeach
    </ul>
@endif

<!-- jQuery 2.0.2 -->
<script src="{{ asset('assets/backend/js/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('assets/backend/js/bootstrap.min.js') }}"></script>
</body>
</html>