@extends('backend.common.template')

@section('title'){{ trans('flights.flights')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="col-md-12">
                {{ Form::open(['url' => route('flights.request.edit'), 'class' => 'form-horizontal']) }}
                <label class="col-md-3 text-center">{{ trans('common.update').' '.trans('common.status') }}</label>
                <div class="col-md-6">
                    {{ Form::select('status', ['pending'=>trans('flights.statuses.pending.label') ,'confirmed'=>trans('flights.statuses.confirmed.label') ,'canceled'=>trans('flights.statuses.canceled.label')], $request->status, ['class' => 'form-control']) }}
                </div>
                {{ Form::hidden('request', $request->id) }}
                {{ Form::submit(trans('common.update'), ['class' => 'btn btn-primary']) }}
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-12">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
            </div>
            <div class="col-md-8">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('flights.trips') }}</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                            <tr>
                                <th>{{ trans('flights.from') }}</th>
                                <th>{{ trans('flights.to') }}</th>
                                <th>{{ trans('flights.checkin') }}</th>
                                @if($request->type == 'return')<th>{{ trans('flights.checkout') }}</th>@endif
                            </tr>
                            </thead>
                            <tbody>
                            @if($request->type == 'multiple')
                                @foreach($request->trips_list as $trip)
                                    <tr>
                                        <td>{{ $trip->from }}</td>
                                        <td>{{ $trip->to }}</td>
                                        <td>{{ $trip->checkin }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>{{ $request->trips_list->from }}</td>
                                    <td>{{ $request->trips_list->to }}</td>
                                    <td>{{ $request->trips_list->checkin }}</td>
                                    @if($request->type == 'return')<td>{{ $request->trips_list->checkout }}</td>@endif
                                </tr>
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('flights.from') }}</th>
                                <th>{{ trans('flights.to') }}</th>
                                <th>{{ trans('flights.checkin') }}</th>
                                @if($request->type == 'return')<th>{{ trans('flights.checkout') }}</th>@endif
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                @if($request->adults_count > 0 && count($request->adults_list))
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">{{ trans('flights.adults') }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class="box-body pad">
                            <table id="example1" class="table table-bordered table-striped text-center">
                                <thead>
                                <tr>
                                    <th>{{ trans('flights.title') }}</th>
                                    <th>{{ trans('flights.name') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($request->adults_list as $title=>$name)
                                    <tr>
                                        <td>{{ $title }}</td>
                                        <td>{{ $name }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>{{ trans('flights.title') }}</th>
                                    <th>{{ trans('flights.name') }}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                @endif

                @if($request->childes_count > 0 && count($request->childes_list))
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">{{ trans('flights.childes') }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class="box-body pad">
                            <table id="example1" class="table table-bordered table-striped text-center">
                                <thead>
                                <tr>
                                    <th>{{ trans('flights.birthdate') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($request->childes_list as $birthdate)
                                    <tr>
                                        <td>{{ $birthdate }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>{{ trans('flights.birthdate') }}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                @endif
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <table class="table">
                            <tr>
                                <th>{{ trans('common.request-code') }}</th>
                                <td>{{ $request->code }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('flights.types.type') }}</th>
                                <td>{{ trans('flights.types.'.$request->type) }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('flights.adults-count') }}</th>
                                <td>{{ $request->adults_count }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('flights.childes-count') }}</th>
                                <td>{{ $request->childes_count }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('flights.direct') }}</th>
                                <td><img src="{{ asset('assets/frontend/img/status_'.$request->direct.'.png') }}"></td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.status') }}</th>
                                <td>{!! trans('flights.statuses.'.$request->status.'.style') !!}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.created-at') }}</th>
                                <td>{{ $request->created_at }}</td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('flights.note') }}</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        {!! $request->note !!}
                    </div>
                </div>

                {{ Form::open(['url' => route('flights.request.edit'), 'class' => 'form-horizontal']) }}
                <div class="box box-success">
                    <div class='box-header'>
                        <h3 class="box-title">{{ trans('common.admin-comment') }}</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body'>
                        <div>
                            {{ Form::textarea('admin_comment', $request->admin_comment, ['id' => 'admin_comment', 'class' => 'form-control']) }}
                        </div>
                        <div class="clearfix"></div><br>
                        {{ Form::hidden('request', $request->id) }}
                        {{ Form::submit(trans('common.update'), array('class' => 'btn btn-primary col-md-12')) }}
                        <div class="clearfix"></div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
        });
    </script>
@stop