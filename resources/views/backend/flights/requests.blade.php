@extends('backend.common.template')

@section('title'){{ trans('trips.trips')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('flights.types.type') }}</th>
                            <th>{{ trans('flights.adults-count') }}</th>
                            <th>{{ trans('flights.childes-count') }}</th>
                            <th>{{ trans('flights.direct') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('common.created-at') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allRequests as $request)
                            <tr>
                                <td>{{ $request->code }}</td>
                                <td>{{ trans('flights.types.'.$request->type) }}</td>
                                <td>{{ $request->adults_count }}</td>
                                <td>{{ $request->childes_count }}</td>
                                <td><img src="{{ asset('assets/backend/img/status_'.$request->direct.'.png') }}"></td>
                                <td><button class="btn-link" data-toggle="tooltip" data-original-title="{{ @$request->user->phone }}">{{ @$request->user->fullname }}</button></td>
                                <td>{{ $request->created_at }}</td>
                                <td>{!! trans('flights.statuses.'.$request->status.'.style') !!}</td>
                                <td><a href="{{ route('flights.request', [$request->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('flights.types.type') }}</th>
                            <th>{{ trans('flights.adults-count') }}</th>
                            <th>{{ trans('flights.childes-count') }}</th>
                            <th>{{ trans('flights.direct') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('common.created-at') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/magnific-popup.css') }}">
@append

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
        });
    </script>
@stop