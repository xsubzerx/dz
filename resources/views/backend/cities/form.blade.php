@extends('backend.common.template')

@section('title'){{ $title }}@stop

@section('content')
    {{ (isset($cityData)) ? Form::model($cityData, array('url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('cities.update', [$cityData->id])), 'method' => 'PUT')) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('cities.store'))]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif

            <div class="col-md-8">
                @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                    <div class='box box-info'>
                        <div class='box-header'>
                            <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class='box-body pad'>
                            <div class="form-group">
                                {{ Form::label('name_'.$localeCode, trans('cities.city-title').':') }}
                                {{ Form::text('name_'.$localeCode, old('name_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('cities.city-title'))) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('country_id', trans('countries.title').':') }}
                            {{ Form::select('country_id', $countries, old('country_id'), ['id' => 'country_id', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('status', trans('common.status').':') }}
                            {{ Form::select('status', array('1' => trans('common.active'),'0' => trans('common.deactive')), old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>
        </div>
    </div>
    {{ Form::close() }}
@stop