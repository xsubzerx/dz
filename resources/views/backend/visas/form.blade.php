@extends('backend.common.template')

@section('title'){{ $title }}@stop

@section('content')
    {{ (isset($visaData)) ? Form::model($visaData, array('url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('visa.update', [$visaData->id])), 'method' => 'PUT')) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('visa.store'))]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif

            <div class="col-md-8">
                @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                    <div class='box box-info'>
                        <div class='box-header'>
                            <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class='box-body pad'>
                            <div class="form-group">
                                {{ Form::label('title_'.$localeCode, trans('visas.title').':') }}
                                {{ Form::text('title_'.$localeCode, old('title_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('visas.title'), 'required')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description_'.$localeCode, trans('visas.description').':') }}
                                {{ Form::textarea('description_'.$localeCode, old('description_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('visas.description'))) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('country_id', trans('countries.title').':') }}
                            {{ Form::select('country_id', $countries, old('country_id'), ['id' => 'country_id', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('price', trans('visas.price').':') }}
                            {{ Form::text('price', old('price'), ['class' => 'form-control', 'placeholder' => trans('visas.price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('has_shipping', trans('visas.has-shipping').':') }}
                            {{ Form::select('has_shipping', array('0' => trans('common.no'),'1' => trans('common.yes')), old('has_shipping'), ['id' => 'has_shipping', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group hasShipping" style="{{ (isset($visaData) && $visaData->has_shipping) ? 'display:block' : 'display: none' }}">
                            {{ Form::label('middle_price', trans('visas.middle-price').':') }}
                            {{ Form::text('middle_price', old('middle_price'), ['class' => 'form-control', 'placeholder' => trans('visas.middle-price')]) }}
                        </div>
                        <div class="form-group hasShipping" style="{{ (isset($visaData) && $visaData->has_shipping) ? 'display:block' : 'display: none' }}">
                            {{ Form::label('east_west_price', trans('visas.east-west-price').':') }}
                            {{ Form::text('east_west_price', old('east_west_price'), ['class' => 'form-control', 'placeholder' => trans('visas.east-west-price')]) }}
                        </div>
                        <div class="form-group hasShipping" style="{{ (isset($visaData) && $visaData->has_shipping) ? 'display:block' : 'display: none' }}">
                            {{ Form::label('south_price', trans('visas.south-price').':') }}
                            {{ Form::text('south_price', old('south_price'), ['class' => 'form-control', 'placeholder' => trans('visas.south-price')]) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('status', trans('common.status').':') }}
                            {{ Form::select('status', array('1' => trans('common.active'),'0' => trans('common.deactive')), old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('featured', trans('common.featured').':') }}
                            {{ Form::select('featured', array('0' => trans('common.no'),'1' => trans('common.yes')), old('featured'), ['id' => 'featured', 'class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>
        </div>
    </div>
    {{ Form::close() }}
@stop

@section('scripts')
    <script src="//cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
    @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
        <script type="text/javascript">
            $(function() { CKEDITOR.replace('description_{{$localeCode}}', {language: "{{$localeCode}}"}); });
        </script>
    @endforeach
    <script type="application/javascript">
        $(document).ready(function () {
            $('#has_shipping').on('change', function () {
                if($(this).val() > 0){
                    $('.hasShipping').fadeIn();
                }else{
                    $('.hasShipping').fadeOut();
                }
            });
        });
    </script>
@stop