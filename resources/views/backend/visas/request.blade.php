@extends('backend.common.template')

@section('title'){{ trans('visas.visas')}} @stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            {{ Form::open(['url' => route('visa.request.edit'), 'class' => 'form-horizontal']) }}
            <label class="col-md-3 text-center">{{ trans('common.update').' '.trans('common.status') }}</label>
            <div class="col-md-6">
                {{ Form::select('status', ['pending'=>trans('visas.statuses.pending.label'), 'progress'=>trans('visas.statuses.progress.label') ,'completed'=>trans('visas.statuses.completed.label'), 'shipped'=>trans('visas.statuses.shipped.label') ,'canceled'=>trans('visas.statuses.canceled.label')], $request->status, ['class' => 'form-control']) }}
            </div>
            {{ Form::hidden('request', $request->id) }}
            {{ Form::submit(trans('common.update'), ['class' => 'btn btn-primary']) }}
            {{ Form::close() }}
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-xs-8">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('visas.doc-title') }}</th>
                            <th>{{ trans('visas.document') }}</th>
                            <th>Download</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($request->documents))
                            @foreach($request->documents as $doc)
                                <tr>
                                    <td>{{ $doc->doc->title_ar }}<br/>{{ $doc->doc->title_fr }}</td>
                                    <td>{!! ($doc->document) ? '<a href="'.route('image', ['visas', $doc->document]).'" class="image-link"><img src="'.route('image', ['visas', $doc->document]).'" height="60" /></a>' : '<img src="'.asset('assets/admin/img/no-image-found.jpg').'" height="60" />' !!}</td>
                                    <td>@if($doc->document)<a href="{{ route('image', ['visas', $doc->document]) }}" download target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>@endif</td>
                                </tr>
                            @endforeach
                        @endif
                        @if($request->payment)
                            <tr>
                                <td>{{ trans('common.payment-reset') }}</td>
                                <td>{!! ($request->payment) ? '<a href="'.route('image', ['payments', $request->payment->payment]).'" class="image-link"><img src="'.route('image.small', ['payments', $request->payment->payment]).'" height="60" /></a>' : '<img src="'.asset('assets/admin/img/no-image-found.jpg').'" >' !!}</td>
                                <td>@if($request->payment)<a href="{{ route('image', ['payments', $request->payment->payment]) }}" download target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>@endif</td>
                            </tr>
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('visas.doc-title') }}</th>
                            <th>{{ trans('visas.document') }}</th>
                            <th>Download</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>

        <div class="col-md-4">
            {{ Form::open(['url' => route('visa.request.edit'), 'class' => 'form-horizontal']) }}
            <div class="box box-success">
                <div class='box-header'>
                    <h3 class="box-title">{{ trans('common.admin-comment') }}</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div>
                        {{ Form::textarea('admin_comment', $request->admin_comment, ['id' => 'admin_comment', 'class' => 'form-control']) }}
                    </div>
                    <div class="clearfix"></div><br>
                    {{ Form::hidden('request', $request->id) }}
                    {{ Form::submit(trans('common.update'), array('class' => 'btn btn-primary col-md-12')) }}
                    <div class="clearfix"></div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>

@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/magnific-popup.css') }}">
@append

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    {{ Html::script('assets/frontend/js/jquery.magnific-popup.min.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();

            $('.image-link').magnificPopup({type:'image'});
        });
    </script>
@stop