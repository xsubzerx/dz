@extends('backend.common.template')

@section('title'){{ trans('visas.visa-requests')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('visas.title') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('common.payment-reset') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.created-at') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allRequests as $request)
                            <tr>
                                <td>{{ $request->code }}</td>
                                <td>{{ $request->visa->title_ar }}<br/>{{ $request->visa->title_fr }}</td>
                                <td>{{ $request->visa->country->name_ar }}<br/>{{ $request->visa->country->name_fr }}</td>
                                <td><button class="btn-link" data-toggle="tooltip" data-original-title="{{ $request->user->phone }}">{{ $request->user->fullname }}</button></td>
                                <td>{!! ($request->payment) ? '<a href="'.route('image', ['payments', $request->payment->payment]).'" class="image-link"><img src="'.route('image.thumbnail', ['payments', $request->payment->payment]).'" height="80"></a>' : '<img src="'.asset('assets/backend/img/status_0.png').'" />' !!}</td>
                                <td>{!! trans('visas.statuses.'.$request->status.'.style') !!}</td>
                                <td>{{ $request->created_at }}</td>
                                <td><a href="{{ route('visa.request', [$request->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('visas.title') }}</th>
                            <th>{{ trans('countries.title') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('common.payment-reset') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.created-at') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/magnific-popup.css') }}">
@append

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    {{ Html::script('assets/frontend/js/jquery.magnific-popup.min.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();

            $('.image-link').magnificPopup({type:'image'});
        });
    </script>
@stop