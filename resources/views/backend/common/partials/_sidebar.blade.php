<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets/backend/img/avatar3.png') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, {{ str_limit(Auth::guard('admin')->user()->name, 30) }}</p>
            </div>
        </div>
        <?php $direction = (LaraLocale::getCurrentLocale() == 'ar') ? 'left' : 'right'; ?>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li @if(Route::currentRouteName() == 'dashboard') class="active" @endif><a href="{{ route('dashboard') }}" ><i class="fa fa-dashboard"></i> <span>{{ trans('common.cpanel') }}</span></a></li>
            <li class="treeview @if(Request::is([LaraLocale::getCurrentLocale().'/backend/settings', LaraLocale::getCurrentLocale().'/backend/terms', LaraLocale::getCurrentLocale().'/backend/contacts'])) active @endif">
                <a href="#"><i class="fa fa-cogs"></i><span>{{ trans('settings.global-settings') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Route::currentRouteName() == 'settings' ) class="active" @endif><a href="{{ route('settings') }}"><i class="fa fa-angle-double-{{$direction}}"></i><span>{{ trans('settings.teb-title') }}</span></a></li>
                    <li @if(Route::currentRouteName() == 'terms' ) class="active" @endif><a href="{{ route('terms') }}"><i class="fa fa-angle-double-{{$direction}}"></i><span>{{ trans('settings.terms') }}</span></a></li>
                    <li @if(Route::currentRouteName() == 'contacts' ) class="active" @endif><a href="{{ route('contacts') }}"><i class="fa fa-angle-double-{{$direction}}"></i><span>{{ trans('settings.contact-info') }}</span></a></li>
                </ul>
            </li>
            <li @if(Route::currentRouteName() == 'contact-us') class="active" @endif><a href="{{ route('contact-us') }}" ><i class="fa fa-envelope"></i> <span>{{ trans('contact.inbox') }}</span></a></li>
            <li @if(Route::currentRouteName() == 'newsletter') class="active" @endif><a href="{{ route('newsletter') }}" ><i class="fa fa-envelope"></i> <span>{{ trans('contact.newsletter') }}</span></a></li>
            <li class="treeview @if(Request::is([LaraLocale::getCurrentLocale().'/backend/navigations', LaraLocale::getCurrentLocale().'/backend/navigations/*'])) active @endif">
                <a href="#"><i class="fa fa-bars"></i><span>{{ trans('navigations.navigations') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Route::currentRouteName() == 'navigations.create') class="active" @endif><a href="{{ route('navigations.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('navigations.create-item') }}</a></li>
                    <li @if(Route::currentRouteName() == 'navigations.index') class="active" @endif><a href="{{ route('navigations.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('navigations.view-items') }}</a></li>
                </ul>
            </li>
            <li class="treeview @if(Request::is([LaraLocale::getCurrentLocale().'/backend/slides', LaraLocale::getCurrentLocale().'/backend/slides/*'])) active @endif">
                <a href="#"><i class="fa fa-picture-o"></i><span>{{ trans('slides.slides') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Route::currentRouteName() == 'slides.create') class="active" @endif><a href="{{ route('slides.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('slides.create-slide') }}</a></li>
                    <li @if(Route::currentRouteName() == 'slides.index') class="active" @endif><a href="{{ route('slides.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('slides.view-slides') }}</a></li>
                </ul>
            </li>
            <li class="treeview @if(Request::is([LaraLocale::getCurrentLocale().'/backend/pages', LaraLocale::getCurrentLocale().'/backend/pages/*'])) active @endif">
                <a href="#"><i class="fa fa-file-text"></i><span>{{ trans('pages.content-pages') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Route::currentRouteName() == 'pages.create') class="active" @endif><a href="{{ route('pages.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('pages.create-page') }}</a></li>
                    <li @if(Route::currentRouteName() == 'pages.index') class="active" @endif><a href="{{ route('pages.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('pages.view-pages') }}</a></li>
                </ul>
            </li>
            <li class="treeview @if(Request::is([LaraLocale::getCurrentLocale().'/backend/countries', LaraLocale::getCurrentLocale().'/backend/countries/*', LaraLocale::getCurrentLocale().'/backend/cities', LaraLocale::getCurrentLocale().'/backend/cities/*'])) active @endif">
                <a href="#"><i class="fa fa-map-marker"></i><span>{{ trans('common.locations') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Route::currentRouteName() == 'countries.create') class="active" @endif><a href="{{ route('countries.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('countries.create-country') }}</a></li>
                    <li @if(Route::currentRouteName() == 'countries.index') class="active" @endif><a href="{{ route('countries.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('countries.view-countries') }}</a></li>
                    <li @if(Route::currentRouteName() == 'cities.create') class="active" @endif><a href="{{ route('cities.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('cities.create-city') }}</a></li>
                    <li @if(Route::currentRouteName() == 'cities.index') class="active" @endif><a href="{{ route('cities.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('cities.view-cities') }}</a></li>-
                </ul>
            </li>
            <li class="treeview @if(Request::is([LaraLocale::getCurrentLocale().'/backend/visa', LaraLocale::getCurrentLocale().'/backend/visa/*', LaraLocale::getCurrentLocale().'/backend/documents', LaraLocale::getCurrentLocale().'/backend/documents/*', LaraLocale::getCurrentLocale().'/backend/visa/requests', LaraLocale::getCurrentLocale().'/backend/visa/request'])) active @endif">
                <a href="#"><i class="ion ion-card"></i><span>{{ trans('visas.visas') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Route::currentRouteName() == 'visa.create') class="active" @endif><a href="{{ route('visa.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('visas.create-visa') }}</a></li>
                    <li @if(Route::currentRouteName() == 'visa.index') class="active" @endif><a href="{{ route('visa.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('visas.view-visas') }}</a></li>
                    <li @if(Route::currentRouteName() == 'visa.requests') class="active" @endif><a href="{{ route('visa.requests') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('visas.visa-requests') }}</a></li>
                </ul>
            </li>
            <li class="treeview @if(Request::is([LaraLocale::getCurrentLocale().'/backend/hotels', LaraLocale::getCurrentLocale().'/backend/hotels/*', LaraLocale::getCurrentLocale().'/backend/rooms', LaraLocale::getCurrentLocale().'/backend/rooms/*', LaraLocale::getCurrentLocale().'/backend/hotels/requests', LaraLocale::getCurrentLocale().'/backend/hotels/request', LaraLocale::getCurrentLocale().'/backend/amenities'])) active @endif">
                <a href="#"><i class="fa fa-h-square"></i><span>{{ trans('hotels.hotels') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Route::currentRouteName() == 'hotels.create') class="active" @endif><a href="{{ route('hotels.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('hotels.create-hotel') }}</a></li>
                    <li @if(Route::currentRouteName() == 'hotels.index') class="active" @endif><a href="{{ route('hotels.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('hotels.view-hotels') }}</a></li>
                    <li @if(Route::currentRouteName() == 'amenities.index') class="active" @endif><a href="{{ route('amenities.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('hotels.view-amenities') }}</a></li>
                    <li @if(Route::currentRouteName() == 'hotels.requests') class="active" @endif><a href="{{ route('hotels.requests') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('hotels.hotel-requests') }}</a></li>
                    <li @if(Route::currentRouteName() == 'hotels.requests') class="active" @endif><a href="{{ route('hotels.requests-custom') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('hotels.hotel-custom-requests') }}</a></li>
                </ul>
            </li>
            <li class="treeview @if(Request::is([LaraLocale::getCurrentLocale().'/backend/trips', LaraLocale::getCurrentLocale().'/backend/trips/*', LaraLocale::getCurrentLocale().'/backend/trips/requests', LaraLocale::getCurrentLocale().'/backend/trips/request'])) active @endif">
                <a href="#"><i class="fa fa-suitcase"></i><span>{{ trans('trips.trips') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Route::currentRouteName() == 'trips.create') class="active" @endif><a href="{{ route('trips.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('trips.create-trip') }}</a></li>
                    <li @if(Route::currentRouteName() == 'trips.index') class="active" @endif><a href="{{ route('trips.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('trips.view-trips') }}</a></li>
                    <li @if(Route::currentRouteName() == 'trips.requests') class="active" @endif><a href="{{ route('trips.requests') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('trips.trips-requests') }}</a></li>
                </ul>
            </li>
            <li @if(Route::currentRouteName() == 'flights.requests') class="active" @endif><a href="{{ route('flights.requests') }}" ><i class="fa fa-plane"></i> <span>{{ trans('flights.flights-requests') }}</span></a></li>
            <li class="treeview @if(Request::is([LaraLocale::getCurrentLocale().'/backend/users', LaraLocale::getCurrentLocale().'/backend/users/*'])) active @endif">
                <a href="#"><i class="fa fa-users"></i><span>{{ trans('users.users') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Route::currentRouteName() == 'users.create') class="active" @endif><a href="{{ route('users.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('users.create-user') }}</a></li>
                    <li @if(Route::currentRouteName() == 'users.index') class="active" @endif><a href="{{ route('users.index', 'type=user') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('users.view-users') }}</a></li>
                    <li @if(Route::currentRouteName() == 'users.index') class="active" @endif><a href="{{ route('users.index', 'type=agency') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('users.view-agencies') }}</a></li>
                    <li @if(Route::currentRouteName() == 'users.generate') class="active" @endif><a href="{{ route('users.generate') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('users.generate-users') }}</a></li>
                </ul>
            </li>
            <li class="treeview @if(Request::is([LaraLocale::getCurrentLocale().'/backend/admins', LaraLocale::getCurrentLocale().'/backend/admins/*'])) active @endif">
                <a href="#"><i class="fa fa-users"></i><span>{{ trans('users.admins') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Route::currentRouteName() == 'admins.create') class="active" @endif><a href="{{ route('admins.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('users.create-admin') }}</a></li>
                    <li @if(Route::currentRouteName() == 'admins.index') class="active" @endif><a href="{{ route('admins.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('users.view-admins') }}</a></li>
                </ul>
            </li>
            <li><a href="{{ URL::to('backend/logout') }}" ><i class="fa fa-sign-out"></i> <span>{{ trans('users.logout') }}</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>