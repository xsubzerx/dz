<!-- jQuery 2.0.2 -->
<script src="{{ asset('assets/backend/js/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('assets/backend/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/backend/js/AdminLTE/app.js') }}"></script>

@yield('scripts')