<!DOCTYPE html>
<html lang="{{ LaraLocale::getCurrentLocale() }}">
    @include('backend.common.partials._head')
    <body class="pace-done skin-black fixed">
        @include('backend.common.partials._header')

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            @include('backend.common.partials._sidebar')
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>@yield('title')</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> {{ trans('common.cpanel') }}</a></li>
                        <li class="active">@yield('title')</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        @include('backend.common.partials._scripts')
    </body>
</html>