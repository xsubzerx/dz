@extends('backend.common.template')

@section('title', $title)

@section('content')

    {{ (isset($userData)) ? Form::model($userData, array('url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('admins.update', [$userData->id])), 'method' => 'PUT')) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('admins.store'))]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                    @if(!empty($errors->all()))
                        <ul class="callout callout-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                    @endif
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('name', trans('users.fullname').':') }}
                        {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => trans('users.fullname'))) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('phone', trans('users.phone').':') }}
                        {{ Form::text('phone', old('phone'), array('class' => 'form-control', 'placeholder' => trans('users.phone'))) }}
                    </div>
                    <br/>
                    <div class="form-group">
                        {{ Form::label('email', trans('users.email').':') }}
                        @if(isset($userData->email))
                            <br/> {{ $userData->email }}
                        @else
                            {{ Form::email('email', old('email'), array('class' => 'form-control', 'placeholder' => trans('users.email'))) }}
                        @endif
                    </div>
                    <br/>
                    <div class="form-group">
                        {{ Form::label('password', trans('users.password').':') }}
                        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => trans('users.password'))) }}
                    </div>
                </div>
                <div class="box-footer">
                    {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@stop
