@extends('backend.common.template')
@section('title', trans('settings.teb-title'))

@section('content')

    <div class="row">
        <!-- left column -->
        {{ Form::model($settings) }}
            <div class="col-md-12">
                @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                @if(!empty($errors->all()))
                    <ul class="callout callout-danger">
                        @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                    </ul>
                @endif
            </div>

            <div class="col-md-8 col-sm-12">
                @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                    <div class='box box-info'>
                        <div class='box-header'>
                            <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class='box-body pad'>
                            <div class="form-group">
                                {{ Form::label('address_'.$localeCode, trans('users.address').':') }}
                                {{ Form::text('address_'.$localeCode, old('address_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('users.address').':')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('contact_page_'.$localeCode, trans('settings.contact-page').':') }}
                                {{ Form::textarea('contact_page_'.$localeCode, old('contact_page_'.$localeCode), array('rows' => 3, 'class' => 'form-control', 'placeholder' => trans('settings.contact-page'))) }}
                            </div>
                        </div>
                    </div><!-- /.box -->
                @endforeach
            </div>

            <div class="col-md-4 col-sm-12">
                <div class='box box-success'>
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-success btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('site_phone', trans('settings.site-phone').':') }}
                            {{ Form::number('site_phone', old('site_phone'), array('class' => 'form-control', 'placeholder' => trans('settings.site-phone'))) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('site_email', trans('settings.site-email').':') }}
                            {{ Form::text('site_email', old('site_email'), array('class' => 'form-control', 'placeholder' => trans('settings.site-email'))) }}
                        </div>
                    </div>
                </div>

                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>
        {{ Form::close() }}
    </div>
@stop

@section('scripts')
    <script src="//cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
    @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
        <script type="text/javascript">
            $(function() { CKEDITOR.replace('contact_page_{{$localeCode}}', {language: "{{$localeCode}}"}); });
        </script>
    @endforeach
@stop