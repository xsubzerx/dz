@extends('backend.common.template')
@section('title', trans('settings.teb-title'))

@section('content')

    <div class="row">
        <!-- left column -->
        {{ Form::model($settings) }}
            <div class="col-md-12">
                @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                @if(!empty($errors->all()))
                    <ul class="callout callout-danger">
                        @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                    </ul>
                @endif
            </div>

            <div class="col-md-8 col-sm-12">
                @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                    <div class='box box-info'>
                        <div class='box-header'>
                            <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class='box-body pad'>
                            <div class="form-group">
                                {{ Form::label('site_name_'.$localeCode, trans('settings.site-name').':') }}
                                {{ Form::text('site_name_'.$localeCode, old('site_name_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('settings.site-name'))) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('site_metakey_'.$localeCode, trans('settings.site-metakey').':') }}
                                {{ Form::text('site_metakey_'.$localeCode, old('site_metakey_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('settings.site-metakey'))) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('site_metadesc_'.$localeCode, trans('settings.site-metadesc').':') }}
                                {{ Form::textarea('site_metadesc_'.$localeCode, old('site_metadesc_'.$localeCode), array('rows' => 3, 'class' => 'form-control', 'placeholder' => trans('settings.site-metadesc'))) }}
                            </div>
                        </div>
                    </div><!-- /.box -->
                @endforeach
            </div>

            <div class="col-md-4 col-sm-12">
                <div class='box box-success'>
                    <div class='box-header'>
                        <h3 class='box-title'>Social Media</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-success btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('facebook', 'Facbook:') }}
                            {{ Form::text('facebook', old('facebook'), array('class' => 'form-control', 'placeholder' => 'Facebook')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('twitter', 'Twitter:') }}
                            {{ Form::text('twitter', old('twitter'), array('class' => 'form-control', 'placeholder' => 'Twitter')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('youtube', 'Youtube:') }}
                            {{ Form::text('youtube', old('youtube'), array('class' => 'form-control', 'placeholder' => 'Youtube')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('instagram', 'Instagram:') }}
                            {{ Form::text('instagram', old('instagram'), array('class' => 'form-control', 'placeholder' => 'Instagram')) }}
                        </div>
                    </div>
                </div><!-- /.box -->

                <div class='box box-success'>
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-success btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('currency_rate', trans('settings.currency-rate').':') }}
                            {{ Form::number('currency_rate', old('currency_rate'), array('class' => 'form-control', 'placeholder' => trans('settings.currency-rate'))) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('status', trans('common.status').':') }}
                            {{ Form::select('status', array('1' => trans('common.active'),'0' => trans('common.deactive')), old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>
        {{ Form::close() }}
    </div>
@stop