@extends('backend.common.template')
@section('title', trans('settings.teb-title'))

@section('content')

    <div class="row">
        <!-- left column -->
        {{ Form::model($settings) }}
            <div class="col-md-12">
                @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                @if(!empty($errors->all()))
                    <ul class="callout callout-danger">
                        @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                    </ul>
                @endif
            </div>

            <div class="col-md-12 col-sm-12">
                @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                    <div class='box box-info'>
                        <div class='box-header'>
                            <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class='box-body pad'>
                            <div class="form-group">
                                {{ Form::label('visas_terms_'.$localeCode, trans('settings.visas-terms').':') }}
                                {{ Form::textarea('visas_terms_'.$localeCode, old('visas_terms_'.$localeCode), array('rows' => 3, 'class' => 'form-control', 'placeholder' => trans('settings.visas-terms'))) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('hotels_terms_'.$localeCode, trans('settings.hotels-terms').':') }}
                                {{ Form::textarea('hotels_terms_'.$localeCode, old('hotels_terms_'.$localeCode), array('rows' => 3, 'class' => 'form-control', 'placeholder' => trans('settings.hotels-terms'))) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('trips_terms_'.$localeCode, trans('settings.trips-terms').':') }}
                                {{ Form::textarea('trips_terms_'.$localeCode, old('trips_terms_'.$localeCode), array('rows' => 3, 'class' => 'form-control', 'placeholder' => trans('settings.trips-terms'))) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('flights_terms_'.$localeCode, trans('settings.flights-terms').':') }}
                                {{ Form::textarea('flights_terms_'.$localeCode, old('flights_terms_'.$localeCode), array('rows' => 3, 'class' => 'form-control', 'placeholder' => trans('settings.flights-terms'))) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('agencies_terms_'.$localeCode, trans('settings.agencies-terms').':') }}
                                {{ Form::textarea('agencies_terms_'.$localeCode, old('agencies_terms_'.$localeCode), array('rows' => 3, 'class' => 'form-control', 'placeholder' => trans('settings.agencies-terms'))) }}
                            </div>
                        </div>
                    </div><!-- /.box -->
                @endforeach

                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>

        {{ Form::close() }}
    </div>
@stop

@section('scripts')
    <script src="//cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
    @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
        <script type="text/javascript">
            $(function() {
                CKEDITOR.replace('visas_terms_{{$localeCode}}', {language: "{{$localeCode}}"});
                CKEDITOR.replace('hotels_terms_{{$localeCode}}', {language: "{{$localeCode}}"});
                CKEDITOR.replace('trips_terms_{{$localeCode}}', {language: "{{$localeCode}}"});
                CKEDITOR.replace('flights_terms_{{$localeCode}}', {language: "{{$localeCode}}"});
                CKEDITOR.replace('agencies_terms_{{$localeCode}}', {language: "{{$localeCode}}"});
            });
        </script>
    @endforeach
@stop