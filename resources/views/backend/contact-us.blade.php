@extends('backend.common.template')

@section('title'){{ trans('contact.inbox')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('contact.sender') }}</th>
                            <th>{{ trans('users.email') }}</th>
                            <th>{{ trans('users.phone') }}</th>
                            <th>{{ trans('contact.subject') }}</th>
                            <th>{{ trans('contact.message') }}</th>
                            <th>{{ trans('contact.sent-at') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($emails as $mail)
                            <tr>
                                <td>{{ $mail->sender }}</td>
                                <td>{{ $mail->email }}</td>
                                <td>{{ $mail->phone }}</td>
                                <td>{{ $mail->subject }}</td>
                                <td>{{ $mail->message }}</td>
                                <td>{{ $mail->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('contact.sender') }}</th>
                            <th>{{ trans('users.email') }}</th>
                            <th>{{ trans('users.phone') }}</th>
                            <th>{{ trans('contact.subject') }}</th>
                            <th>{{ trans('contact.message') }}</th>
                            <th>{{ trans('contact.sent-at') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
        });
    </script>
@stop