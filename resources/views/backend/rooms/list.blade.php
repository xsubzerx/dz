@extends('backend.common.template')

@section('title'){{ trans('hotels.rooms')}} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <a href="{{ route('rooms.create', [$hotelID]) }}" class="btn btn-primary btn-flat center-block">{{ trans('hotels.create-room') }}</a>
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('hotels.room') }}</th>
                            <th>{{ trans('hotels.hotel') }}</th>
                            <th>{{ trans('hotels.room-image') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allRooms as $room)
                            <tr>
                                <td>{{ $room->title_ar }}<br/>{{ $room->title_fr }}</td>
                                <td>{{ $room->hotel->name_ar }}<br/>{{ $room->hotel->name_fr }}</td>
                                <td>@if($room->image)<img src="{{ route('image.thumbnail', ['hotels', $room->image] ) }}" height="60" />@endif</td>
                                <td><a href="{{ route('rooms.edit', [$room->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                <td>
                                    {{ Form::open(['url' => route('rooms.destroy', [$room->id]), 'method' => 'DELETE']) }}
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="confirm('{{trans('common.delete-confirm')}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('hotels.room') }}</th>
                            <th>{{ trans('hotels.hotel') }}</th>
                            <th>{{ trans('hotels.room-image') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
        });
    </script>
@stop