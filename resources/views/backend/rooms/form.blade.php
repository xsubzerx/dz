@extends('backend.common.template')

@section('title'){{ $title }}@stop

@section('content')
    {{ (isset($roomData)) ? Form::model($roomData, array('url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('rooms.update', [$roomData->id])), 'method' => 'PUT', 'files' => true)) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('rooms.store')), 'files' => true]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif

            <div class="col-md-8">
                @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                    <div class='box box-info'>
                        <div class='box-header'>
                            <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <div class='box-body pad'>
                            <div class="form-group">
                                {{ Form::label('title_'.$localeCode, trans('hotels.title').':') }}
                                {{ Form::text('title_'.$localeCode, old('title_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('hotels.title'), 'required')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description_'.$localeCode, trans('hotels.description').':') }}
                                {{ Form::textarea('description_'.$localeCode, old('description_'.$localeCode), array('class' => 'form-control editor', 'placeholder' => trans('hotels.description'))) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-4">
                <div class="box box-success">
                    <div class='box-header'>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('adults_count', trans('hotels.adults-count').':') }}
                            {{ Form::number('adults_count', old('adults_count'), ['id' => 'adults_count', 'class' => 'form-control', 'placeholder' => trans('hotels.adults-count'), 'min' => 1, 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('adults_price', trans('hotels.adults-price').':') }}
                            {{ Form::text('price', old('price'), ['id' => 'adults_price', 'class' => 'form-control', 'placeholder' => trans('hotels.adults-price'), 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('childes_count', trans('hotels.childes-count').':') }}
                            {{ Form::number('childes_count', old('childes_count'), ['id' => 'childes_count', 'class' => 'form-control', 'placeholder' => trans('hotels.childes-count'), 'min' => 0, 'required']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('image', trans('hotels.room-image').':') }}
                            {{ Form::file('image', array('class' => 'form-control', 'placeholder' => trans('hotels.room-image'))) }}
                            <div class="clearfix"></div>
                            <hr>
                            <div class="content-block" id="image-block">
                                @if(isset($roomData) && $roomData->image)<button type="button" class="btn btn-sm btn-danger btn-flat"><i class="fa fa-trash-o"></i></button>@endif
                                <img src="{{ (isset($roomData) && $roomData->image) ? route('image.small', ['hotels', $roomData->image]) : asset('assets/backend/img/no-image-found.jpg') }}" class="img-thumbnail img-responsive center-block" height="100" />
                            </div>
                        </div>
                    </div>
                </div>

                {{ Form::hidden('hotel_id', (isset($roomData)) ? $roomData->hotel_id : $hotelID) }}
                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>
        </div>
    </div>
    {{ Form::close() }}
@stop

@section('scripts')
    <script src="//cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
    @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
        <script type="text/javascript">
            $(function() { CKEDITOR.replace('description_{{$localeCode}}', {language: "{{$localeCode}}"}); });
        </script>
    @endforeach

    @if(isset($roomData) && $roomData->image)
    <script type="text/javascript">
        $(document).ready(function() {
            $('#image-block').on('click', 'button', function () {
                if(confirm('{{trans('common.delete-confirm')}}')){
                    $.ajax({
                        url: '{{route('room.photo.delete')}}',
                        method: 'POST',
                        data: {rID: '{{$roomData->id}}', img: '{{$roomData->image}}'},
                        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                        success: function (res) {
                            if(res.success){
                                $('#image-block .img-thumbnail').attr('src', '{{asset('assets/backend/img/no-image-found.jpg')}}');
                                $('#image-block button').remove();
                            }
                        }
                    })
                }
            });
        });
    </script>
    @endif
@stop