@extends('backend.common.template')

@section('title', $title)

@section('content')

    {{ (isset($userData)) ? Form::model($userData, ['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('users.update', [$userData->id])), 'method' => 'PUT']) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('users.store'))]) }}
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        @if(Session::has('msg'))
                            {!! Session::get('msg') !!}
                        @endif
                        @if(!empty($errors->all()))
                            <ul class="callout callout-danger">
                                @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="box-body">
                        <div class="form-group col-md-6 col-sm-12">
                            {{ Form::label('firstname', ($agency) ? trans('users.agency-name').':' : trans('users.firstname').':') }}
                            {{ Form::text('firstname', old('firstname'), ['class' => 'form-control', 'placeholder' => trans('users.firstname'), 'required']) }}
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            {{ Form::label('lastname', ($agency) ? trans('users.agency-owner').':' : trans('users.lastname').':') }}
                            {{ Form::text('lastname', old('lastname'), ['class' => 'form-control', 'placeholder' => trans('users.lastname'), 'required']) }}
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            {{ Form::label('email', trans('users.email').':') }}
                            @if(isset($userData->email))
                                <br/> {{ $userData->email }}
                            @else
                                {{ Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('users.email'), 'required']) }}
                            @endif
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            {{ Form::label('phone', trans('users.phone').':') }}
                            {{ Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder' => trans('users.phone'), 'required']) }}
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            {{ Form::label('section', trans('users.section').':') }}
                            {{ Form::text('section', old('section'), ['class' => 'form-control', 'placeholder' => trans('users.section'), 'required']) }}
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            {{ Form::label('town', trans('users.town').':') }}
                            {{ Form::text('town', old('town'), ['class' => 'form-control', 'placeholder' => trans('users.town'), 'required']) }}
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            {{ Form::label('state', trans('users.state').':') }}
                            {{ Form::text('state', old('state'), ['class' => 'form-control', 'placeholder' => trans('users.state'), 'required']) }}
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            {{ Form::label('address', trans('users.address').':') }}
                            {{ Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => trans('users.address'), 'required']) }}
                        </div>

                        @if($agency)
                            <div class="form-group col-md-6 col-sm-12">
                                {{ Form::label('website', trans('users.website').':') }}
                                {{ Form::text('website', old('website'), ['class' => 'form-control', 'placeholder' => trans('users.website')]) }}
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                {{ Form::label('facebook', trans('users.facebook').':') }}
                                {{ Form::text('facebook', old('facebook'), ['class' => 'form-control', 'placeholder' => trans('users.facebook')]) }}
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                {{ Form::label('license_number', trans('users.license-number').':') }}
                                {{ Form::text('license_number', old('license_number'), ['class' => 'form-control', 'placeholder' => trans('users.license-number'), 'required']) }}
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                {{ Form::label('license_year', trans('users.license-year').':') }}
                                {{ Form::text('license_year', old('license_year'), ['class' => 'form-control', 'placeholder' => trans('users.license-year')]) }}
                            </div>
                        @else
                            <div class="form-group col-md-12 col-sm-12">
                                {{ Form::label('passport', trans('users.passport').':') }}
                                {{ Form::text('passport', old('passport'), ['class' => 'form-control', 'placeholder' => trans('users.passport')]) }}
                            </div>
                        @endif

                        <div class="form-group col-md-4 col-sm-12">
                            {{ Form::label('password', trans('users.password').':') }}
                            {{ Form::password('password', ['class' => 'form-control', 'placeholder' => trans('users.password')]) }}
                        </div>

                        <div class="form-group col-md-4 col-sm-12">
                            {{ Form::label('password_confirmation', trans('users.repassword').':') }}
                            {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('users.repassword')]) }}
                        </div>

                        <div class="form-group col-md-4 col-md-12">
                            {{ Form::label('status', trans('common.status').':') }}
                            {{ Form::select('status', array('1' => trans('common.active'),'0' => trans('common.deactive')), old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="box-footer">
                        {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}

@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            var city = $('#city_id').val();
            var currRegion = $('#currRegion').val();
            getCityRegions(city, currRegion);

            $('#city_id').on('change', function(){
                city = $(this).val();
                getCityRegions(city, currRegion);
            });

            function getCityRegions(city, currRegion) {
                $.get("/getCityRegions/"+city, function(data, status){
                    var opt = '';
                    $.each(data, function (index, value) {
                        if(index == currRegion){
                            opt += '<option value="'+index+'" selected="selected">'+value+'</option>';
                        }else{
                            opt += '<option value="'+index+'">'+value+'</option>';
                        }
                    });
                    $('#region_id').html(opt);
                });
            }

            $('#addPhone').on('click', function(){
                $('#phones').append('{{ Form::number('phones[]', null, ['class' => 'form-control', 'placeholder' => trans('users.phone')]) }}');
            });
        });
    </script>
@stop
