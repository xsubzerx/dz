@extends('backend.common.template')

@section('title', trans('common.dashboard'))

@section('content')

    @if(count($latestVisaRequests))
    <div class="col-md-12">
        <!-- Success box -->
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">{{ trans('visas.visa-requests') }}</h3>
                <div class="box-tools pull-right">
                    <div class="btn-group" data-toggle="btn-toggle">
                        <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>{{ trans('common.request-code') }}</th>
                        <th>{{ trans('visas.title') }}</th>
                        <th>{{ trans('countries.title') }}</th>
                        <th>{{ trans('users.fullname') }}</th>
                        <th>{{ trans('common.created-at') }}</th>
                        <th>{{ trans('common.status') }}</th>
                        <th></th>
                    </tr>
                    @foreach($latestVisaRequests as $request)
                        <tr>
                            <td>{{ $request->code }}</td>
                            <td>{{ $request->visa->title_ar }}<br/>{{ $request->visa->title_fr }}</td>
                            <td>{{ $request->visa->country->name_ar }}<br/>{{ $request->visa->country->name_fr }}</td>
                            <td>{{ $request->user->fullname }}</td>
                            <td>{{ $request->created_at }}</td>
                            <td>{!! trans('visas.statuses.'.$request->status.'.style') !!}</td>
                            <td><a href="{{ route('visa.request', [$request->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- /.box -->
    </div>
    @endif

    @if(count($latestHotelsRequests))
    <div class="col-md-12">
        <!-- Success box -->
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">{{ trans('hotels.hotel-requests') }}</h3>
                <div class="box-tools pull-right">
                    <div class="btn-group" data-toggle="btn-toggle">
                        <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>{{ trans('common.request-code') }}</th>
                        <th>{{ trans('hotels.name') }}</th>
                        <th>{{ trans('cities.city') }}</th>
                        <th>{{ trans('users.fullname') }}</th>
                        <th>{{ trans('common.created-at') }}</th>
                        <th>{{ trans('common.status') }}</th>
                        <th></th>
                    </tr>
                    @foreach($latestHotelsRequests as $request)
                        <tr>
                            <td>{{ $request->code }}</td>
                            <td>{{ $request->hotel->name_ar }}<br/>{{ $request->hotel->name_fr }}</td>
                            <td>{{ $request->hotel->city->name_ar }}<br/>{{ $request->hotel->city->name_fr }}</td>
                            <td>{{ $request->user->fullname }}</td>
                            <td>{{ $request->created_at }}</td>
                            <td>{!! trans('hotels.statuses.'.$request->status.'.style') !!}</td>
                            <td><a href="{{ route('hotels.request', [$request->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- /.box -->
    </div>
    @endif

    @if(count($latestHotelsCustomRequests))
        <div class="col-md-12">
            <!-- Success box -->
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">{{ trans('hotels.hotel-custom-requests') }}</h3>
                    <div class="box-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <th>{{ trans('users.fullname') }}</th>
                            <th>{{ trans('hotels.destination') }}</th>
                            <th>{{ trans('hotels.hotel') }}</th>
                            <th>{{ trans('hotels.budget') }}</th>
                            <th>{{ trans('hotels.rooms-count') }}</th>
                            <th>{{ trans('hotels.adults-count') }}</th>
                            <th>{{ trans('hotels.childes-count') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th></th>
                        </tr>
                        @foreach($latestHotelsCustomRequests as $request)
                            <tr>
                                <td>{{ $request->code }}</td>
                                <td>{{ $request->user->fullname }}</td>
                                <td>{{ $request->destination }}</td>
                                <td>{{ $request->hotel }}</td>
                                <td>{{ $request->budget }}</td>
                                <td>{{ $request->rooms_count }}</td>
                                <td>{{ $request->adults_count }}</td>
                                <td>{{ $request->childes_count }}</td>
                                <td>{!! trans('flights.statuses.'.$request->status.'.style') !!}</td>
                                <td><a href="{{ route('hotels.request.custom', [$request->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- /.box -->
        </div>
    @endif

    @if(count($latestTripsRequests))
    <div class="col-md-12">
        <!-- Success box -->
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">{{ trans('trips.trips-requests') }}</h3>
                <div class="box-tools pull-right">
                    <div class="btn-group" data-toggle="btn-toggle">
                        <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>{{ trans('common.request-code') }}</th>
                        <th>{{ trans('trips.title') }}</th>
                        <th>{{ trans('cities.city') }}</th>
                        <th>{{ trans('countries.title') }}</th>
                        <th>{{ trans('users.fullname') }}</th>
                        <th>{{ trans('common.created-at') }}</th>
                        <th>{{ trans('common.status') }}</th>
                        <th></th>
                    </tr>
                    @foreach($latestTripsRequests as $request)
                        <tr>
                            <td>{{ $request->code }}</td>
                            <td>{{ $request->trip->title_ar }}<br/>{{ $request->trip->title_fr }}</td>
                            <td>{{ @$request->trip->city->name_ar }}<br/>{{ @$request->trip->city->name_fr }}</td>
                            <td>{{ @$request->trip->country->name_ar }}<br/>{{ @$request->country->city->name_fr }}</td>
                            <td>{{ $request->user->fullname }}</td>
                            <td>{{ $request->created_at }}</td>
                            <td>{!! trans('trips.statuses.'.$request->status.'.style') !!}</td>
                            <td><a href="{{ route('trips.request', [$request->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- /.box -->
    </div>
    @endif

    @if(count($latestFlightsRequests))
    <div class="col-md-12">
        <!-- Success box -->
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">{{ trans('flights.flights-requests') }}</h3>
                <div class="box-tools pull-right">
                    <div class="btn-group" data-toggle="btn-toggle">
                        <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>{{ trans('common.request-code') }}</th>
                        <th>{{ trans('flights.types.type') }}</th>
                        <th>{{ trans('flights.adults-count') }}</th>
                        <th>{{ trans('flights.childes-count') }}</th>
                        <th>{{ trans('flights.direct') }}</th>
                        <th>{{ trans('users.fullname') }}</th>
                        <th>{{ trans('common.created-at') }}</th>
                        <th>{{ trans('common.status') }}</th>
                        <th></th>
                    </tr>
                    @foreach($latestFlightsRequests as $request)
                        <tr>
                            <td>{{ $request->code }}</td>
                            <td>{{ trans('flights.types.'.$request->type) }}</td>
                            <td>{{ $request->adults_count }}</td>
                            <td>{{ $request->childes_count }}</td>
                            <td><img src="{{ asset('assets/backend/img/status_'.$request->direct.'.png') }}"></td>
                            <td>{{ $request->user->fullname }}</td>
                            <td>{{ $request->created_at }}</td>
                            <td>{!! trans('flights.statuses.'.$request->status.'.style') !!}</td>
                            <td><a href="{{ route('flights.request', [$request->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- /.box -->
    </div>
    @endif

@stop
