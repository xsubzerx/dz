@extends('backend.common.template')

@section('title', trans('navigations.navigations'))

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('navigations.title') }}</th>
                            <th>{{ trans('navigations.parent') }}</th>
                            <th>{{ trans('navigations.order') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allItems as $item)
                            <tr>
                                <td>{{ $item->title_ar }}<br/>{{ $item->title_fr }}</td>
                                <td>{!! ($item->parentItem) ? $item->parentItem->title_ar .'<br/>'. $item->parentItem->title_fr : trans('common.none') !!}</td>
                                <td>{{ $item->order }}</td>
                                <td><img src="{{ asset('assets/backend/img/status_'.$item->status.'.png') }}" /></td>
                                <td><a href="{{ route('navigations.edit', [$item->id]) }}"><i class="fa fa-edit" aria-hidden="true"></i></a></td>
                                <td>
                                    {{ Form::open(['url' => route('navigations.destroy', [$item->id]), 'method' => 'DELETE']) }}
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="confirm('{{trans('common.delete-confirm')}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('navigations.title') }}</th>
                            <th>{{ trans('navigations.parent') }}</th>
                            <th>{{ trans('navigations.order') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
        });
    </script>
@stop