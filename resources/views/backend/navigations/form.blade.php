@extends('backend.common.template')

@section('title', $title)

@section('content')
    {{ (isset($itemData)) ? Form::model($itemData, array('url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('navigations.update', [$itemData->id])), 'method' => 'PUT')) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('navigations.store'))]) }}
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif
        </div>

        <!-- left column -->
        <div class="col-md-8">
            @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                <div class='box box-info'>
                    <div class='box-header'>
                        <h3 class='box-title'><small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="form-group">
                            {{ Form::label('title_'.$localeCode, trans('navigations.title').':') }}
                            {{ Form::text('title_'.$localeCode, old('title_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('navigations.title'))) }}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="col-md-4">
            <div class="box box-success">
                <div class='box-header'>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class='box-body pad'>
                    <div class="form-group">
                        {{ Form::label('parent', trans('navigations.parent').':') }}
                        {{ Form::select('parent', $parents, old('parent'), ['id' => 'link', 'class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('link', trans('navigations.link').':') }}
                        {{ Form::select('link', $menuList, old('link'), ['id' => 'link', 'class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('order', trans('navigations.order').':') }}
                        {{ Form::number('order', old('order'), array('class' => 'form-control', 'placeholder' => trans('navigations.order'))) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('status', trans('common.status').':') }}
                        {{ Form::select('status', array('1' => trans('common.active'),'0' => trans('common.deactive')), old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                    </div>
                </div>
            </div>

            {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
        </div>
    </div>
    {{ Form::close() }}
@stop