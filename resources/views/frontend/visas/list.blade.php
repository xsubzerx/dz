@extends('frontend.common.template')

@section('title', trans('visas.visas'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('visas.visas') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('visas.visas') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section class="service-section">
        <div class="container">
            <div class="row">
                @foreach($allVisas as $vCountry)
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                        <div class="single-service">
                            <a href="{{ route('site.visa.country', [$vCountry->slug]) }}">
                                <div class="simg">
                                    <img src="{{ route('image.medium', ['countries', $vCountry->flag])  }}" alt="{{ $vCountry->name }}" class="img-responsive">
                                </div>
                                <h4>{{ $vCountry->name }}</h4>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 margin10 text-center">
            {{ $allVisas->links() }}
        </div>
        <div class="clearfix"></div>
    </section>
@stop