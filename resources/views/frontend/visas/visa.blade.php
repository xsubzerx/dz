@extends('frontend.common.template')

@section('title', $title)

@section('styles')
    {!! Minify::stylesheet('/assets/frontend/css/dropzone.min.css') !!}
@append

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ $title }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li><a href="#">{{ trans('visas.visas') }}</a></li>
                <li class="active">{{ $country->name }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact ">
        <div class="container">
            <div class="row">
                @if(Session::has('msg'))<div class="col-md-6 col-md-offset-3">{!! Session::get('msg') !!}</div>@endif
                {{ Form::open(['url' => route('site.visa.request'), 'id' => 'my-awesome-dropzone', 'files' => true]) }}
                    <div class="col-md-10 col-lg-offset-1">
                        <div class="form-group col-md-4 col-sm-4">
                            <label for="count" class="control-label col-md-6 col-sm-12">{{ trans('visas.count') }}</label>
                            <div class="col-md-6 col-sm-12 form-group">
                                {{ Form::select('count', ['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10'], '1', ['class' => 'form-control', 'id' => 'count']) }}
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-sm-6">
                            <label for="count" class="control-label col-md-4 col-sm-12">{{ trans('visas.title') }}</label>
                            <div class="col-md-8 col-sm-12 form-group">
                                {{ Form::select('visa', $country->visasList(), null, ['class' => 'form-control', 'id' => 'visa']) }}
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <button type="button" id="continue" class="btn btn-primary">{{ trans('common.continue') }}</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="divider margin20 hr2"><span></span></div>
                    <div class="col-xs-12 col-sm-12 col-md-12" id="description"></div>
                    <div class="col-xs-12 col-sm-12 col-md-12" id="docs"></div>
                    <div class="col-xs-12 col-sm-12 col-md-12" id="shipping"></div>
                    {{ Form::hidden('total', null, ['id' => 'total']) }}
                    {{ Form::hidden('code', null, ['id' => 'jscode']) }}
                {{ Form::close() }}
            </div>
        </div>
    </section>

    <div class="sc_modal_section">
        <div class="modal fade" id="terms">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">{{ trans('settings.visas-terms') }}</h4>
                    </div>
                    <div class="modal-body">
                        {!! $config->visas_terms !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    {!! Minify::javascript('/assets/frontend/js/dropzone.min.js') !!}
    <script type="application/javascript">
        $(document).ready(function() {
            var uploadCode = new Date().valueOf()+Math.random().toString(36).substr(2, 9);
            $('#jscode').val(uploadCode);
            $('#continue').on('click', function(){
                var visa_count = $('#count').val();
                var visa_type = $('#visa').val();
                $('#docs').html('');
                getVisaDocs(visa_count, visa_type, uploadCode);
            });

            function getVisaDocs(vc, vt, code) {
                $.ajax({
                    url: '{{route("site.visa.documents")}}',
                    method: 'POST',
                    data: {visa: vt, count: vc},
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    success: function (response) {
                        if(response.description != ''){
                            $('#description').html(response.description);
                        }
                        if(response.documents != ''){
                            $('#docs').append(response.documents);
                            for(var i=0;i<vc;i++){
                                $.each(response.ids, function (indx, val) {
                                    new Dropzone("div#file_"+i+"_"+val, {
                                        url: "{{route('upload.temp')}}",
                                        paramName: "file-"+i+'-'+val+'@'+code,
                                        maxFilesize: 2,
                                        maxFiles: 1,
                                        uploadMultiple: false,
                                        addRemoveLinks: true,
                                        maxfilesexceeded: function(file) {
                                            this.removeAllFiles();
                                            this.addFile(file);
                                        },
                                        acceptedFiles: 'image/*'
                                    });
                                });
                            }
                        }
                        if(response.shipping != ''){
                            $('#shipping').html(response.shipping);
                        }
                        $('#total').val(response.total);
                        if(response.is_logged_in) {
                            $('#shipping').append(
                                    '<div class="col-md-8 col-sm-12"><input type="checkbox" name="terms" value="1" required> {!! trans('visas.accept-terms') !!}</div>' +
                                    '<div class="col-md-4 col-sm-12"><input type="submit" class="btn btn-primary pull-right" value="{{trans('common.request-now')}}" >&nbsp;<h4 class="pull-right margin5" id="totalPrice">'+response.total+' {{trans('common.dzd')}} <i class="fa fa-angle-double-left"></i></h4></div>'
                            );
                        }else{
                            $('#shipping').append('<a href="{{route('login')}}" class="btn btn-danger pull-right" >{{trans('auth.login')}}</a>&nbsp;<h4 class="pull-right margin5" id="totalPrice">'+response.total+' {{trans('common.dzd')}} <i class="fa fa-angle-double-left"></i></h4>');
                        }
                    }
                });
            }

            $(document).on('change', '#state', function () {
                $.ajax({
                    url: '{{route("site.visa.price")}}',
                    method: 'POST',
                    data: {visa: $('#visa').val(), count: $('#count').val(), state: $('#state').val()},
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    success: function (response) {
                        $('#totalPrice').html(response.price+' {{trans('common.dzd')}}');
                        $('#total').val(response.price);
                    }
                });
            });

        });
    </script>
@append