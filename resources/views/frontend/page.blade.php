@extends('frontend.common.template')

@section('title', $pageData->title)

@section('content')

    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ $pageData->title }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ $pageData->title }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section>
        <div class="container">
            <div class="row">
                @if($pageData->image)
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="pix_sohortcode_image_slider border_0 slider_1">
                        <div class="flexslider pix_sc_slider1">
                            <img src="{{ route('image.medium', ['pages', $pageData->image]) }}" alt="{{ $pageData->title }}">
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-xs-12 {{ ($pageData->image) ? 'col-sm-8 col-md-8' : 'col-sm-12 col-md-12' }}">
                    {!! $pageData->content !!}
                </div>
            </div>
        </div>
    </section>

@stop