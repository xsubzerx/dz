@extends('frontend.common.template')

@section('title', trans('auth.register'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('auth.register') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="#">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('auth.register') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-offset-1">
                    <form class="contact-form" role="form" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if($agency)
                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                            <label for="photo" class="control-label">
                                {{ trans('users.agency-logo') }}
                                @if ($errors->has('image'))
                                    <span class="help-block">({{ $errors->first('image') }})</span>
                                @endif
                            </label>
                            <input id="photo" type="file" class="form-control" name="image">
                        </div>

                        <div class="col-md-6 col-sm-12 form-group">
                            <div id="photo-preview">
                                <img src="{{ asset('assets/backend/img/no-image-found.jpg') }}" class="img-thumbnail">
                            </div>
                        </div>
                        @endif

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label for="firstname" class="control-label">
                                {{ ($agency) ? trans('users.agency-name') : trans('users.firstname') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('firstname'))
                                    <span class="help-block">({{ $errors->first('firstname') }})</span>
                                @endif
                            </label>
                            <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" required autofocus>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="lastname" class="control-label">
                                {{ ($agency) ? trans('users.agency-owner') : trans('users.lastname') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('lastname'))
                                    <span class="help-block">({{ $errors->first('lastname') }})</span>
                                @endif
                            </label>
                            <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" required>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">
                                {{ trans('users.email') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('email'))
                                    <span class="help-block">({{ $errors->first('email') }})</span>
                                @endif
                            </label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="control-label">
                                {{ trans('users.phone') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('phone'))
                                    <span class="help-block">({{ $errors->first('phone') }})</span>
                                @endif
                            </label>
                            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                            <label for="section" class="control-label">
                                {{ trans('users.section') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('section'))
                                    <span class="help-block">({{ $errors->first('section') }})</span>
                                @endif
                            </label>
                            <input id="section" type="text" class="form-control" name="section" value="{{ old('section') }}" required>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('town') ? ' has-error' : '' }}">
                            <label for="town" class="control-label">
                                {{ trans('users.town') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('town'))
                                    <span class="help-block">({{ $errors->first('town') }})</span>
                                @endif
                            </label>
                            <input id="town" type="text" class="form-control" name="town" value="{{ old('town') }}" required>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            <label for="state" class="control-label">
                                {{ trans('users.state') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('state'))
                                    <span class="help-block">({{ $errors->first('state') }})</span>
                                @endif
                            </label>
                            <input id="state" type="text" class="form-control" name="state" value="{{ old('state') }}" required>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="control-label">
                                {{ trans('users.address') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('address'))
                                    <span class="help-block">({{ $errors->first('address') }})</span>
                                @endif
                            </label>
                            <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required>
                        </div>


                            @if($agency)
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                                <label for="website" class="control-label">{{ trans('users.website') }}</label>
                                <input id="website" type="text" class="form-control" name="website" value="{{ old('website') }}">
                            </div>
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
                                <label for="facebook" class="control-label">{{ trans('users.facebook') }}</label>
                                <input id="facebook" type="text" class="form-control" name="facebook" value="{{ old('facebook') }}">
                            </div>
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('license_number') ? ' has-error' : '' }}">
                                <label for="license_number" class="control-label">
                                    {{ trans('users.license-number') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                    @if ($errors->has('license_number'))
                                        <span class="help-block">({{ $errors->first('license_number') }})</span>
                                    @endif
                                </label>
                                <input id="license_number" type="text" class="form-control" name="license_number" value="{{ old('license_number') }}" required>
                            </div>
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('license_year') ? ' has-error' : '' }}">
                                <label for="license_year" class="control-label">
                                    {{ trans('users.license-year') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                    @if ($errors->has('license_year'))
                                        <span class="help-block">({{ $errors->first('license_year') }})</span>
                                    @endif
                                </label>
                                {{ Form::select('license_year', $years, old('license_year'), ['id' => 'license_year', 'class' => 'form-control']) }}
                            </div>
                        @else
                            <div class="col-md-12 col-sm-12 form-group{{ $errors->has('passport') ? ' has-error' : '' }}">
                                <label for="passport" class="control-label">{{ trans('users.passport') }}</label>
                                <input id="passport" type="number" class="form-control" name="passport" value="{{ old('passport') }}">
                            </div>
                        @endif

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">
                                {{ trans('users.password') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('password'))
                                    <span class="help-block">({{ $errors->first('password') }})</span>
                                @endif
                            </label>
                            <input id="password" type="password" class="form-control" name="password" required>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="control-label">
                                {{ trans('users.repassword') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">({{ $errors->first('password_confirmation') }})</span>
                                @endif
                            </label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 col-sm-12">
                            <input type="hidden" name="type" value="{{ ($agency) ? 'agency' : 'user' }}">
                            <button type="submit" class="message-sub pull-right btn btn-blue">{{ trans('auth.register') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#photo").change(function() {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#photo-preview').find('img').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@append
