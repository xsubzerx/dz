@extends('frontend.common.template')

@section('title', trans('auth.login'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('auth.password-reset') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="#">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('auth.password-reset') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-offset-3">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(!empty($errors->all()))
                        <ul class="callout callout-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                    @endif
                    <form class="contact-form" role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('users.email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                        <button type="submit" class="message-sub pull-right btn btn-blue">{{ trans('contact.send') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop