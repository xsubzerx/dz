@extends('frontend.common.template')

@section('title', trans('auth.login'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('auth.password-reset') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="#">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('auth.password-reset') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-offset-3">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(!empty($errors->all()))
                        <ul class="callout callout-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                    @endif
                    {{ Form::open(['url' => route('password.request'), 'class' => 'contact-form', 'role' => 'form']) }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="{{ trans('users.email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control" name="password" placeholder="{{ trans('auth.password') }}">
                            @if ($errors->has('password'))
                                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="{{ trans('auth.repass') }}">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                        {{ Form::hidden('token', $token) }}
                        <button type="submit" class="message-sub pull-right btn btn-blue">{{ trans('auth.password-reset') }}</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@stop