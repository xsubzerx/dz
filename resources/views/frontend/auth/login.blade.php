@extends('frontend.common.template')

@section('title', trans('auth.login'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('auth.login') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="#">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('auth.login') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-offset-3">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                    <form class="contact-form" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                            @endif
                        </div>
                        <div class="form-group pull-left">
                            <input type="checkbox" name="remember"/> {{ trans('auth.remember') }}
                        </div>
                        <div class="form-group pull-right">
                            <a href="password/reset">{{ trans('auth.forgot-password') }}</a>
                        </div>
                        <div class="clearfix"></div>
                        <button type="submit" class="message-sub btn btn-blue">{{ trans('auth.sign-me-in') }}</button>
                        <a href="{{ route('register') }}" class="message-sub btn btn-danger">{{ trans('auth.don-have-account') }}</a>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop