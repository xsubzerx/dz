@extends('frontend.common.template')

@section('title', trans('users.my-hotels-custom'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('users.my-hotels-custom') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('users.my-hotels-custom') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th>{{ trans('common.request-code') }}</th>
                        <th>{{ trans('hotels.destination') }}</th>
                        <th>{{ trans('hotels.hotel') }}</th>
                        <th>{!! trans('hotels.checkin').'<br>'.trans('hotels.checkout') !!}</th>
                        <th>{{ trans('hotels.budget') }}</th>
                        <th>{{ trans('hotels.rooms-count') }}</th>
                        <th>{{ trans('hotels.adults-count') }}</th>
                        <th>{!! trans('hotels.childes-count').'<br>'.trans('hotels.childes-ages') !!}</th>
                        <th>{{ trans('hotels.note') }}</th>
                        <th>{{ trans('common.status') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($requests as $request)
                        <tr>
                            <td>{{ $request->code }}</td>
                            <td>{{ $request->destination }}</td>
                            <td>{{ $request->hotel }}</td>
                            <td>
                                <p>{{ $request->checkin }}</p>
                                <p>{{ $request->checkout }}</p>
                            </td>
                            <td>{{ $request->budget }}</td>
                            <td>{{ $request->rooms_count }}</td>
                            <td>{{ $request->adults_count }}</td>
                            <td>
                                {{ $request->childes_count }}
                                @if(is_array($request->childes_ages_list0) && count($request->childes_ages_list))
                                <br>
                                ({{ implode(', ', $request->childes_ages_list) }})
                                @endif
                            </td>
                            <td>@if($request->note)<button class="btn btn-primary" data-toggle="popover" data-content="{{ $request->note }}">{{ trans('common.preview') }}</button>@endif</td>
                            <td>{!! trans('flights.statuses.'.$request->status.'.style') !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-toggle="popover"]').popover();
        });
    </script>
@append