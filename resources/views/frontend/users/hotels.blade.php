@extends('frontend.common.template')

@section('title', trans('users.my-hotels'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('users.my-hotels') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('users.my-hotels') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th>{{ trans('common.request-code') }}</th>
                        <th>{{ trans('hotels.name') }}</th>
                        <th>{!! trans('cities.city').'<br>'.trans('countries.title') !!}</th>
                        <th>{{ trans('hotels.rooms') }}</th>
                        <th>{!! trans('hotels.checkin').'<br>'.trans('hotels.checkout') !!}</th>
                        <th>{{ trans('hotels.childes-count') }}</th>
                        <th>{{ trans('common.price') }}</th>
                        <th>{{ trans('common.payment-reset') }}</th>
                        <th>{{ trans('common.status') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($requests as $request)
                        <tr>
                            <td>{{ $request->code }}</td>
                            <td>{{ $request->hotel->name }}</td>
                            <td>
                                <p>{{ $request->hotel->city->name }}</p>
                                <p>{{ $request->hotel->city->country->name }}</p>
                            </td>
                            <td>
                                @foreach($request->hotel->rooms as $room)
                                    @if(!in_array($room->id, array_keys(json_decode($request->rooms, true)))) @continue @endif
                                    <p>{{ '('.json_decode($request->rooms, true)[$room->id].') '.$room->title }}</p>
                                @endforeach
                            </td>
                            <td>
                                <p>{{ $request->checkin }}</p>
                                <p>{{ $request->checkout }}</p>
                            </td>
                            <td>{{ count(json_decode($request->childes)) }}</td>
                            <td>{{ $request->total.' '.trans('common.dzd') }}</td>
                            <td>{!! ($request->payment) ? '<a href="'.route('image', ['payments', $request->payment->payment]).'" class="image-link"><img src="'.route('image.thumbnail', ['payments', $request->payment->payment]).'"></a>' : '<a href="'.route('site.payments.upload', ['action' => 'hotel', 'id' => $request->id]).'"><i class="fa fa-upload"></i> '.trans('common.upload').'</a>' !!}</td>
                            <td>{!! trans('hotels.statuses.'.$request->status.'.style') !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop

@section('styles')
    {!! Minify::stylesheet('/assets/frontend/css/magnific-popup.css') !!}
@append

@section('scripts')
    {!! Minify::javascript('/assets/frontend/js/jquery.magnific-popup.min.js') !!}
    <script>
        $(document).ready(function() {
            $('.image-link').magnificPopup({type:'image'});
        });
    </script>
@append