@extends('frontend.common.template')

@section('title', trans('users.my-trips'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('users.my-trips') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('users.my-trips') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th>{{ trans('common.request-code') }}</th>
                        <th>{{ trans('trips.title') }}</th>
                        <th>{!! trans('cities.city').'<br>'.trans('countries.title') !!}</th>
                        <th>{{ trans('hotels.quantity') }}</th>
                        <th>{{ trans('hotels.childes-count') }}</th>
                        <th>{{ trans('common.price') }}</th>
                        <th>{{ trans('common.payment-reset') }}</th>
                        <th>{{ trans('common.status') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($requests as $request)
                        <tr>
                            <td>{{ $request->code }}</td>
                            <td>{{ $request->trip->title }}</td>
                            <td>
                                <p>{{ @$request->trip->city->name }}</p>
                                <p>{{ $request->trip->country->name }}</p>
                            </td>
                            <td>
                                @if($request->single)<p>( {{ $request->single }} ) {{ trans('trips.single-room') }}</p>@endif
                                @if($request->double)<p>( {{ $request->double }} ) {{ trans('trips.double-room') }}</p>@endif
                                @if($request->triple)<p>( {{ $request->triple }} ) {{ trans('trips.triple-room') }}</p>@endif
                                @if($request->quad)<p>( {{ $request->quad }} ) {{ trans('trips.quad-room') }}</p>@endif
                                @if($request->quinary)<p>( {{ $request->quinary }} ) {{ trans('trips.quinary-room') }}</p>@endif
                            </td>
                            <td>
                                @if($request->childes_first)<p>( {{ $request->childes_first }} ) {{ trans('trips.childes-first') }}</p>@endif
                                @if($request->childes_second)<p>( {{ $request->childes_second }} ) {{ trans('trips.childes-second') }}</p>@endif
                                @if($request->childes_third)<p>( {{ $request->childes_third }} ) {{ trans('trips.childes-third') }}</p>@endif
                            </td>
                            <td>{{ $request->total.' '.trans('common.dzd') }}</td>
                            <td>@if($request->trip->created_by != auth()->guard('user')->user()->id || $request->payment){!! ($request->payment) ? '<a href="'.route('image', ['payments', $request->payment->payment]).'" class="image-link"><img src="'.route('image.thumbnail', ['payments', $request->payment->payment]).'"></a>' : '<a href="'.route('site.payments.upload', ['action' => 'trip', 'id' => $request->id]).'"><i class="fa fa-upload"></i> '.trans('common.upload').'</a>' !!}@endif</td>
                            <td>{!! trans('trips.statuses.'.$request->status.'.style') !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop

@section('styles')
    {!! Minify::stylesheet('/assets/frontend/css/magnific-popup.css') !!}
@append

@section('scripts')
    {!! Minify::javascript('/assets/frontend/js/jquery.magnific-popup.min.js') !!}
    <script>
        $(document).ready(function() {
            $('.image-link').magnificPopup({type:'image'});
        });
    </script>
@append