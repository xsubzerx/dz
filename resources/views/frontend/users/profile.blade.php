@extends('frontend.common.template')

@section('title', trans('users.profile'))

@section('styles')
    <link href="{{ asset('assets/backend/css/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <style>
        .colorpicker {
            right:inherit;
        }
    </style>
@append

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('users.profile') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('users.profile') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                @if(Session::has('msg'))<div class="col-md-10 col-lg-offset-1">{!! Session::get('msg') !!}</div>@endif
                @if(!empty($errors->all()))
                    <div class="col-md-10 col-lg-offset-1">
                        <ul class="alert alert-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                    </div>
                @endif
                <div class="col-md-10 col-lg-offset-1">
                    {{ Form::model(auth()->guard('user')->user(), ['url' => route('site.profile'), 'method' => 'PUT', 'class' => 'contact-form', 'files' => true]) }}
                        @if(auth()->guard('user')->user()->type == 'agency')
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                                <label for="photo" class="control-label">
                                    {{ trans('users.agency-logo') }}
                                    @if ($errors->has('image'))
                                        <span class="help-block">({{ $errors->first('image') }})</span>
                                    @endif
                                </label>
                                <input id="photo" type="file" class="form-control" name="image">
                            </div>

                            <div class="col-md-6 col-sm-12 form-group">
                                <div id="photo-preview">
                                    <img src="{{ (auth()->guard('user')->user()->photo) ? route('image', ['users', auth()->guard('user')->user()->photo]) : asset('assets/backend/img/no-image-found.jpg') }}" class="img-thumbnail">
                                </div>
                            </div>
                        @endif

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label for="firstname" class="control-label">
                                {{ (auth()->guard('user')->user()->type == 'agency') ? trans('users.agency-name') : trans('users.firstname') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('firstname'))
                                    <span class="help-block">({{ $errors->first('firstname') }})</span>
                                @endif
                            </label>
                            {{ Form::text('firstname', old('firstname'), ['id' => 'firstname', 'class' => 'form-control', 'autofocus', 'required']) }}
                        </div>

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="lastname" class="control-label">
                                {{ (auth()->guard('user')->user()->type == 'agency') ? trans('users.agency-owner') : trans('users.lastname') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                @if ($errors->has('lastname'))
                                    <span class="help-block">({{ $errors->first('lastname') }})</span>
                                @endif
                            </label>
                            {{ Form::text('lastname', old('lastname'), ['id' => 'lastname', 'class' => 'form-control', 'required']) }}
                        </div>

                        <div class="col-md-6 col-sm-12 form-group>
                            <label for="email" class="control-label">{{ trans('users.email') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small></label>
                            {{ Form::email('email', old('email'), ['id' => 'email', 'class' => 'form-control','readonly']) }}
                        </div>

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="control-label">
                                {{ trans('users.phone') }}
                                @if ($errors->has('phone'))
                                    <span class="help-block">({{ $errors->first('phone') }})</span>
                                @endif
                            </label>
                            {{ Form::text('phone', old('phone'), ['id' => 'phone', 'class' => 'form-control', 'required']) }}
                        </div>

                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                                <label for="section" class="control-label">
                                    {{ trans('users.section') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                    @if ($errors->has('section'))
                                        <span class="help-block">({{ $errors->first('section') }})</span>
                                    @endif
                                </label>
                                {{ Form::text('section', old('section'), ['id' => 'section', 'class' => 'form-control', 'required']) }}
                            </div>
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('town') ? ' has-error' : '' }}">
                                <label for="town" class="control-label">
                                    {{ trans('users.town') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                    @if ($errors->has('town'))
                                        <span class="help-block">({{ $errors->first('town') }})</span>
                                    @endif
                                </label>
                                {{ Form::text('town', old('town'), ['id' => 'town', 'class' => 'form-control', 'required']) }}
                            </div>

                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                <label for="state" class="control-label">
                                    {{ trans('users.state') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                    @if ($errors->has('state'))
                                        <span class="help-block">({{ $errors->first('state') }})</span>
                                    @endif
                                </label>
                                {{ Form::text('state', old('state'), ['id' => 'state', 'class' => 'form-control', 'required']) }}
                            </div>

                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="control-label">
                                    {{ trans('users.address') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                    @if ($errors->has('address'))
                                        <span class="help-block">({{ $errors->first('address') }})</span>
                                    @endif
                                </label>
                                {{ Form::text('address', old('address'), ['id' => 'address', 'class' => 'form-control', 'required']) }}
                            </div>

                        @if(auth()->guard('user')->user()->type == 'agency')
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                                <label for="website" class="control-label">{{ trans('users.website') }}</label>
                                {{ Form::text('website', old('website'), ['id' => 'website', 'class' => 'form-control']) }}
                            </div>
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
                                <label for="facebook" class="control-label">{{ trans('users.facebook') }}</label>
                                {{ Form::text('facebook', old('facebook'), ['id' => 'facebook', 'class' => 'form-control']) }}
                            </div>
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('license_number') ? ' has-error' : '' }}">
                                <label for="license_number" class="control-label">
                                    {{ trans('users.license-number') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                    @if ($errors->has('license_number'))
                                        <span class="help-block">({{ $errors->first('license_number') }})</span>
                                    @endif
                                </label>
                                {{ Form::text('license_number', old('license_number'), ['id' => 'license_number', 'class' => 'form-control', 'required']) }}
                            </div>
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('license_year') ? ' has-error' : '' }}">
                                <label for="license_year" class="control-label">
                                    {{ trans('users.license-year') }} <small><i class="fa fa-asterisk text-danger" aria-hidden="true"></i></small>
                                    @if ($errors->has('license_year'))
                                        <span class="help-block">({{ $errors->first('license_year') }})</span>
                                    @endif
                                </label>
                                {{ Form::select('license_year', $years, old('license_year'), ['id' => 'license_year', 'class' => 'form-control']) }}
                            </div>
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('background_color') ? ' has-error' : '' }}">
                                <label for="license_number" class="control-label">
                                    {{ trans('users.background-color') }}
                                    @if ($errors->has('background_color'))
                                        <span class="help-block">({{ $errors->first('background_color') }})</span>
                                    @endif
                                </label>
                                {{ Form::text('background_color', old('background_color'), ['id' => 'background_color', 'class' => 'form-control colorpicker']) }}
                            </div>
                            <div class="col-md-6 col-sm-12 form-group{{ $errors->has('font_color') ? ' has-error' : '' }}">
                                <label for="license_year" class="control-label">
                                    {{ trans('users.font-color') }}
                                    @if ($errors->has('font_color'))
                                        <span class="help-block">({{ $errors->first('font_color') }})</span>
                                    @endif
                                </label>
                                {{ Form::text('font_color', old('font_color'), ['id' => 'font_color', 'class' => 'form-control colorpicker']) }}
                            </div>
                        @else
                            <div class="col-md-12 col-sm-12 form-group{{ $errors->has('passport') ? ' has-error' : '' }}">
                                <label for="passport" class="control-label">{{ trans('users.passport') }}</label>
                                {{ Form::number('passport', old('passport'), ['id' => 'passport', 'class' => 'form-control']) }}
                            </div>
                        @endif

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">{{ trans('users.password') }}
                                @if ($errors->has('password'))
                                    <span class="help-block">({{ $errors->first('password') }})</span>
                                @endif
                            </label>
                            <input id="password" type="password" class="form-control" name="password">
                        </div>

                        <div class="col-md-6 col-sm-12 form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="control-label">{{ trans('users.repassword') }}
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">({{ $errors->first('password_confirmation') }})</span>
                                @endif
                            </label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 col-sm-12">
                            <button type="submit" class="message-sub pull-right btn btn-blue">{{ trans('common.update') }}</button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script src="{{ asset('assets/backend/js/plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.colorpicker').colorpicker();

            $("#photo").change(function() {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#photo-preview').find('img').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@append