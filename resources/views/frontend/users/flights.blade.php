@extends('frontend.common.template')

@section('title', trans('users.my-flights'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('users.my-flights') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('users.my-flights') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th>{{ trans('common.request-code') }}</th>
                        <th>{{ trans('flights.types.type') }}</th>
                        <th>{{ trans('flights.adults-count') }}</th>
                        <th>{{ trans('flights.childes-count') }}</th>
                        <th>{{ trans('flights.direct') }}</th>
                        <th>{{ trans('common.status') }}</th>
                        <th>{{ trans('common.created-at') }}</th>
                        <th>{{ trans('common.preview') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($requests as $request)
                        <tr>
                            <td>{{ $request->code }}</td>
                            <td>{{ trans('flights.types.'.$request->type) }}</td>
                            <td>{{ $request->adults_count }}</td>
                            <td>{{ $request->childes_count }}</td>
                            <td><img src="{{ asset('assets/frontend/img/status_'.$request->direct.'.png') }}"></td>
                            <td>{!! trans('flights.statuses.'.$request->status.'.style') !!}</td>
                            <td>{{ $request->created_at }}</td>
                            <td><a href="{{ route('site.flight.show', [$request->id]) }}"><i class="fa fa-eye"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop