@extends('frontend.common.template')

@section('title', trans('users.my-visas'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('users.my-visas') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('users.my-visas') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th>{{ trans('common.request-code') }}</th>
                        <th>{{ trans('visas.title') }}</th>
                        <th>{{ trans('countries.title') }}</th>
                        <th>{{ trans('common.count') }}</th>
                        <th>{{ trans('common.price') }}</th>
                        <th>{{ trans('common.payment-reset') }}</th>
                        <th>{{ trans('common.status') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($requests as $request)
                        <tr>
                            <td>{{ $request->code }}</td>
                            <td>{{ $request->visa->title }}</td>
                            <td>{{ $request->visa->country->name }}</td>
                            <td>{{ $request->count }}</td>
                            <td>{{ ($request->visa->price * $request->count) }}</td>
                            <td>{!! ($request->payment) ? '<a href="'.route('image', ['payments', $request->payment->payment]).'" class="image-link"><img src="'.route('image.thumbnail', ['payments', $request->payment->payment]).'"></a>' : '<a href="'.route('site.payments.upload', ['action' => 'visa', 'id' => $request->id]).'"><i class="fa fa-upload"></i> '.trans('common.upload').'</a>' !!}</td>
                            <td>{!! trans('visas.statuses.'.$request->status.'.style') !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop

@section('styles')
    {!! Minify::stylesheet('/assets/frontend/css/magnific-popup.css') !!}
@append

@section('scripts')
    {!! Minify::javascript('/assets/frontend/js/jquery.magnific-popup.min.js') !!}
    <script>
        $(document).ready(function() {
            $('.image-link').magnificPopup({type:'image'});
        });
    </script>
@append