@extends('frontend.common.template')

@section('title', trans('users.my-flights').' ('.$flight->code.' )')

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('users.my-flights').' ('.$flight->code.' )' }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li><a href="{{ route('site.my-flights') }}">{{ trans('users.my-flights') }}</a></li>
                <li class="active">{{ trans('users.my-flights').' ('.$flight->code.' )' }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="title3">{{ trans('flights.trips') }}</h2>
                    <div class="divider margin5 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                    <table class="table table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('flights.from') }}</th>
                            <th>{{ trans('flights.to') }}</th>
                            <th>{{ trans('flights.checkin') }}</th>
                            @if($flight->type == 'return')<th>{{ trans('flights.checkout') }}</th>@endif
                        </tr>
                        </thead>
                        <tbody>
                                @if($flight->type == 'multiple')
                                    @foreach($flight->trips_list as $trip)
                                        <tr>
                                            <td>{{ $trip->from }}</td>
                                            <td>{{ $trip->to }}</td>
                                            <td>{{ $trip->checkin }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>{{ $flight->trips_list->from }}</td>
                                        <td>{{ $flight->trips_list->to }}</td>
                                        <td>{{ $flight->trips_list->checkin }}</td>
                                        @if($flight->type == 'return')<td>{{ $flight->trips_list->checkout }}</td>@endif
                                    </tr>
                                @endif
                        </tbody>
                    </table>

                    @if($flight->adults_count > 0 && count($flight->adults_list))
                    <h2 class="title3 margin20">{{ trans('flights.adults') }}</h2>
                    <div class="divider margin5 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                    <table class="table table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('flights.title') }}</th>
                            <th>{{ trans('flights.name') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($flight->adults_list as $title=>$name)
                            <tr>
                                <td>{{ $title }}</td>
                                <td>{{ $name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif

                    @if($flight->childes_count > 0 && count($flight->childes_list))
                        <h2 class="title3 margin20">{{ trans('flights.childes') }}</h2>
                        <div class="divider margin5 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                        <table class="table table-striped text-center">
                            <thead>
                            <tr>
                                <th>{{ trans('flights.birthdate') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($flight->childes_list as $birthdate)
                                <tr>
                                    <td>{{ $birthdate }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

                    @if($flight->note)
                        <h2 class="title3 margin20">{{ trans('flights.note') }}</h2>
                        <div class="divider margin5 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                        {{ $flight->note }}
                    @endif
                </div>
                <div class="col-md-4">
                    <table class="table table-striped text-center">
                        <tr>
                            <th>{{ trans('common.request-code') }}</th>
                            <td>{{ $flight->code }}</td>
                        </tr>
                        <tr>
                            <th>{{ trans('flights.types.type') }}</th>
                            <td>{{ trans('flights.types.'.$flight->type) }}</td>
                        </tr>
                        <tr>
                            <th>{{ trans('flights.adults-count') }}</th>
                            <td>{{ $flight->adults_count }}</td>
                        </tr>
                        <tr>
                            <th>{{ trans('flights.childes-count') }}</th>
                            <td>{{ $flight->childes_count }}</td>
                        </tr>
                        <tr>
                            <th>{{ trans('flights.direct') }}</th>
                            <td><img src="{{ asset('assets/frontend/img/status_'.$flight->direct.'.png') }}"></td>
                        </tr>
                        <tr>
                            <th>{{ trans('common.status') }}</th>
                            <td>{!! trans('flights.statuses.'.$flight->status.'.style') !!}</td>
                        </tr>
                        <tr>
                            <th>{{ trans('common.created-at') }}</th>
                            <td>{{ $flight->created_at }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
@stop