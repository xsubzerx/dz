@extends('frontend.common.template')

@section('title', $trip->title)

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ $trip->title }}  <small>({{ $trip->double_price }} {{ trans('common.dzd') }}) @if($trip->commission && $trip->commission > 0 && auth()->guard('user')->check() && auth()->guard('user')->user()->type == 'agency')<span class="text-danger">({{ trans('trips.commission') }}: {{ $trip->commission }} {{ trans('common.dzd') }})</span></small>@endif</h2>

            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li><a href="{{ route('site.trips.index') }}">{{ trans('trips.trips') }}</a></li>
                <li class="active">{{ $trip->title }}</li>
            </ul>
            <div class="clearfix"></div>
            <h6 class="page-title"><i class="fa fa-map-marker"></i> {{ (isset($trip->city->name)) ? $trip->city->name.', '.$trip->country->name : $trip->country->name }}</h6>
            @if(!$trip->fixed)
                 / <h6 class="page-title"><i class="fa fa-calendar"></i> {{ $trip->start_date.' '.trans('common.to').' '.$trip->end_date }}</h6>
            @endif
        </div>
    </div> <!-- end .breadcrumb-area -->

    @if(count($trip->album))
        <section class="slider-demo">
            <div class="container">
                <div class="demoSliderWrapper">
                    <div id="ei-slider2" class="ei-slider">
                        <ul class="ei-slider-large">
                            @foreach($trip->album as $k=>$photo)
                                <li><img src="{{ route('image', ['trips', $photo->photo]) }}" alt="image{{$k}}"></li>
                            @endforeach
                        </ul><!-- ei-slider-large -->
                        <ul class="ei-slider-thumbs">
                            <li class="ei-slider-element">Current</li>
                            @foreach($trip->album as $k=>$photo)
                                <li><a href="#">Slide {{$k}}</a><img src="{{ route('image.thumbnail', ['trips', $photo->photo]) }}" alt="thumb{{$k}}"></li>
                            @endforeach
                        </ul><!-- ei-slider-thumbs -->
                    </div><!-- ei-slider -->
                </div>
            </div>
        </section>
    @endif

    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-7">
                    <h2 class="title3">{{ trans('trips.description') }}</h2>
                    <div class="divider hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                    {!! $trip->description !!}
                    <div class="clearfix"></div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <h2 class="title3 margin_top5">{{ trans('trips.has') }}</h2>
                        <div class="divider margin20 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                        {!! $trip->has !!}
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <h2 class="title3 margin_top5">{{ trans('trips.has-not') }}</h2>
                        <div class="divider margin20 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                        {!! $trip->has_not !!}
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-5">
                    <h2 class="title3">{{ trans('trips.booking') }}</h2>
                    <div class="divider margin20 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                    {{ Form::open(['class' => 'contact-form']) }}
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th></th>
                            <th class="text-center"><h4>{{ trans('common.person') }}</h4></th>
                            <th class="text-center"><h4>{{ trans('hotels.quantity') }}</h4></th>
                            <th class="text-center"><h4>{{ trans('hotels.total') }}</h4></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center">{{ trans('trips.single-room') }}</td>
                            <td class="text-center">{{ $trip->single_price }}<br>{{ trans('common.dzd') }}</td>
                            <td class="text-center">{{ Form::select('single', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['class' => 'form-control']) }}</td>
                            <td class="text-center"><span id="totalSingle">0</span><br>{{ trans('common.dzd') }}</td>
                        </tr>
                        <tr>
                            <td class="text-center">{{ trans('trips.double-room') }}</td>
                            <td class="text-center">{{ $trip->double_price }}<br>{{ trans('common.dzd') }}</td>
                            <td class="text-center">{{ Form::select('double', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['class' => 'form-control']) }}</td>
                            <td class="text-center"><span id="totalDouble">0</span><br>{{ trans('common.dzd') }}</td>
                        </tr>
                        <tr>
                            <td class="text-center">{{ trans('trips.triple-room') }}</td>
                            <td class="text-center">{{ $trip->triple_price }}<br>{{ trans('common.dzd') }}</td>
                            <td class="text-center">{{ Form::select('triple', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['class' => 'form-control']) }}</td>
                            <td class="text-center"><span id="totalTriple">0</span><br>{{ trans('common.dzd') }}</td>
                        </tr>
                        @if($trip->quad_price > 0)
                        <tr>
                            <td class="text-center">{{ trans('trips.quad-room') }}</td>
                            <td class="text-center">{{ $trip->quad_price }}<br>{{ trans('common.dzd') }}</td>
                            <td class="text-center">{{ Form::select('quad', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['class' => 'form-control']) }}</td>
                            <td class="text-center"><span id="totalQuad">0</span><br>{{ trans('common.dzd') }}</td>
                        </tr>
                        @endif
                        @if($trip->quinary_price > 0)
                        <tr>
                            <td class="text-center">{{ trans('trips.quinary-room') }}</td>
                            <td class="text-center">{{ $trip->quinary_price }}<br>{{ trans('common.dzd') }}</td>
                            <td class="text-center">{{ Form::select('quinary', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['class' => 'form-control']) }}</td>
                            <td class="text-center"><span id="totalQuinary">0</span><br>{{ trans('common.dzd') }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td class="text-center">{{ trans('trips.childes-first') }}</td>
                            <td class="text-center">{{ $trip->childes_first_price }}<br>{{ trans('common.dzd') }}</td>
                            <td class="text-center">{{ Form::select('childes_first_count', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['class' => 'form-control']) }}</td>
                            <td class="text-center"><span id="totalChildes1">0</span><br>{{ trans('common.dzd') }}</td>
                        </tr>
                        <tr>
                            <td class="text-center">{{ trans('trips.childes-second') }}</td>
                            <td class="text-center">{{ $trip->childes_second_price }}<br>{{ trans('common.dzd') }}</td>
                            <td class="text-center">{{ Form::select('childes_second_count', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['class' => 'form-control']) }}</td>
                            <td class="text-center"><span id="totalChildes2">0</span><br>{{ trans('common.dzd') }}</td>
                        </tr>
                        <tr>
                            <td class="text-center">{{ trans('trips.childes-third') }}</td>
                            <td class="text-center">{{ $trip->childes_third_price }}<br>{{ trans('common.dzd') }}</td>
                            <td class="text-center">{{ Form::select('childes_third_count', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['class' => 'form-control']) }}</td>
                            <td class="text-center"><span id="totalChildes3">0</span><br>{{ trans('common.dzd') }}</td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="3"><h4>{{ trans('hotels.total') }}</h4></td>
                            <td class="text-center"><span id="totalPrice">0</span><br>{{ trans('common.dzd') }}</td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="col-md-12 col-sm-12">
                        {{ Form::hidden('total', 0) }}
                        @if(auth()->guard('user')->check())
                            <button type="submit" class="btn btn-lg btn-success pull-right">{{ trans('trips.booking') }}</button>
                        @else
                            <a href="{{ route('login') }}" class="btn btn-lg btn-danger pull-right">{{ trans('auth.login') }}</a>
                        @endif
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@stop

@section('styles')
    {!! Minify::stylesheet('/assets/frontend/shortcodes/css/eislideshow.css') !!}
@append

@section('scripts')
    {!! Minify::javascript('/assets/frontend/shortcodes/js/jquery.eislideshow.js') !!}
    <script type="application/javascript">
        $(document).ready(function(){
            $("#ei-slider2").eislideshow({
                animation           : "sides",
                autoplay            : true,
                slideshow_interval  : 5000,
                titlesFactor        : 0
            });

            var singlePr = '{{ $trip->single_price }}';
            var doublePr = '{{ $trip->double_price }}';
            var triplePr = '{{ $trip->triple_price }}';
            var quadPr = '{{ $trip->quad_price }}';
            var quinaryPr = '{{ $trip->quinary_price }}';
            var ch1Pr = '{{ $trip->childes_first_price }}';
            var ch2Pr = '{{ $trip->childes_second_price }}';
            var ch3Pr = '{{ $trip->childes_third_price }}';

            $('select[name="single"]').on('change', function () {
                $('#totalSingle').html(parseFloat(singlePr) * parseInt($(this).val()));
                calctotal();
            });
            $('select[name="double"]').on('change', function () {
                $('#totalDouble').html(parseFloat(doublePr) * parseInt($(this).val()));
                calctotal();
            });
            $('select[name="triple"]').on('change', function () {
                $('#totalTriple').html(parseFloat(triplePr) * parseInt($(this).val()));
                calctotal();
            });
            $('select[name="quad"]').on('change', function () {
                $('#totalQuad').html(parseFloat(quadPr) * parseInt($(this).val()));
                calctotal();
            });
            $('select[name="quinary"]').on('change', function () {
                $('#totalQuinary').html(parseFloat(quinaryPr) * parseInt($(this).val()));
                calctotal();
            });
            $('select[name="childes_first_count"]').on('change', function () {
                $('#totalChildes1').html(parseFloat(ch1Pr) * parseInt($(this).val()));
                calctotal();
            });
            $('select[name="childes_second_count"]').on('change', function () {
                $('#totalChildes2').html(parseFloat(ch2Pr) * parseInt($(this).val()));
                calctotal();
            });
            $('select[name="childes_third_count"]').on('change', function () {
                $('#totalChildes3').html(parseFloat(ch3Pr) * parseInt($(this).val()));
                calctotal();
            });
            
            function calctotal() {
                var total = parseFloat($('#totalSingle').html()) +
                        parseFloat($('#totalDouble').html()) +
                        parseFloat($('#totalTriple').html()) +
                        parseFloat($('#totalChildes1').html()) +
                        parseFloat($('#totalChildes2').html()) +
                        parseFloat($('#totalChildes3').html());
                if($('#totalQuad').length){
                    total += parseFloat($('#totalQuad').html());
                }
                if($('#totalQuinary').length){
                    total += parseFloat($('#totalQuinary').html());
                }
                $('#totalPrice').html(total);
                $('input[name="total"]').val(total);
            }
        });
    </script>
@append