@extends('frontend.common.template')

@section('title', trans('trips.booking'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('trips.booking') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li><a href="{{ route('site.trips.show', [$trip->id]) }}">{{ $trip->name }}</a></li>
                <li class="active">{{ trans('trips.booking') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="cart-form-wrapper">
                            <table class="table" cellspacing="0">
                                <thead>
                                <tr>
                                    <th colspan="2" class="product-name">{{ trans('trips.title') }}</th>
                                    <th class="product-quantity">{{ trans('trips.single-room') }}</th>
                                    <th class="product-quantity">{{ trans('trips.double-room') }}</th>
                                    <th class="product-quantity">{{ trans('trips.triple-room') }}</th>
                                    <th class="product-quantity">{{ trans('trips.childes-first') }}</th>
                                    <th class="product-quantity">{{ trans('trips.childes-second') }}</th>
                                    <th class="product-quantity">{{ trans('trips.childes-third') }}</th>
                                    <th class="product-subtotal">{{ trans('hotels.total') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="cart_item">
                                    <td colspan="2" class="product-name">{{ $trip->title }}</td>
                                    <td><img src="{{ asset('assets/frontend/img/status_'.intval(array_key_exists('single', $booking)).'.png') }}" ></td>
                                    <td><img src="{{ asset('assets/frontend/img/status_'.intval(array_key_exists('double', $booking)).'.png') }}" ></td>
                                    <td><img src="{{ asset('assets/frontend/img/status_'.intval(array_key_exists('triple', $booking)).'.png') }}" ></td>
                                    <td>{{ (array_key_exists('childes_first', $booking)) ? $booking['childes_first'] : '0' }}</td>
                                    <td>{{ (array_key_exists('childes_second', $booking)) ? $booking['childes_second'] : '0' }}</td>
                                    <td>{{ (array_key_exists('childes_third', $booking)) ? $booking['childes_third'] : '0' }}</td>
                                    <td class="product-subtotal">{{ $booking['total'] }} {{ trans('common.dzd') }}</td>
                                </tr>
                                </tbody>
                            </table>
                            {{ Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('site.trips.booking.confirm'))]) }}
                            <div class="clearfix"></div>
                            <input type="checkbox" name="terms" value="1" required> {!! trans('trips.accept-terms') !!}
                            <div class="clearfix"></div>
                            <p><small>{{ trans('common.note-payment') }}</small></p>
                            {{ Form::hidden('trip_id', $trip->id) }}
                            <input type="hidden" name="single" value="{{ intval(array_key_exists('single', $booking)) }}">
                            <input type="hidden" name="double" value="{{ intval(array_key_exists('double', $booking)) }}">
                            <input type="hidden" name="triple" value="{{ intval(array_key_exists('triple', $booking)) }}">
                            {{ Form::hidden('childes_first', (array_key_exists('childes_first', $booking)) ? $booking['childes_first'] : '0') }}
                            {{ Form::hidden('childes_second', (array_key_exists('childes_second', $booking)) ? $booking['childes_second'] : '0') }}
                            {{ Form::hidden('childes_third', (array_key_exists('childes_third', $booking)) ? $booking['childes_third'] : '0') }}
                            {{ Form::hidden('total', $booking['total']) }}
                            <button type="submit" class="btn btn-blue btn-full margin_top20">{{ trans('trips.booking') }}</button>
                            {{ Form::close() }}
                        </div>
                    </div>

                </div>  <!-- // row -->
            </div>  <!-- // container -->
        </main>
    </div>

    <div class="sc_modal_section">
        <div class="modal fade" id="terms">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">{{ trans('settings.trips-terms') }}</h4>
                    </div>
                    <div class="modal-body">
                        {!! $config->trips_terms !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop