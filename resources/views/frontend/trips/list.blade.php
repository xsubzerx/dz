@extends('frontend.common.template')

@section('title', trans('trips.trips'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('trips.trips') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('trips.trips') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 contact-form">
                    {{ Form::open(['method' => 'GET']) }}
                    <div class="col-md-3">
                        <label for="country" class="margin5 col-md-4 col-sm-6">{{ trans('countries.title') }}</label>
                        <div class="col-md-8 col-sm-6 no-padding">{{ Form::select('country', $countries, old('country'), ['id' => 'country', 'class' => 'form-control']) }}</div>
                    </div>
                    <div class="col-md-3">
                        <label for="city" class="margin5 col-md-4 col-sm-6">{{ trans('cities.city') }}</label>
                        <div class="col-md-8 col-sm-6 no-padding">{{ Form::select('selectCity', ['' => trans('common.all')], old('city'), ['id' => 'city', 'class' => 'form-control']) }}</div>
                        {{ Form::hidden('city', old('city'), ['id'=>'currCity']) }}
                    </div>
                    <div class="col-md-3">
                        <label for="date" class="margin5 col-md-4 col-sm-6">{{ trans('trips.date') }}</label>
                        <div class="col-md-8 col-sm-6 no-padding">{{ Form::select('date', $dates, old('date'), ['id' => 'date', 'class' => 'form-control']) }}</div>
                    </div>
                    <div class="col-md-2">
                        {{ Form::submit(trans('common.search'), ['class'=>'btn btn-lg btn-success']) }}
                    </div>
                    {{ Form::close() }}
                </div>
                @forelse($trips as $trip)
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <a href="{{ route('site.trips.show', [$trip->id]) }}" >
                        <div class="event-item">
                            <div class="event-thumb">
                                <img src="{{ route('image.large', ['trips', @$trip->randPhoto->photo]) }}" alt="{{ $trip->title }}">
                                <span>{{ $trip->double_price }} <br> {{ trans('common.dzd') }}</span>
                            </div>
                            <div class="event-title"><h4>{{ $trip->title }}</h4></div>
                            <div class="event-desc">
                                <div class="event-meta">
                                    <span><i class="fa fa-thumb-tack"></i> {{ (isset($trip->city->name)) ? $trip->city->name.', '.$trip->country->name : $trip->country->name }}</span>
                                    @if(!$trip->fixed)<span><i class="fa fa-calendar"></i> {{ $trip->start_date_short.' : '.$trip->end_date_short }}</span>@endif
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        </a>
                    </div>
                    @empty
                        <div class="col-sm-12 col-md-12 text-center">
                            <div class="alert alert-info">{{ trans('common.no-result') }}</div>
                        </div>
                    @endforelse
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 margin10 text-center">
                {{ $trips->links() }}
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
@stop

@section('scripts')
    <script>
        $(document).ready(function () {
            $("form").submit(function() {
                $('select[name=selectCity]').prop('disabled', true);
                $(this).find(':input').filter(function() { return !this.value; }).attr('disabled', 'disabled');
                return true;
            });

            var currCountry = $('#country').val();
            var currCity = $('#currCity').val();
            if(currCountry != '')
                getCountryCities(currCountry, currCity);

            $('#city').on('change', function () {
                currCity = $(this).val();
                $('#currCity').val(currCity);
            });

            $('#country').on('change', function () {
                currCountry = $(this).val();
                getCountryCities(currCountry);
            });

            function getCountryCities(co, ct='') {
                if(co != ''){
                    $.ajax({
                        url: '/getCountryCities/'+co+'/'+ct,
                        method: 'GET',
                        success: function (response) {
                            if(response.citiesList != ''){
                                $('#city').html(response.citiesList);
                            }
                        }
                    });
                }else{
                    $('#city').html('<option value="">{{trans('common.all')}}</option>');
                }
            }
        });
    </script>
@append