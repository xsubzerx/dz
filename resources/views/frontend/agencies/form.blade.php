@extends('frontend.common.template')

@section('title', $title)

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ $title }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li><a href="{{ route('agency.trips') }}">{{ trans('users.my-trips') }}</a></li>
                <li class="active">{{ $title }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section class="vtabs type_2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                    @if(!empty($errors->all()))
                        <ul class="callout callout-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                    @endif
                </div>
                <div class="clearfix"></div>
                {{ (isset($trip)) ? Form::model($trip, ['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('agency.trips.edit', [$trip->id])), 'method' => 'PUT']) : Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('agency.trips.create'))]) }}
                <div class="col-xs-12 col-md-4">
                    <!-- Nav tabs -->
                    <ul role="tablist" class="vtabs-nav">
                        @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                        <li>
                            <a data-toggle="tab" role="tab" aria-controls="tab_{{$localeCode}}" href="#tab_{{$localeCode}}" aria-expanded="false">
                                <small>{!! trans('common.'.$localeCode.'-flag') !!}</small> {{ $properties['native'] }}
                            </a>
                        </li>
                        @endforeach
                        <li>
                            <a data-toggle="tab" role="tab" aria-controls="tab_common" href="#tab_common" aria-expanded="true">
                                {{ trans('trips.details') }}
                            </a>
                        </li>
                        @if(isset($trip))
                            <li>
                                <a data-toggle="tab" role="tab" aria-controls="tab_album" href="#tab_album" aria-expanded="true">
                                {{ trans('trips.album') }}
                                </a>
                            </li>
                        @endif
                    </ul>
                    <div class="clearfix"></div><br>
                    <button type="submit" class="btn btn-primary center-block">{{ trans('common.save') }}</button>
                </div>

                <div class="col-xs-12 col-md-8">
                    <div class="tabpanel">
                        <div class="tab-content">
                            @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                            <div role="tabpanel" class="tab-pane" id="tab_{{$localeCode}}">
                                <div class="row">
                                    <div class="form-group">
                                        {{ Form::label('title_'.$localeCode, trans('trips.title').':') }}
                                        {{ Form::text('title_'.$localeCode, old('title_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('trips.title'), 'required')) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('description_'.$localeCode, trans('trips.description').':') }}
                                        {{ Form::textarea('description_'.$localeCode, old('description_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('trips.description'))) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('has_'.$localeCode, trans('trips.has').':') }}
                                        {{ Form::textarea('has_'.$localeCode, old('has_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('trips.has'))) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('has_not_'.$localeCode, trans('trips.has-not').':') }}
                                        {{ Form::textarea('has_not_'.$localeCode, old('has_not_'.$localeCode), array('class' => 'form-control', 'placeholder' => trans('trips.has-not'))) }}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div role="tabpanel" class="tab-pane" id="tab_common">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        {{ Form::label('country_id', trans('countries.title').':') }}
                                        {{ Form::select('country_id', $countriesList, old('country_id'), ['id' => 'country_id', 'class' => 'form-control']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('city_id', trans('cities.city').':') }}
                                        {{ Form::select('city_id', [], old('city_id'), ['id' => 'city_id', 'class' => 'form-control']) }}
                                        {{ Form::hidden('currCity', (isset($trip)) ?  $trip->city_id : old('city_id'), ['id' => 'currCity']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('single_price', trans('trips.single-price').':') }}
                                        {{ Form::text('single_price', old('single_price'), ['class' => 'form-control', 'placeholder' => trans('trips.single-price'), 'required']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('double_price', trans('trips.double-price').':') }}
                                        {{ Form::text('double_price', old('double_price'), ['class' => 'form-control', 'placeholder' => trans('trips.double-price'), 'required']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('triple_price', trans('trips.triple-price').':') }}
                                        {{ Form::text('triple_price', old('triple_price'), ['class' => 'form-control', 'placeholder' => trans('trips.triple-price'), 'required']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('quad_price', trans('trips.quad-price').':') }}
                                        {{ Form::text('quad_price', old('quad_price'), ['class' => 'form-control', 'placeholder' => trans('trips.quad-price'), 'required']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('quinary_price', trans('trips.quinary-price').':') }}
                                        {{ Form::text('quinary_price', old('quinary_price'), ['class' => 'form-control', 'placeholder' => trans('trips.quinary-price'), 'required']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('childes_first_price', trans('trips.childes-first-price').':') }}
                                        {{ Form::text('childes_first_price', old('childes_first_price'), ['class' => 'form-control', 'placeholder' => trans('trips.childes-first-price'), 'required']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('childes_second_price', trans('trips.childes-second-price').':') }}
                                        {{ Form::text('childes_second_price', old('childes_second_price'), ['class' => 'form-control', 'placeholder' => trans('trips.childes-second-price'), 'required']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('childes_third_price', trans('trips.childes-third-price').':') }}
                                        {{ Form::text('childes_third_price', old('childes_third_price'), ['class' => 'form-control', 'placeholder' => trans('trips.childes-third-price'), 'required']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('commission', trans('trips.commission').':') }}
                                        {{ Form::text('commission', old('commission'), ['class' => 'form-control', 'placeholder' => trans('trips.commission')]) }}
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('status', trans('common.status').':') }}
                                        {{ Form::select('status', array('1' => trans('common.active'),'0' => trans('common.deactive')), old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('fixed', trans('trips.fixed').':') }}
                                        {{ Form::select('fixed', array('0' => trans('common.no'),'1' => trans('common.yes')), old('fixed'), ['id' => 'fixed', 'class' => 'form-control']) }}
                                    </div>
                                    <div class="form-group col-md-6 fixedTrip">
                                        {{ Form::label('start', trans('trips.start').':') }}
                                        {{ Form::text('start', old('start'), array('class' => 'form-control datepicker', 'id' => 'start', 'placeholder' => trans('trips.start'))) }}
                                    </div>
                                    <div class="form-group col-md-6 fixedTrip">
                                        {{ Form::label('end', trans('trips.end').':') }}
                                        {{ Form::text('end', old('end'), array('class' => 'form-control datepicker', 'id' => 'end', 'placeholder' => trans('trips.end'))) }}
                                    </div>
                                </div>
                            </div>
                            @if(isset($trip))
                                <div role="tabpanel" class="tab-pane" id="tab_album">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="dropzone" class="dropzone"></div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </section>

    @if($config->agencies_terms)
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content panel panel-info">
                    <div class="modal-header panel-heading">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title">{{ trans('settings.agencies-terms') }}</h4>
                    </div>
                    <div class="modal-body">
                        {!! $config->agencies_terms !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('common.continue') }}</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/backend/css/datepicker/datepicker.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/frontend/css/dropzone.min.css') }}">
@append

@section('scripts')
    @if(isset($trip))
        <script type="text/javascript" src="{{ asset('assets/frontend/js/dropzone.min.js') }}"></script>

        <script type="text/javascript">
            Dropzone.autoDiscover = false;
            new Dropzone("div#dropzone", {
                url: '{{route('agency.trips.album', $trip->id)}}',
                paramName: "image",
                maxFilesize: 2,
                maxFiles: 3,
                headers: {'X-CSRF-Token': '{{csrf_token()}}'},
                addRemoveLinks: true,
                removedfile: function (file) {
                    var name = file.name;
                    $.ajax({
                        url: '/{{LaraLocale::getCurrentLocale()}}/agency/trips/photo/'+name,
                        type: 'DELETE',
                        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    });
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                },
                init: function () {
                    var thisDropzone = this;
                    $.get('{{route('agency.trips.album', [$trip->id])}}', function (data) {
                        if (data == null) {
                            return;
                        }
                        $.each(data, function (key, value) {
                            var mockFile = { name: key, size: value.size };
                            thisDropzone.emit("addedfile", mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, '/image/trips/thumbnail-' + key);
                            thisDropzone.emit("complete", mockFile);

                            var featuredBtn = document.createElement('a');
                            featuredBtn.setAttribute('href',"#");
                            featuredBtn.setAttribute('class', 'dz-featured');
                            if(value.featured > 0){
                                featuredBtn.innerHTML = '<i class="fa fa-star"></i>';
                            }else{
                                featuredBtn.innerHTML = '<i class="fa fa-star-o"></i>';
                            }
                            featuredBtn.setAttribute('data-filename', key);
                            mockFile.previewTemplate.appendChild(featuredBtn);
                        });
                    });
                },
                success: function (file) {
                    var featuredBtn = document.createElement('a');
                    featuredBtn.setAttribute('href',"#");
                    featuredBtn.setAttribute('data-filename', file.fullname);
                    featuredBtn.setAttribute('class', 'dz-featured');
                    featuredBtn.innerHTML = '<i class="fa fa-star-o"></i>';
                    file.previewTemplate.appendChild(featuredBtn);
                }
            });

            $(document).ready(function () {
                $(document).on('click', '.dz-featured', function (e) {
                    e.preventDefault();
                    var thisItm = $(this);
                    var filename = $(this).attr('data-filename');
                    var url = '{{ route('agency.trips.album.featured', [':filename']) }}';
                    $.get(url.replace(':filename', filename), function (data) {
                        if(data.success){
                            $('.dz-featured i').removeClass('fa-star').addClass('fa-star-o');
                            thisItm.children('i').removeClass('fa-star-o').addClass('fa-star');
                        }
                    });
                });
            });
        </script>
    @endif

    <script src="{{ asset('assets/backend/js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
    @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
        <script type="text/javascript">
            $(function() { CKEDITOR.replace('description_{{$localeCode}}', {language: "{{$localeCode}}"}); });
            $(function() { CKEDITOR.replace('has_{{$localeCode}}', {language: "{{$localeCode}}"}); });
            $(function() { CKEDITOR.replace('has_not_{{$localeCode}}', {language: "{{$localeCode}}"}); });
        </script>
    @endforeach
    <script type="text/javascript">
        $(document).ready(function() {
            $(window).on('load',function(){
                $('#myModal').modal('show');
            });

            $('.vtabs-nav li:first-child').addClass('active');
            $('.tab-pane:first-child').addClass('active');

            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

            var start = $('#start').datepicker({
                onRender: function(date) {return date.valueOf() < now.valueOf() ? 'disabled' : '';},
                format: 'yyyy-mm-dd'
            }).on('changeDate', function(ev) {
                if (ev.date.valueOf() > end.date.valueOf()) {
                    var newDate = new Date(ev.date)
                    newDate.setDate(newDate.getDate() + 1);
                    end.setValue(newDate);
                }
                start.hide();
                $('#end')[0].focus();
            }).data('datepicker');

            var end = $('#end').datepicker({
                onRender: function(date) {return date.valueOf() <= start.date.valueOf() ? 'disabled' : '';},
                format: 'yyyy-mm-dd'
            }).on('changeDate', function(ev) {
                end.hide();
            }).data('datepicker');


            $('#fixed').on('change', function(){
                if($(this).val() > 0){
                    $('.fixedTrip').fadeOut();
                }else{
                    $('.fixedTrip').fadeIn();
                }
            });

            var countryID = $('#country_id').val();
            var cityID = $('#currCity').val();
            getCountryCities(countryID, cityID);
            $('#country_id').on('change', function () {
                countryID = $(this).val();
                getCountryCities(countryID, cityID);
            });
            $('#city_id').on('change', function () {
                cityID = $('#city_id').val();
            });

            function getCountryCities(co,ct) {
                $.ajax({
                    url: '/getCountryCitiesById/'+co+'/'+ct,
                    method: 'GET',
                    success: function (response) {
                        if(response.citiesList != ''){
                            $('#city_id').html(response.citiesList);
                        }
                    }
                });
            }
        });
    </script>
@stop