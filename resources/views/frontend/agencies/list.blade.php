@extends('frontend.common.template')

@section('title', trans('trips.view-trips'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('trips.view-trips') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('trips.view-trips') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="cart-form-wrapper">
                            <table class="table example1" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>{{ trans('trips.title') }}</th>
                                    <th>{{ trans('countries.title') }}</th>
                                    <th>{{ trans('cities.city') }}</th>
                                    <th>{{ trans('common.status') }}</th>
                                    <th>{{ trans('common.update') }}</th>
                                    <th>{{ trans('common.delete') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(auth()->guard('user')->user()->agencyTrips as $trip)
                                    <tr>
                                        <td>{{ $trip->title }}</td>
                                        <td>{{ $trip->country->name }}</td>
                                        <td>{{ @$trip->city->name }}</td>
                                        <td><img src="{{ asset('assets/backend/img/status_'.$trip->status.'.png') }}" /></td>
                                        <td><a href="{{ route('agency.trips.edit', [$trip->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                        <td>
                                            {{ Form::open(['url' => route('agency.trips.destroy', [$trip->id]), 'method' => 'DELETE']) }}
                                            <button type="submit" class="btn btn-danger btn-sm" onclick="confirm('{{trans('common.delete-confirm')}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>  <!-- // row -->
            </div>  <!-- // container -->
        </main>
    </div>
@stop

@section('scripts')
    {{ Html::style('assets/backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('assets/backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('assets/backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
        });
    </script>
@stop