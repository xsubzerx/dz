@extends('frontend.common.template')

@section('title', trans('common.thanks'))

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-10 col-lg-offset-1">
                    <div class="sc_quote">
                        {!! $msg !!}
                    </div>
                    @if(isset($country))
                    <div class="col-sm-12 col-md-12">
                        <h3 class="title3">{{ trans('common.recommendation') }}</h3>
                        <div class="divider margin20 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                        <div class="spn_sc_list_area">
                            <ul class="list_style_06">
                                <li><a href="{{ route('site.visa.country', [$country->slug]) }}">{{ trans('visas.country-visas', ['country' => $country->name]) }}</a></li>
                                <li><a href="{{ route('site.hotels.index', ['country' => $country->slug]) }}" target="_blank">{{ trans('hotels.country-hotels', ['country' => $country->name]) }}</a></li>
                                <li><a href="{{ route('site.trips.index', ['country' => $country->slug]) }}" target="_blank">{{ trans('trips.country-trips', ['country' => $country->name]) }}</a></li>
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@stop

@section('styles')
    <style>
        .alert{margin: 0;padding: 50px;text-align: center;font-size: 24px;}
        .sc_quote.style_3 blockquote, .sc_quote.style_4 blockquote, .sc_quote.style_2 blockquote{padding: 0;}
    </style>
@append