@extends('frontend.common.template')

@section('title', trans('common.home'))

@section('content')
    @if(Session::has('msg')) <div class="col-md-8 col-md-offset-2 fixedMsg">{!! Session::get('msg') !!}</div> @endif

    @include('frontend.common.partials._slider')

    @if(count($featuredTrips))
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                        <div class="section-head text-center">
                            <h2>{{ trans('trips.featured-trips') }}</h2>
                        </div>
                    </div>
                    @foreach($featuredTrips as $ftrip)
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <a href="{{ route('site.trips.show', [$ftrip->id]) }}">
                                <div class="event-item">
                                    <div class="event-thumb">
                                        <img src="{{ route('image.medium', ['trips', @$ftrip->randPhoto->photo]) }}" alt="{{ $ftrip->title }}">
                                        <span>{{ $ftrip->double_price }} <br> {{ trans('common.dzd') }}</span>
                                    </div>
                                    <div class="event-title"><h4>{{ $ftrip->title }}</h4></div>
                                    <div class="event-desc">
                                        <div class="event-meta">
                                            <span><i class="fa fa-thumb-tack"></i> {{ (isset($ftrip->city->name)) ? $ftrip->city->name.', '.$ftrip->country->name : $ftrip->country->name }}</span>
                                            @if(!$ftrip->fixed)<span><i class="fa fa-calendar"></i> {{ $ftrip->start_date_short.' : '.$ftrip->end_date_short }}</span>@endif
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @if(count($visaCountries))
    <div class="sc_box_title style3 margin_top20">
        <h2>{{ trans('visas.request-ur-visa-online') }}</h2>
    </div>
    <section class="gray-bg service-section">
        <div class="container">
            <div class="row">
                @foreach($visaCountries as $vCountry)
                <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                    <div class="single-service">
                        <a href="{{ route('site.visa.country', [$vCountry->slug]) }}">
                        <div class="simg">
                            <img src="{{ route('image.medium', ['countries', $vCountry->flag])  }}" alt="{{ $vCountry->name }}" class="img-responsive">
                        </div>
                        <h4>{{ $vCountry->name }}</h4>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="clearfix"></div>
            <div class="divider margin10 hr6"><span></span></div>
            <div class="col-md-12 text-center">
                <a href="{{ route('site.visas.index') }}" class="btn btn-success btn-lg">Show More</a>
            </div>
        </div>
    </section>
    @endif


    <div class="clearfix"></div>

    @if(count($featuredHotels))
    <section>
        <div class="container">
            <div class="row">
                <header class="col-lg-12 section-head text-center item_top">
                    <h2>{{ trans('hotels.featured-hotels') }}</h2>
                </header>
                <div class="travel-grid">
                    @foreach($featuredHotels as $fhotel)
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <article class="travel-item">
                                <figure class="travel-thumb">
                                    <img src="{{ route('image.medium', ['hotels', @$fhotel->randPhoto->photo]) }}" alt="{{ $fhotel->name }}" class="img-responsive">
                                </figure>
                                <div class="details">
                                    <h3>{{ $fhotel->name }}
                                        <span>
                                            @for($i=0;$i<$fhotel->stars;$i++)
                                                <i class="fa fa-star"></i>
                                            @endfor
                                        </span>
                                    </h3>
                                    <span class="address">{{ $fhotel->city->name.', '.$fhotel->city->country->name }}</span>
                                    <div class="price">{{ trans('hotels.price-start') }} <span>{{ $fhotel->minPrice() * $config->currency_rate }} {{ trans('common.dzd') }}</span></div>
                                    <div class="action text-center">
                                        <a href="{{route('site.hotels.show', [$fhotel->id])}}" class="btn btn-ash-border">{{ trans('common.read-more') }}</a>
                                    </div>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @endif

@stop