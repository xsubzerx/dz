@extends('frontend.common.template')

@section('title', trans('common.upload-payment'))

@section('content')

    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('common.upload-payment') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('common.upload-payment') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    @if(!empty($errors->all()))
                        <ul class="alert alert-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                        <div class="clearfix"></div>
                    @endif
                    <div class="cart-form-wrapper">
                        <table class="table table-striped" cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="product-price">{{ trans('common.request-code') }}</th>
                                    <th class="product-name">{{ trans('common.action') }}</th>
                                    @if(array_key_exists('count', $getAction))<th class="product-quantity">{{ trans('common.count') }}</th>@endif
                                    <th class="product-price">{{ trans('common.price') }}</th>
                                    <th class="product-subtotal">{{ trans('common.created-at') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="cart_item">
                                    <td class="product-price">{{ $getAction['code'] }}</td>
                                    <td class="product-name">{{ $getAction['title'] }}</td>
                                    @if(array_key_exists('count', $getAction))<td class="product-quantity">{{ $getAction['count'] }}</td>@endif
                                    <td class="product-price"><span class="amount">{{ $getAction['price'] }}</span></td>
                                    <td class="product-subtotal">
                                        <span class="amount">{{ $getAction['created_at'] }}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="clearfix"></div>
                        <div class="divider margin20 hr6"><span></span></div>

                        {{ Form::open(['url' => route('site.payments.upload.post'), 'files' => true]) }}
                            <div class="row actions">
                                <div class="co-xs-8 col-sm-8 col-md-8">
                                    <div class="coupon-box">
                                        {{ Form::file('image', ['class' => 'form-control', 'required']) }}
                                    </div>
                                </div>
                                <div class="col-xs-4  col-sm-4 col-md-4 text-right">
                                    <input type="submit" value="{{ trans('common.upload') }}" name="update_cart" class="btn btn-primary btn-lg">
                                </div>
                            </div>
                            {{ Form::hidden('action', $getAction['action']) }}
                            {{ Form::hidden('action_id', $getAction['action_id']) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop