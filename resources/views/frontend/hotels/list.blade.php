@extends('frontend.common.template')

@section('title', trans('hotels.hotels'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('hotels.hotels') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('hotels.hotels') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="blog-section">
        <div class="">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="posts-section">
                        <article class="post-entry margin_bottom20">
                            <div class="contact-form padding_top20 padding_bottom20">
                                {{ Form::open(['method' => 'GET']) }}
                                <div class="col-md-5">
                                    <label for="country" class="margin5 col-md-4 col-sm-4 col-xs-4">{{ trans('countries.title') }}</label>
                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">{{ Form::select('country', $countries, old('country'), ['id' => 'country', 'class' => 'form-control']) }}</div>
                                </div>
                                <div class="col-md-5">
                                    <label for="city" class="margin5 col-md-4 col-sm-4 col-xs-4">{{ trans('cities.city') }}</label>
                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">{{ Form::select('selectCity', ['' => trans('common.all')], old('city'), ['id' => 'city', 'class' => 'form-control']) }}</div>
                                    {{ Form::hidden('city', old('city'), ['id'=>'currCity']) }}
                                </div>
                                <div class="col-md-2 col-xs-12">
                                    {{ Form::submit(trans('common.search'), ['class'=>'btn btn-lg btn-success col-xs-12']) }}
                                </div>
                                {{ Form::close() }}
                                <div class="clearfix"></div>
                            </div>
                        </article>

                        <article class="post-entry">
                            <div class="padding_top20 padding_bottom20">
                                @forelse($hotels as $hotel)
                                    <div class="col-sm-6 col-md-4 text-center">
                                        <div class="promotion-item">
                                            <figure class="promo-thumb">
                                                <img src="{{ route('image.large', ['hotels', @$hotel->randPhoto->photo]) }}" alt="">
                                                <figcaption class="promo-price">{{ $hotel->minPrice() * $config->currency_rate }} {{ trans('common.dzd') }}</figcaption>
                                            </figure>
                                            <div class="promo-city">
                                                <h4>{{ $hotel->name }}</h4>
                                            </div>
                                            <div class="promo-meta clearfix">
                                                <span class="meta-inner"><i class="fa fa-location-arrow"></i> {{ $hotel->city->country->name }}</span>
                                                <span class="meta-inner"><i class="fa fa-info"></i> {{ $hotel->city->name }}</span>
                                            </div>
                                            <div class="promo-entry">
                                                <a href="{{route('site.hotels.show', [$hotel->id])}}" class="btn center-block">{{ trans('common.read-more') }}</a>
                                            </div>
                                        </div>
                                    </div> <!-- end .promotion-item -->
                                @empty
                                    <div class="col-sm-12 col-md-12 text-center">
                                        <div class="alert alert-info">{{ trans('common.no-result') }}</div>
                                    </div>
                                @endforelse
                                <div class="clearfix"></div>
                            </div>
                        </article>

                        <nav class="text-center">
                            {{ $hotels->links() }}
                            <div class="clearfix"></div>
                        </nav>
                    </div>
                </div> <!-- end .col-xs-12 col-md-8 -->
                <div class="col-xs-12 col-md-4">
                    <div class="right-sidebar">
                        <aside class="sidebar">
                            <h5 class="text-center">لم تجد فندق مناسب؟ يمكنك طلب فندق / وجهة معينة سوف يقوم فريق العمل بدراسة طلبك و تقديم افضل عروض الفنادق في كل وكالات الجزائر و المواقع العالمية و سيتم الرد عليك خلال يوم عمل كحد أقصي بالتلفون</h5>
                            <hr>
                            {{ Form::open(['url' => route('site.hotels.custom'), 'id' => 'contact-form', 'class' => 'row contact-form']) }}
                            <div class="form-group">
                                <label for="destination" class="col-md-3">{{ trans('hotels.destination') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name="destination" placeholder="{{ trans('hotels.destination') }}" required class="form-control" id="destination">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="hotel" class="col-md-3">{{ trans('hotels.hotel') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name="hotel" placeholder="{{ trans('hotels.hotel') }}" class="form-control" id="hotel">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="checkin">{{ trans('hotels.checkin') }}</label>
                                <input type="text" name="checkin" placeholder="{{ trans('hotels.checkin') }}" required class="form-control datepicker" id="checkin">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="checkout">{{ trans('hotels.checkout') }}</label>
                                <input type="text" name="checkout" placeholder="{{ trans('hotels.checkout') }}" required class="form-control datepicker2" id="checkout">
                            </div>
                            <div class="form-group">
                                <label for="budget" class="col-md-3">{{ trans('hotels.budget') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name="budget" placeholder="{{ trans('hotels.budget') }}" class="form-control" id="budget">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="rooms_count">{{ trans('hotels.rooms-count')}}</label>
                                {{ Form::select('rooms_count', [1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 1, ['id' => 'rooms_count', 'class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-4">
                                <label for="adults_count">{{ trans('hotels.adults-count')}}</label>
                                {{ Form::select('adults_count', [1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 1, ['id' => 'adults_count', 'class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-4">
                                <label for="childes_count">{{ trans('hotels.childes-count')}}</label>
                                {{ Form::select('childes_count', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['id' => 'childes_count', 'class' => 'form-control']) }}
                            </div>
                            <div class="form-group" id="childes"></div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <textarea name="note" class="form-control" placeholder="{{ trans('hotels.note') }}" id="note" rows="3"></textarea>
                            </div>
                            <div class="col-md-12">
                                @if(auth()->guard('user')->check())
                                    <input type="submit" value="{{ trans('contact.send') }}" class="message-sub pull-right btn btn-blue">
                                @else
                                    <a href="{{ route('login') }}" class="btn btn-danger pull-right">{{ trans('auth.login') }}</a>
                                @endif
                            </div>
                            {{ Form::close() }}
                        </aside>
                    </div>
                </div> <!-- end .col-xs-12 col-md-4 -->
            </div>
        </div>
    </section>
@stop

@section('styles')
    {!! Minify::stylesheet('/assets/frontend/css/jquery-ui.min.css') !!}
@append

@section('scripts')
    {!! Minify::javascript('/assets/frontend/js/jquery-ui.min.js') !!}
    <script>
        $(document).ready(function () {
            $("form").submit(function() {
                $('select[name=selectCity]').prop('disabled', true);
                $(this).find(':input').filter(function() { return !this.value; }).attr('disabled', 'disabled');
                return true;
            });

            var currCountry = $('#country').val();
            var currCity = $('#currCity').val();
            if(currCountry != '')
                getCountryCities(currCountry, currCity);

            $('#city').on('change', function () {
                currCity = $(this).val();
                $('#currCity').val(currCity);
            });

            $('#country').on('change', function () {
                currCountry = $(this).val();
                getCountryCities(currCountry);
            });

            function getCountryCities(co, ct='') {
                if(co != ''){
                    $.ajax({
                        url: '/getCountryCities/'+co+'/'+ct,
                        method: 'GET',
                        success: function (response) {
                            if(response.citiesList != ''){
                                $('#city').html(response.citiesList);
                            }
                        }
                    });
                }else{
                    $('#city').html('<option value="">{{trans('common.all')}}</option>');
                }
            }


            $(".datepicker").datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd',
                onSelect: function (selected) {
                    $('#checkout').datepicker('option', 'minDate', selected)
                }
            });

            $(".datepicker2").datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd',
                onSelect: function (selected) {
                    $('#checkin').datepicker('option', 'maxDate', selected)
                }
            });

            $('#childes_count').on('change', function () {
                var count = $(this).val();
                $('#childes').html('<h5>{{trans('hotels.childes-ages')}}:</h5><div class="clearfix"></div>');
                for(var i=0;i<count;i++){
                    $('#childes').append('<div class="col-md-3">{{ Form::select('childes_ages[]', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12], 0, ['id' => 'childes_ages', 'class' => 'form-control']) }}</div>');
                }
            });
        });
    </script>
@append