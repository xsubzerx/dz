@extends('frontend.common.template')

@section('title', $hotel->name)

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ $hotel->name }} <small class="starts">@for($i=0;$i<$hotel->stars;$i++)<i class="icon icon-star5"></i>@endfor</small></h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li><a href="{{ route('site.hotels.index') }}">{{ trans('hotels.hotels') }}</a></li>
                <li class="active">{{ $hotel->name }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    @if(count($hotel->album))
    <section class="slider-demo">
        <div class="container">
            <div class="demoSliderWrapper">
                <div id="ei-slider2" class="ei-slider">
                    <ul class="ei-slider-large">
                        @foreach($hotel->album as $k=>$photo)
                            <li><img src="{{ route('image', ['hotels', $photo->photo]) }}" alt="image{{$k}}"></li>
                        @endforeach
                    </ul><!-- ei-slider-large -->
                    <ul class="ei-slider-thumbs">
                        <li class="ei-slider-element">Current</li>
                        @foreach($hotel->album as $k=>$photo)
                            <li><a href="#">Slide {{$k}}</a><img src="{{ route('image.thumbnail', ['hotels', $photo->photo]) }}" alt="thumb{{$k}}"></li>
                        @endforeach
                    </ul><!-- ei-slider-thumbs -->
                </div><!-- ei-slider -->
            </div>
        </div>
    </section>
    @endif

    <section>
        <div class="container">
            <div class="row">
                @if($hotel->description)
                    <div class="col-md-12">
                        <h1 class="title3">{{ trans('hotels.description') }}</h1>
                        <div class="divider margin_bottom20 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                        {!! $hotel->description !!}
                    </div>
                @endif

                @if(count($amenities))
                    <div class="col-md-12">
                        <h1 class="title3 margin_top20">{{ trans('hotels.amenities') }}</h1>
                        <div class="divider margin_bottom20 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                        <div class="spn_sc_list_area">
                            <ul class="list_style_02">
                                @foreach($amenities as $amenity)
                                    <li class="col-md-4 col-sm-6"><span class="{{ ($amenity->icon) ? substr($amenity->icon, 0, strpos($amenity->icon, "-")).' '.$amenity->icon : 'fa fa-check' }}"></span>{{ $amenity->title }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                <div class="clearfix"></div>

                <div class="col-md-8 col-sm-12">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                    {{ Form::open() }}
                    <div id="bdp-forums" class="forum-head">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="col-md-6 no-padding">{{ trans('hotels.title') }}</th>
                                <th class="col-md-2 no-padding">{{ trans('hotels.capacity') }}</th>
                                <th class="col-md-2 no-padding">{{ trans('hotels.price') }}</th>
                                <th class="col-md-1 no-padding">{{ trans('hotels.no-rooms') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($hotel->rooms as $room)
                                <tr>
                                    <td>
                                        @if($room->image)<a href="{{ route('image', ['hotels', $room->image]) }}" class="image-link">@endif <img src="{{ route('image.medium', ['hotels', $room->image]) }}" alt="{{ $room->title }}" class="pull-left img-thumbnail">@if($room->image)</a>@endif
                                        <h5>{{ $room->title }}</h5>
                                        <small>{{ strip_tags($room->description) }}</small>
                                    </td>
                                    <td>
                                        {{ $room->adults_count }} <i class="fa fa-users"></i>
                                        @if($room->childes_count > 0)
                                            + {{ $room->childes_count }} <i class="fa fa-child"></i>
                                        @endif
                                    </td>
                                    <td>{{ $room->price * $config->currency_rate }} {{ trans('common.dzd') }}</td>
                                    <td>
                                        {{ Form::select('count_'.$room->id, [''=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['class' => 'form-control']) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <label>{{ trans('hotels.checkin') }}</label>
                        {{ Form::text('checkin', null, ['class' => 'form-control datepicker', 'id' => 'checkin', 'placeholder' => trans('hotels.checkin'), 'required']) }}
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <label>{{ trans('hotels.checkout') }}</label>
                        {{ Form::text('checkout', null, ['class' => 'form-control datepicker2', 'id' => 'checkout', 'placeholder' => trans('hotels.checkout'), 'required']) }}
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <label>{{ trans('hotels.childes-count') }}</label>
                        <select name="childes_count" class="form-control">
                            @for($i=0;$i<=20;$i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div id="childes_ages" class="col-md-12"></div>
                    <div class="clearfix"></div>
                    <div class="pull-right margin10">
                    @if(auth()->guard('user')->check())
                        <button type="submit" class="btn btn-lg btn-success">{{ trans('hotels.booking') }}</button>
                    @else
                        <a href="{{ route('login') }}" class="btn btn-lg btn-danger">{{ trans('auth.login') }}</a>
                    @endif
                    </div>

                    {{ Form::close() }}
                </div>

                <div class="col-md-4 col-sm-12 shortcode-widget">
                    <div class="spn_sc_list_area">
                        <ul class="list_style_07">
                            <li><i class="icon icon-phone3"></i> {{ $config->site_phone }}</li>
                            <li><i class="icon icon-location"></i> {{ $hotel->address }} - {{ $hotel->city->name }} - {{ $hotel->city->country->name }}</li>
                        </ul>
                    </div>
                    @if($hotel->nearby)
                    <h3 class="title3">{{ trans('hotels.nearby') }}</h3>
                    <div class="divider margin10 hr4 hr_{{LaraLocale::getCurrentLocale()}}"><span></span></div>
                    <p>{!! $hotel->nearby !!}</p>
                    @endif
                    <div class="shortcodemap">
                        <div id="map_canvas" class="map_canvas"><div style="height: 100%; width: 100%;"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('styles')
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/frontend/shortcodes/css/iconmoon.css') }}" />
    {!! Minify::stylesheet([
        '/assets/frontend/shortcodes/css/eislideshow.css',
        '/assets/frontend/css/magnific-popup.css',
        '/assets/frontend/css/jquery-ui.min.css'
    ]) !!}
@append

@section('scripts')
    {!! Minify::javascript([
        '/assets/frontend/js/jquery-ui.min.js',
        '/assets/frontend/shortcodes/js/jquery.eislideshow.js',
        '/assets/frontend/js/jquery.magnific-popup.min.js'
    ]) !!}
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>

    <script type="application/javascript">
        $(document).ready(function () {
            $(".datepicker").datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd',
                onSelect: function (selected) {
                    $('#checkout').datepicker('option', 'minDate', selected)
                }
            });

            $(".datepicker2").datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd',
                onSelect: function (selected) {
                    $('#checkin').datepicker('option', 'maxDate', selected)
                }
            });

            $("#ei-slider2").eislideshow({
                animation           : "sides",
                autoplay            : true,
                slideshow_interval  : 5000,
                titlesFactor        : 0
            });

            $('.image-link').magnificPopup({type:'image'});

            $('select[name=childes_count]').on('change', function(){
                var count = $(this).val();
                var childesAges = '<h5 class="margin10">{{trans('hotels.childes-ages')}}</h5>';
                for(var i=0;i<count;i++){
                    childesAges += '<div class="col-md-2 col-sm-4 margin5"><select name="childes_ages[]" class="form-control">';
                    for(var x=0;x<=12;x++) {
                        childesAges += '<option value="'+x+'">'+x+'</option>';
                    }
                    childesAges += '</select></div>';
                }
                $('#childes_ages').next().html(childesAges);
            });
        });
    </script>
@append