@extends('frontend.common.template')

@section('title', trans('hotels.booking'))

@section('styles')
    {!! Minify::stylesheet('/assets/frontend/css/jquery-ui.min.css') !!}
@append

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('hotels.booking') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li><a href="{{ route('site.hotels.show', [$hotel->id]) }}">{{ $hotel->name }}</a></li>
                <li class="active">{{ trans('hotels.booking') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <div class="row">
                    {{ Form::open(['url' => LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), route('site.hotels.booking.confirm'))]) }}
                    <div class="col-xs-12 col-md-8">
                        <div class="cart-form-wrapper">
                            <table class="shop_table cart" cellspacing="0">
                                <thead>
                                <tr>
                                    <th class="product-thumbnail">{{ trans('hotels.title') }}</th>
                                    <th colspan="2" class="product-name">{{ trans('hotels.title') }}</th>
                                    <th class="product-price">{{ trans('hotels.price') }}</th>
                                    <th class="product-quantity">{{ trans('hotels.quantity') }}</th>
                                    <th class="product-subtotal">{{ trans('hotels.total') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(array_key_exists('rooms', $booking) && count($booking['rooms']))
                                    @foreach($hotel->rooms as $room)
                                        @if(!in_array($room->id, array_keys($booking['rooms']))) @continue @endif
                                        <tr class="cart_item">
                                            <td class="product-thumbnail">
                                                <a href="#">
                                                    <img src="{{ route('image.thumbnail', ['hotels',$room->image]) }}" alt="">
                                                </a>
                                            </td>
                                            <td class="product-name">
                                                <a href="#">{{ $room->title }}</a>
                                            </td>
                                            <td class="product-price">
                                                <span class="amount">{{ $room->price * $config->currency_rate }} {{ trans('common.dzd') }}</span>
                                            </td>
                                            <td class="product-quantity">
                                                <div class="quantity">{{ (array_key_exists('rooms', $booking)) ? $booking['rooms'][$room->id] : '' }}</div>
                                            </td>
                                            <td class="product-subtotal">
                                                <span class="amount">{{ (array_key_exists('rooms', $booking)) ? $room->price * $config->currency_rate * $booking['rooms'][$room->id] : $room->price * $config->currency_rate }} {{ trans('common.dzd') }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="cart-collaterals">
                                                <h4 class="right">{{ trans('hotels.room-members') }}</h4>
                                                @for($i=0;$i<$booking['rooms'][$room->id];$i++)
                                                    <div class="form-group col-md-4">
                                                        <label>{{ trans('users.birthdate') }}</label>
                                                        <input type="text" name="members[{{$room->id}}][birthdate][]" class="form-control datepicker" required>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>{{ trans('users.fullname') }}</label>
                                                        <input type="text" name="members[{{$room->id}}][name][]" class="form-control" required>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>{{ trans('users.nickname') }}</label>
                                                        <input type="text" name="members[{{$room->id}}][nickname][]" class="form-control" required>
                                                    </div>
                                                @endfor
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4">
                        <div class="cart-collaterals">
                            <div class="cart_totals">
                                <h2>{{ trans('hotels.booking-info') }}</h2>
                                <table cellspacing="0">
                                    <tbody>
                                    @if(count($booking['childes']))
                                    <tr class="tax-rate tax-rate-tax-1">
                                        <th colspan="3">{{ trans('hotels.childes-ages') }}</th>
                                        <td>
                                            @foreach($booking['childes'] as $child)
                                                <span class="amount">{{ $child.' '.trans('hotels.years-old') }}</span><br>
                                            @endforeach
                                        </td>
                                    </tr>
                                    @endif
                                    <tr class="tax-rate tax-rate-tax-1">
                                        <th colspan="3">{{ trans('hotels.checkin') }}</th>
                                        <td><span class="amount">{{ $booking['checkin'] }}</span></td>
                                    </tr>
                                    <tr class="tax-rate tax-rate-tax-1">
                                        <th colspan="3">{{ trans('hotels.checkout') }}</th>
                                        <td><span class="amount">{{ $booking['checkout'] }}</span></td>
                                    </tr>
                                    <tr class="cart-subtotal">
                                        <th colspan="3">{{ trans('hotels.subtotal') }}</th>
                                        <td><span class="amount">{{ $booking['subtotal'] * $config->currency_rate }} {{ trans('common.dzd') }}</span></td>
                                    </tr>
                                    <tr class="tax-rate tax-rate-tax-1">
                                        <th colspan="3">{{ trans('hotels.days') }}</th>
                                        <td><span class="amount">{{ $booking['days'].' '.trans('hotels.days') }}</span></td>
                                    </tr>
                                    <tr class="order-total">
                                        <th colspan="3">{{ trans('hotels.total') }}</th>
                                        <td><strong><span class="amount">{{ $booking['subtotal'] * $config->currency_rate * $booking['days'] }} {{ trans('common.dzd') }}</span></strong> </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>
                                <input type="checkbox" name="terms" value="1" required> {!! trans('hotels.accept-terms') !!}
                                <div class="clearfix"></div>
                                <p><small>{{ trans('common.note-payment') }}</small></p>
                                    {{ Form::hidden('hotel_id', $hotel->id) }}
                                    {{ Form::hidden('checkin', $booking['checkin']) }}
                                    {{ Form::hidden('checkout', $booking['checkout']) }}
                                    {{ Form::hidden('rooms', (array_key_exists('rooms', $booking) && count($booking['rooms']) ? json_encode($booking['rooms']) : 0)) }}
                                    {{ Form::hidden('childes', json_encode($booking['childes'])) }}
                                    {{ Form::hidden('total', $booking['subtotal'] * $config->currency_rate * $booking['days']) }}
                                    <button type="submit" class="btn btn-blue btn-full margin_top20">{{ trans('hotels.booking') }}</button>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>  <!-- // row -->
            </div>  <!-- // container -->
        </main>
    </div>

    <div class="sc_modal_section">
        <div class="modal fade" id="terms">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">{{ trans('settings.hotels-terms') }}</h4>
                    </div>
                    <div class="modal-body">
                        {!! $config->hotels_terms !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    {!! Minify::javascript('/assets/frontend/js/jquery-ui.min.js') !!}
    <script>
        $(document).ready(function(){
            $(".datepicker").datepicker({
                maxDate: 0,
                dateFormat: 'yy-mm-dd',
            });
        });
    </script>
@append