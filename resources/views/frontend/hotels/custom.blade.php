@extends('frontend.common.template')

@section('title', trans('hotels.hotel-custom-requests'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('hotels.hotel-custom-requests') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('hotels.hotel-custom-requests') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                @if(!empty($errors->all()))
                    <ul class="callout callout-danger">
                        @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                    </ul>
                @endif
                <div class="col-md-12 item_left">
                    {{ Form::open(['id' => 'contact-form', 'class' => 'row contact-form']) }}
                    <div class="col-md-6">
                        <label for="destination">{{ trans('hotels.destination') }}</label>
                        <input type="text" name="destination" placeholder="{{ trans('hotels.destination') }}" required class="form-control" id="destination">
                    </div>
                    <div class="col-md-6">
                        <label for="hotel">{{ trans('hotels.hotel') }}</label>
                        <input type="text" name="hotel" placeholder="{{ trans('hotels.hotel') }}" class="form-control" id="hotel">
                    </div>
                    <div class="col-md-6">
                        <label for="checkin">{{ trans('hotels.checkin') }}</label>
                        <input type="text" name="checkin" placeholder="{{ trans('hotels.checkin') }}" required class="form-control datepicker" id="checkin">
                    </div>
                    <div class="col-md-6">
                        <label for="checkout">{{ trans('hotels.checkout') }}</label>
                        <input type="text" name="checkout" placeholder="{{ trans('hotels.checkout') }}" required class="form-control datepicker2" id="checkout">
                    </div>
                    <div class="col-md-6">
                        <label for="budget">{{ trans('hotels.budget') }}</label>
                        <input type="text" name="budget" placeholder="{{ trans('hotels.budget') }}" class="form-control" id="budget">
                    </div>
                    <div class="col-md-6">
                        <label for="rooms_count">{{ trans('hotels.rooms-count')}}</label>
                        {{ Form::select('rooms_count', [1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 1, ['id' => 'rooms_count', 'class' => 'form-control']) }}
                    </div>
                    <div class="col-md-6">
                        <label for="adults_count">{{ trans('hotels.adults-count')}}</label>
                        {{ Form::select('adults_count', [1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 1, ['id' => 'adults_count', 'class' => 'form-control']) }}
                    </div>
                    <div class="col-md-6">
                        <label for="childes_count">{{ trans('hotels.childes-count')}}</label>
                        {{ Form::select('childes_count', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['id' => 'childes_count', 'class' => 'form-control']) }}
                    </div>
                    <div class="col-md-12 no-padding" id="childes"></div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <label for="note">{{ trans('hotels.note') }}</label>
                        <textarea name="note" class="form-control" placeholder="{{ trans('hotels.note') }}" id="note"></textarea>
                    </div>
                    <div class="col-md-12">
                        @if(auth()->guard('user')->check())
                            <input type="submit" value="{{ trans('contact.send') }}" class="message-sub pull-right btn btn-blue">
                        @else
                            <a href="{{ route('login') }}" class="btn btn-danger pull-right">{{ trans('auth.login') }}</a>
                        @endif
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@stop

@section('styles')
    {!! Minify::stylesheet('/assets/frontend/css/jquery-ui.min.css') !!}
@append

@section('scripts')
    {!! Minify::javascript('/assets/frontend/js/jquery-ui.min.js') !!}
    <script>
        $(document).ready(function () {
            $(".datepicker").datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd',
                onSelect: function (selected) {
                    $('#checkout').datepicker('option', 'minDate', selected)
                }
            });

            $(".datepicker2").datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd',
                onSelect: function (selected) {
                    $('#checkin').datepicker('option', 'maxDate', selected)
                }
            });

            $('#childes_count').on('change', function () {
                var count = $(this).val();
                $('#childes').html('<h5 class="title3">{{trans('hotels.childes-ages')}}</h5><div class="clearfix"></div>');
                for(var i=0;i<count;i++){
                    $('#childes').append('<div class="col-md-2">{{ Form::select('childes_ages[]', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12], 0, ['id' => 'childes_ages', 'class' => 'form-control']) }}</div>');
                }
            });
        });
    </script>
@append