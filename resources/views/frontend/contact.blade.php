@extends('frontend.common.template')

@section('title', trans('contact.contact-us'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('contact.contact-us') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('contact.contact-us') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-8 item_left">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                    @if(!empty($errors->all()))
                        <ul class="callout callout-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                    @endif
                    {{ Form::open(['id' => 'contact-form', 'class' => 'row contact-form']) }}
                    <div class="col-md-6">
                        <input type="text" name="sender" placeholder="{{ trans('contact.sender') }}" required class="form-control">
                        @if ($errors->has('sender'))
                            <span class="help-block">({{ $errors->first('sender') }})</span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <input type="email" name="email" placeholder="{{ trans('users.email') }}" required class="form-control">
                        @if ($errors->has('email'))
                            <span class="help-block">({{ $errors->first('email') }})</span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="phone" placeholder="{{ trans('users.phone') }}" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="subject" placeholder="{{ trans('contact.subject') }}" required class="form-control">
                    </div>
                    <div class="col-md-12">
                        <textarea name="message" class="form-control" required placeholder="{{ trans('contact.message') }}"></textarea>
                        <input type="submit" value="{{ trans('contact.send') }}" class="message-sub pull-right btn btn-blue">
                    </div>
                    {{ Form::close() }}
                </div>

                <div class="col-md-4 item_right">
                    <div class="address">
                        <h5>{{ trans('contact.contact-info') }}</h5>
                        <p><i class="fa fa-map-marker"></i>{{ $config->site_address }}</p>
                        <p><i class="fa fa-phone"></i>{{ $config->site_phone }}</p>
                        <p><i class="fa fa-envelope"></i><a href="mailto:{{$config->site_email}}">{{$config->site_email}}</a></p>
                    </div>
                    {!! $config->contact_page !!}
                </div>
            </div>

            <div class="row contact-page">
                <div class="col-md-12 item_left">
                    <div id="map-canvas"></div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>
@append