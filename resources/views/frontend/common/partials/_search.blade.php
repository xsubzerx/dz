<div class="search-tabs">
    <div class="container">
        <div class="search-tab-content">
            <!-- Nav tabs -->
            <ul class="search-tab-nav">
                <li class="toggle-form">
                    <a href="javascript:;"><h2><i class="fa fa-angle-up"></i>{{ trans('hotels.search-for-hotel') }}</h2></a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="hotels">
                    <div class="tab-search-form">
                        <form action="{{route('site.hotels.index')}}" class="ts-form1">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <div class="input-field">
                                        <label for="Country">{{ trans('countries.country') }}</label>
                                        <select name="country" id="Country" class="form-control">
                                            <option value="0" selected>{{ trans('countries.select') }}</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->slug}}">{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="input-field">
                                        <label for="City">{{ trans('cities.city') }}</label>
                                        <select name="city" id="City" class="form-control">
                                            <option value="0" selected>{{ trans('cities.select') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="input-field">
                                        <i class="fa fa-calendar input-icon"></i>
                                        <label for="checkin">{{ trans('hotels.checkin') }}</label>
                                        <input type="text" class="form-control datepicker" name="checkin" id="checkin" placeholder="{{ trans('hotels.checkin') }}">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="input-field">
                                        <i class="fa fa-calendar input-icon"></i>
                                        <label for="checkout">{{ trans('hotels.checkout') }}</label>
                                        <input type="text" class="form-control datepicker2" name="checkout" id="checkout" placeholder="{{ trans('hotels.checkout') }}">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="input-field">
                                        <i class="fa fa-home input-icon"></i>
                                        <label for="adults">{{ trans('hotels.adults') }}</label>
                                        <input type="number" class="form-control" name="adults" id="adults" placeholder="{{ trans('hotels.adults') }}">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="input-field">
                                        <i class="fa fa-user input-icon"></i>
                                        <label for="Childes">{{ trans('hotels.childes') }}</label>
                                        <input type="number" class="form-control" name="childes" id="Childes" placeholder="{{ trans('hotels.childes') }}">
                                    </div>
                                </div>
                            </div>
                            <input type="submit" value="{{ trans('common.search') }}" class="btn btn-blue">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('styles')
    {!! Minify::stylesheet('/assets/frontend/css/jquery-ui.min.css') !!}
@append

@section('scripts')
    {!! Minify::javascript('/assets/frontend/js/jquery-ui.min.js') !!}
    <script>
        $(document).ready(function () {
            $(".datepicker").datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd',
                onSelect: function (selected) {
                    $('#checkout').datepicker('option', 'minDate', selected)
                }
            });

            $(".datepicker2").datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd',
                onSelect: function (selected) {
                    $('#checkin').datepicker('option', 'maxDate', selected)
                }
            });

            $("form.ts-form1").submit(function() {
                $(this).find(':input').filter(function() { return !this.value; }).attr('disabled', 'disabled');
                return true;
            });

            $('#Country').on('change', function () {
                getCountryCities($(this).val());
            });

            function getCountryCities(co) {
                $.ajax({
                    url: '/getCountryCities/'+co,
                    method: 'GET',
                    success: function (response) {
                        if(response.citiesList != ''){
                            $('#City').html(response.citiesList);
                        }
                    }
                });
            }
        });
    </script>
@append