<div class="top-bar">
    <div class="top-bar-inner">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <nav class="top-nav">
                        <ul>
                            <li><a href="mailto:{{$config->site_email}}') }}"><i class="fa fa-envelope-o"></i> {{$config->site_email}}</a></li>
                            <li><a href="#"><i class="fa fa-phone"></i> {{ $config->site_phone }}</a></li>
                        </ul>
                    </nav>
                </div>

                <div class="col-sm-6 col-md-6">
                    <nav class="top-nav">
                        <ul class="text-right">
                            <li>
                                <div class="language-wrap">
                                    <i class="fa fa-globe"></i>
                                    <select name="language" id="setLocale" class="rsvp">
                                        @foreach(LaraLocale::getSupportedLocales() as $localeCode => $properties)
                                            <option value="{{LaraLocale::getLocalizedURL($localeCode, LaraLocale::getNonLocalizedURL())}}" @if(LaraLocale::getCurrentLocale() == $localeCode) selected="true" @endif>{{$properties['native']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </li>
                            @if(auth()->guard('user')->check())
                                <li class="dropdown">
                                    <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">
                                        {{ trans('users.welcome'). ' ' . auth()->guard('user')->user()->fullname }} <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('site.profile') }}">{{ trans('users.profile') }}</a></li>
                                        <li><a href="{{ route('site.my-visas') }}">{{ trans('users.my-visas') }}</a></li>
                                        <li><a href="{{ route('site.my-hotels') }}">{{ trans('users.my-hotels') }}</a></li>
                                        <li><a href="{{ route('site.my-hotels.custom') }}">{{ trans('users.my-hotels-custom') }}</a></li>
                                        <li><a href="{{ route('site.my-trips') }}">{{ trans('users.my-trips') }}</a></li>
                                        <li><a href="{{ route('site.my-flights') }}">{{ trans('users.my-flights') }}</a></li>
                                        @if(auth()->guard('user')->user()->type == 'agency')
                                            <li><hr></li>
                                            <li><a href="{{ route('agency.trips.create') }}">{{ trans('trips.create-trip') }}</a></li>
                                            <li><a href="{{ route('agency.trips') }}">{{ trans('trips.view-trips') }}</a></li>
                                            <li><a href="{{ route('site.agency-trips-requests') }}">{{ trans('trips.my-trips-requests') }}</a></li>
                                            <li><hr></li>
                                        @endif
                                        <li><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ trans('users.logout') }}</a></li>
                                    </ul>
                                    {{ Form::open(['url'=>route('logout'), 'id'=>'logout-form', 'class'=>'hidden']) }}{{ Form::close() }}
                                </li>
                            @else
                                <li style="position: relative;">
                                    <a href="{{route('register.agency')}}"><i class="fa fa-key"></i> {{ trans('auth.agency-register') }}</a>
                                    @if (LaraLocale::getLocalizedURL() == route('site.home'))
                                    <div class="arrow_box">
                                        <h2>{{ trans('auth.agency-register-box') }}</h2>
                                    </div>
                                    @endif
                                </li>
                                <li><a href="{{route('register')}}"><i class="fa fa-key"></i> {{ trans('auth.register') }}</a></li>
                                <li><a href="{{route('login')}}"><i class="fa fa-lock"></i> {{ trans('auth.login') }}</a></li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script type="application/javascript">
        $(document).ready(function() {
            $('#setLocale').on('change', function (e) {
                e.preventDefault();

                window.location.href = $(this).val();
            });
        });
    </script>
@append