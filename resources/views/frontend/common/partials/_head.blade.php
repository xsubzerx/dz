<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <!-- Meta Description -->
    <meta name="description" content="{{ $config->site_metadesc }}">
    <!-- Meta Keyword -->
    <meta name="keywords" content="{{ $config->site_metakey }}">
    <!-- meta character set -->
    <meta charset="utf-8">

    <!-- Site Title -->
    <title>{{ $config->site_name }} :: @yield('title')</title>

    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/favicon.png') }}">

    <!-- CSS ============================================= -->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/frontend/css/font-awesome.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" />
    {!! Minify::stylesheet([
//        '/assets/frontend/css/bootstrap.min.css',
        '/assets/frontend/css/jquery.bxslider.css',
        '/assets/frontend/css/animate.css',
        '/assets/frontend/shortcodes/css/main.css',
        '/assets/frontend/shortcodes/css/media-queries.css',
        '/assets/frontend/css/main.css',
        '/assets/frontend/css/media-queries.css',
    ]) !!}

    @if(LaraLocale::getCurrentLocale() == 'ar')
        {!! Minify::stylesheet([
            '/assets/backend/css/bootstrap-rtl.min.css',
            '/assets/frontend/css/main-rtl.css'
        ]) !!}
    @endif
    <link id="themeColorChangeLink" type="text/css" rel="stylesheet" href="{{ asset('assets/frontend/css/colors/c10.css') }}">

    @yield('styles')

    <!--[if lt IE 9]>
    {!! Minify::javascript([
        '//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js',
        '//oss.maxcdn.com/respond/1.4.2/respond.min.js'
    ]) !!}
    <![endif]-->
</head>
