@if(auth()->guard('user')->check() && auth()->guard('user')->user()->type == 'agency')
<div id="welcome-bar" style="background-color: {{ auth()->guard('user')->user()->background_color ?? '#339933' }}">
	<div class="container">
		<div class="col-md-6">
			<h3 style="color: {{ auth()->guard('user')->user()->font_color ?? '#ffffff' }}">{{ trans('users.welcome'). ' ' . auth()->guard('user')->user()->fullname }}</h3>
		</div>
		<div class="col-md-6 text-right">
			@if(auth()->guard('user')->user()->photo)
				<img src="{{ route('image', ['users', auth()->guard('user')->user()->photo]) }}" alt="{{ auth()->guard('user')->user()->fullname }}">
			@endif
		</div>
	</div>
</div>
<div class="clearfix"></div>
@endif