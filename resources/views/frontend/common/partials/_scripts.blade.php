<!--
JavaScripts
========================== -->
<!-- main jQuery -->
{!! Minify::javascript([
    '/assets/frontend/js/vendor/modernizr-2.6.2.min.js',
    '/assets/frontend/js/vendor/jquery-1.11.1.min.js',
    '/assets/frontend/js/jquery-migrate.min.js',
    '/assets/frontend/js/bootstrap.min.js',
    '/assets/frontend/js/jquery.bxslider.min.js',
    '/assets/frontend/js/jquery.selectbox-0.2.min.js',
    '/assets/frontend/js/jquery.sticky.js',
    '/assets/frontend/js/wow.min.js',
    '/assets/frontend/js/bootstrap-notify.min.js',
    '/assets/frontend/js/main.js',
]) !!}

@if($alertToPay)
<script type="application/javascript">
    $(document).ready(function () {
        $.notify({
            message: '{{trans('common.alert-pay')}}'
        },{
            type: 'danger',
            placement: {from: "bottom", align: "left"},
            timer: 10000,
            allow_dismiss: false,
        });
    });
</script>
@endif

<script type="application/javascript">
    $(document).ready(function(){
        // Floating Contact Widget Trigger
        $(".amg-floating-icon").on("mouseenter", function(){
            $(this).closest(".amg-floating-contact-wrap").addClass("hover");
        });
        $(".amg-floating-contact-wrap").on("mouseleave", function(){
            $(this).removeClass("hover");
        });
    });
</script>

@yield('scripts')