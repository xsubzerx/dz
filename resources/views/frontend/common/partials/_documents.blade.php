@if(count($documents))
    @for($x=0;$x<$count;$x++)
        <h5>{{ trans('visas.person-docs.'.$x) }}</h5>
        <div class="divider margin5 hr5"><span></span></div>
        @foreach($documents as $doc)
            <div class="col-md-2 col-sm-4">
                <div id="file_{{$x}}_{{$doc->id}}" class="dropzone"></div>
                <p>{{ $doc->title }}</p>
                @if($doc->has_app)
                    <a href="{{ route('image', ['visas', $doc->application]) }}" target="_blank" download class="btn btn-success downloadApp"><i class="fa fa-download"></i></a>
                @endif
            </div>
        @endforeach
        <div class="clearfix"></div>
    @endfor
@endif