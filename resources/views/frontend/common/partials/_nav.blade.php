<div class="mega-menu-wrapper border clear">
    <div class="navbar-header">
        <!-- responsive nav button -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <!-- /responsive nav button -->
        <!-- logo -->
        <a class="navbar-brand" href="{{route('site.home')}}">
            <img src="{{ asset('assets/frontend/img/logo.jpg') }}" alt="{{ $config->site_name }}" height="80">
        </a>
        <!-- /logo -->
    </div>
    <!-- main nav -->
    <nav class="collapse navbar-collapse navbar-right">
        <ul class="nav navbar-nav">
            @foreach($mainNav as $item)
                <li @if(LaraLocale::getNonLocalizedURL($item->link) == LaraLocale::getNonLocalizedURL(url()->current())) class="current" @endif>
                    <a href="{{ LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), $item->link) }}">
                        {{ $item->title }}
                    </a>
                    @if(count($item->childrenItems))
                    <ul class="sub-menu">
                        @foreach($item->childrenItems as $subitem)
                            <li>
                                <a href="{{ LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), $subitem->link) }}">
                                    {{ $subitem->title }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </nav>
    <!-- /main nav -->
</div>