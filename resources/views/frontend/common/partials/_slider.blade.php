<!-- Home Banner ==================================== -->
<div id="start" class="home26 fitscreen">
    @if(count($banner))
    <div class="slide26 medical">
        @foreach($banner as $k=>$slide)
            @if(!$slide->image) @continue @endif
            <div class="item fitscreen" style="background-image: url({{ route('image', ['slides', $slide->image]) }});">
                <div class="overlay"></div>
                @if($slide->label)
                    <div class="container fitscreen">
                        <div class="item-inner {{ (($k % 2) == 0) ? 'text-left' : 'text-right' }}">
                            @if(!empty($slide->link) && $slide->link != '#')
                                <a href="{{ LaraLocale::getLocalizedURL(LaraLocale::getCurrentLocale(), $slide->link) }}"><h1>{{ $slide->label }}</h1></a>
                            @else
                                <h1>{{ $slide->label }}</h1>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
    @endif

    @include('frontend.common.partials._search')
</div>
<!-- End Home Banner ==================================== -->