<!-- footer widget ==================================== -->
<section class="footer-widget">
    <div class="container">
        <div class="row with-border">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="footer-item contact-info">
                    <h4>{{ trans('contact.contact-info') }}</h4>
                    <ul>
                        <li><i class="fa fa-building-o"></i><span>{{ $config->site_name }}</span></li>
                        <li><i class="fa fa-map-marker"></i><span>{{ $config->site_address }}</span></li>
                        <li><i class="fa fa-phone"></i><span>{{ $config->site_phone }}</span></li>
                        <li><i class="fa fa-envelope-o"></i><span><a href="mailto:{{ $config->site_email }}">{{ $config->site_email }}</a></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="footer-item recent-posts">
                    <ul>
                        <li><a href="{{ route('site.visas.index') }}">{{ trans('visas.visas') }}</a></li>
                        <li><a href="{{ route('site.hotels.index') }}">{{ trans('hotels.hotels') }}</a></li>
                        <li><a href="{{ route('site.trips.index') }}">{{ trans('trips.trips') }}</a></li>
                        <li><a href="{{ route('site.flights') }}">{{ trans('flights.flights') }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 hidden-sm hidden-xs">
                <div class="footer-item recent-posts">
                    <h4>{{ trans('common.find-facebook') }}</h4>
                    <div class="fb-page" data-href="https://www.facebook.com/Voyagedz1" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Voyagedz1" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Voyagedz1">voyagedz.com</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End footer widget ==================================== -->

<!-- footer ==================================== -->
<footer class="footer">
    <div class="container">
        <p class="copyright pull-left item_left"><strong>{{ trans('common.copyrights', ['siteName' => $config->site_name]) }}</strong></p>
        <ul class="pull-right social-links text-center item_right">
            @if($config->twitter)<li><a href="{{$config->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a></li>@endif
            @if($config->instagram)<li><a href="{{$config->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>@endif
            @if($config->facebook)<li><a href="{{$config->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>@endif
            @if($config->youtube)<li><a href="{{$config->youtube}}" target="_blank"><i class="fa fa-youtube"></i></a></li>@endif
        </ul>
    </div>
</footer>
<!-- End footer ==================================== -->

<div class="amg-floating-contacts">
    <div class="amg-floating-contact-wrap amg-green">
        <div class="amg-floating-icon">
            <i class="fa fa-phone"></i>
        </div>
        <div class="amg-floating-link amg-floating-phone">
            <a class="amg-nav-item" href="#">{{ $config->site_phone }}</a>
        </div>
    </div>
</div>