<!DOCTYPE html>
<html lang="{{ LaraLocale::getCurrentLocale() }}">
@include('frontend.common.partials._head')
<body class="bordered">
<div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=404819859950895';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <!-- Preloader -->
    <div id="preloader">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>

    <div id="wrapper" class="main-wrapper">
        @include('frontend.common.partials._welcomebar')
        @include('frontend.common.partials._topbar')
        @include('frontend.common.partials._header')
        <div  class="clearfix"></div>

        @yield('content')

        @include('frontend.common.partials._footer')

        <!-- back to top -->
        <a href="javascript:;" id="go-top"><i class="fa fa-angle-up"></i> {{ trans('common.top') }}</a>
        <!-- end #go-top -->
    </div>

    @include('frontend.common.partials._scripts')
</body>
</html>