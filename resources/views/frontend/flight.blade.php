@extends('frontend.common.template')

@section('title', trans('flights.flight-booking'))

@section('content')
    <div class="breadcrumb-area clearfix">
        <div class="container">
            <h2 class="page-title">{{ trans('flights.flight-booking') }}</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="{{ route('site.home') }}">{{ trans('common.home') }}</a></li>
                <li class="active">{{ trans('flights.flight-booking') }}</li>
            </ul>
        </div>
    </div> <!-- end .breadcrumb-area -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                @if(!empty($errors->all()))
                    <ul class="callout callout-danger">
                        @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                    </ul>
                @endif
                <div class="col-md-12 item_left">
                    {{ Form::open(['url' => route('site.flight.booking'), 'id' => 'contact-form', 'class' => 'row contact-form']) }}
                    <div class="col-md-4">
                        <input type="radio" name="type" value="one-way" id="one-way" checked="checked">
                        <label for="one-way">{{ trans('flights.types.one-way') }}</label>
                    </div>
                    <div class="col-md-4">
                        <input type="radio" name="type" value="return" id="return">
                        <label for="return">{{ trans('flights.types.return') }}</label>
                    </div>
                    <div class="col-md-4">
                        <input type="radio" name="type" value="multiple" id="multiple">
                        <label for="multiple">{{ trans('flights.types.multiple') }}</label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div id="trips">
                        <div class="col-md-4">
                            <input type="text" name="from" placeholder="{{ trans('flights.from') }}" required class="form-control">
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="to" placeholder="{{ trans('flights.to') }}" required class="form-control">
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="checkin" placeholder="{{ trans('flights.checkin') }}" required class="form-control datepicker" id="checkin">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <input type="checkbox" name="direct" value="1" id="direct">
                        <label for="direct">{{ trans('flights.direct') }}</label>
                    </div>
                    <div class="col-md-4 no-padding">
                        <label for="adults_count" class="col-md-8">{{ trans('flights.adults-count')}}</label>
                        <div class="col-md-4">
                            {{ Form::select('adults_count', [1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 1, ['id' => 'adults_count', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="col-md-8 no-padding" id="adults">
                        <div class="col-md-4">
                            <input type="text" name="adults_title[]" placeholder="{{ trans('flights.title') }}" class="form-control" required>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="adults_name[]" placeholder="{{ trans('flights.name') }}" class="form-control" required>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 no-padding">
                        <label for="childes_count" class="col-md-8">{{ trans('flights.childes-count')}}</label>
                        <div class="col-md-4">
                            {{ Form::select('childes_count', [0=>0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9], 0, ['id' => 'childes_count', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="col-md-8 no-padding" id="childes"></div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <textarea name="note" class="form-control" placeholder="{{ trans('flights.note') }}"></textarea>
                    </div>
                    <div class="col-md-12">
                        @if(auth()->guard('user')->check())
                            <input type="submit" value="{{ trans('contact.send') }}" class="message-sub pull-right btn btn-blue">
                        @else
                            <a href="{{ route('login') }}" class="btn btn-danger pull-right">{{ trans('auth.login') }}</a>
                        @endif
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@stop

@section('styles')
    {!! Minify::stylesheet('/assets/frontend/css/jquery-ui.min.css') !!}
@append

@section('scripts')
    {!! Minify::javascript('/assets/frontend/js/jquery-ui.min.js') !!}
    <script>
        $(document).ready(function () {
            loadDatePicker();
            function loadDatePicker() {
                $(".datepicker").datepicker({
                    minDate: 0,
                    dateFormat: 'yy-mm-dd',
                    onSelect: function (selected) {
                        $('#checkout').datepicker('option', 'minDate', selected)
                    }
                });

                $(".datepicker2").datepicker({
                    minDate: 0,
                    dateFormat: 'yy-mm-dd',
                    onSelect: function (selected) {
                        $('#checkin').datepicker('option', 'maxDate', selected)
                    }
                });
            }

            $('#one-way').on('change', function () {
                $('#trips').html('<div class="col-md-4"><input type="text" name="from" placeholder="{{ trans('flights.from') }}" required class="form-control"></div><div class="col-md-4"><input type="text" name="to" placeholder="{{ trans('flights.to') }}" required class="form-control"></div><div class="col-md-4"><input type="text" name="checkin" placeholder="{{ trans('flights.checkin') }}" required class="form-control datepicker" id="checkin"></div>');
                loadDatePicker();
            });

            $('#return').on('change', function () {
                $('#trips').html('<div class="col-md-3"><input type="text" name="from" placeholder="{{ trans('flights.from') }}" required class="form-control"></div><div class="col-md-3"><input type="text" name="to" placeholder="{{ trans('flights.to') }}" required class="form-control"></div><div class="col-md-3"><input type="text" name="checkin" placeholder="{{ trans('flights.checkin') }}" required class="form-control datepicker" id="checkin"></div><div class="col-md-3"><input type="text" name="checkout" placeholder="{{ trans('flights.checkout') }}" required class="form-control datepicker2" id="checkout"></div>');
                loadDatePicker();
            });

            $('#multiple').on('change', function () {
                $('#trips').html('<div class="col-md-4"><input type="text" name="from[]" placeholder="{{ trans('flights.from') }}" required class="form-control"></div><div class="col-md-4"><input type="text" name="to[]" placeholder="{{ trans('flights.to') }}" required class="form-control"></div><div class="col-md-4"><input type="text" name="checkin[]" placeholder="{{ trans('flights.checkin') }}" required class="form-control datepicker" id="checkin"></div><div class="col-md-12"><button type="button" class="btn btn-sm btn-success pull-right" id="addTrip"><i class="fa fa-plus"></i></button></div>');
                loadDatePicker();
            });

            $(document).on('click', '#addTrip', function () {
                $(this).parent().before('<div class="col-md-4"><input type="text" name="from[]" placeholder="{{ trans('flights.from') }}" required class="form-control"></div><div class="col-md-4"><input type="text" name="to[]" placeholder="{{ trans('flights.to') }}" required class="form-control"></div><div class="col-md-4"><input type="text" name="checkin[]" placeholder="{{ trans('flights.checkin') }}" required class="form-control datepicker"></div>');
                loadDatePicker();
            });

            $('#adults_count').on('change', function () {
                var count = $(this).val();
                $('#adults').html('');
                for(var i=0;i<count;i++){
                    $('#adults').append('<div class="col-md-4"><input type="text" name="adults_title[]" placeholder="{{ trans('flights.title') }}" class="form-control" required></div><div class="col-md-8"><input type="text" name="adults_name[]" placeholder="{{ trans('flights.name') }}" class="form-control" required></div>');
                }
            });

            $('#childes_count').on('change', function () {
                var count = $(this).val();
                $('#childes').html('');
                for(var i=0;i<count;i++){
                    $('#childes').append('<div class="col-md-12"><input type="text" name="childes_birthdate[]" placeholder="{{ trans('flights.birthdate') }}" class="form-control datepicker" required></div>');
                }
                loadDatePicker();
            });
        });
    </script>
@append