<?php

return [
    'countries'         => 'البلدان',
    'country'           => 'البلد',
    'view-countries'    => 'عرض البلدان',
    'title'             => 'اسم البلد',
    'flag'              => 'علم البلد',
    'create-country'    => 'إضافة بلد جديدة',
    'update-country'    => 'تحديث بيانات بلد',
    'select'            => '-- اختر بلد --',
];
