<?php

return [
    'flights'                   => 'رحلات الطيران',
    'country-flights'           => 'رحلات الطيران إلي :country',
    'accept-terms'              => 'أوافق علي  <a href="#" data-toggle="modal" data-target="#terms">شروط والأحكام</a> رحلات الطيران',
    'flight-booking'            => 'حجز رحلة طيران',
    'types'                     => [
        'type'      => 'نوع الرحلة',
        'one-way'   => 'ذهاب فقط',
        'return'    => 'ذهاب وعودة',
        'multiple'  => 'وجهات متعددة'
    ],
    'trips'                     => 'رحلات الطيران',
    'from'                      => 'نقطة الإنطلاق',
    'to'                        => 'الوجهة',
    'checkin'                   => 'تاريخ السفر',
    'checkout'                  => 'تاريخ العودة',
    'adults-count'              => 'عدد البالغين',
    'childes-count'             => 'عدد الأطفال',
    'title'                     => 'اللقب',
    'name'                      => 'الاسم',
    'birthdate'                 => 'تاريخ الميلاد',
    'note'                      => 'ملاحظات',
    'direct'                    => 'طيران مباشر فقط؟',
    'request-success'           => '<div class="alert alert-success">ابعت بينات سفرك المتوقعة و سيتم التواصل معاك خلال ساعة عمل كحد اقصي لعرض لك افضل تذاكر الطيران في كل شركات الطيران في العالم و  الدفع عن طريق CCPأو نقدا في وكالتنا</div>',
    'statuses'                  => [
        'pending'   => ['label' => 'جاري الفحص', 'style' => '<span class="label label-warning">جاري الفحص</span>'],
        'confirmed' => ['label' => 'تم تأكيد الحجز', 'style' => '<span class="label label-success">تم تأكيد الحجز</span>'],
        'canceled'  => ['label' => 'إلغاء', 'style' => '<span class="label label-danger">إلغاء</span>'],
    ],
    'adults'                    => 'بيانات البالغين',
    'childes'                   => 'بيانات الأطفال',
    'flights-requests'          => 'طلبات الطيران',

];