<?php

return [
    'cities'        => 'المدن',
    'city'          => 'المدينة',
    'view-cities'   => 'عرض المدن',
    'city-title'    => 'اسم المدينة',
    'create-city'   => 'إضافة مدينة جديدة',
    'update-city'   => 'تحديث بيانات مدينة',
    'select-city'   => 'قم بتحديد مدينة',
    'all-cities'    => 'جميع المدن',
    'select'        => '-- اختر مدينة --',
];
