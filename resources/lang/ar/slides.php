<?php

return [
    'slides'        => 'شرائح الصور',
    'slides-list' 	=> 'قائمة الشرائح',
    'create-slide'  => 'إضافة شريحة جديدة',
    'update-slide'	=> 'تعديل بيانات شريحة',
    'view-slides'	=> 'عرض الشرائح',
    'label'			=> 'الوصف',
    'order' 	    => 'الترتيب',
    'description'   => 'الوصف',
    'link'          => 'الرابط',
    'image'         => 'الصورة'
];
