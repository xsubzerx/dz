<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'failed'            => 'هذا التصريح غير مطابق لسجلات الدخول لدينا.',
    'throttle'          => 'محاولات دخول عديدة. نرجوا المحاولة بعد :seconds ثانية.',
    'register'          => 'التسجيل',
    'agency-register'   => 'تسجيل الوكالات',
    'agency-register-box'=> 'تسجيل الوكالات السياحية اضغط هنا',
    'register-now'      => 'سجل الأن!',
    'password'          => 'كلمة المرور',
    'email'             => 'بريدك الإلكتروني',
    'remail'            => 'تأكيد  البريد الإلكتروني',
    'terms'             => 'موافق علي الشروط والقوانين.',
    'welcome'           => 'أهلاً بعودتك ',
    'login'             => 'تسجيل دخول',
    'signin'            => 'تسجيل دخول',
    'sign-me-in'        => 'دخول',
    'forgot-password'   => 'نسيت كلمة المرور؟',
    'remember'          => 'تذكرني',
    'don-have-account'  => 'ليس لديك حساب بعد!',
    'repass'            => 'إعادة كتابة كلمة المرور',
    'have-account'      => 'لديك حساب بالفعل؟',
    'login-facebook'    => 'قم بتسجيل الدخول بحساب الفيسبوك',
    'cpanel-login'      => 'تسجيل دخول لوحة الإدارة',
    'password-reset'    => 'إعادة ضبط كلمة المرور',
    'reset-email-msg1'  => 'You are receiving this email because we received a password reset request for your account.',
    'reset-email-msg2'  => 'If you did not request a password reset, no further action is required.',
    'register-confirm-msg' => '<div class="alert alert-info">تم التسجيل بنجاح وسيتم مراجعه وتفعيل الحساب عن طريق مدير الموقع في اقرب وقت.</div>',
    'inactive-account'  => '<div class="alert alert-danger">هذا الحساب غير مفعل!</div>',

];
