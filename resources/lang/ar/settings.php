<?php

return [
    'teb-title'     => 'إعداد الموقع',
    'update-Settings' => 'تحديث إعداد الموقع',
    'site-name' => 'إسم الموقع',
    'site-metakey' => 'الكلمات المفتاحية',
    'site-metadesc' => 'الكلمات الوصفية',
    'site-email' => 'البريد الإلكتروني للموقع',
    'site-phone' => 'رقم التليفون الخاص بالموقع',
    'currency-rate' => 'سعر تغيير العملة',
    'global-settings'       => 'الإعدادت العامة',
    'terms'                 => 'الشروط والأحكام',
    'visas-terms'           => 'شروط وأحكام التأشيرات',
    'hotels-terms'          => 'شروط وأحكام الفنادق',
    'trips-terms'           => 'شروط وأحكام الرحلات',
    'flights-terms'         => 'شروط وأحكام رحلات الطيران',
    'agencies-terms'        => 'شروط وأحكام الوكالات',
    'contact-info'          => 'بيانات الإتصال',
    'contact-page'          => 'صفحه الإتصال',
];
