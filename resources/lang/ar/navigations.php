<?php

return [
    'navigations'   => 'القوائم',
    'items-list' 	=> 'قائمة العناصر',
    'create-item'   => 'إضافة عنصر جديدة',
    'update-item'	=> 'تعديل بيانات عنصر',
    'view-items'	=> 'عرض العناصر',
    'title'			=> 'عنوان العنصر',
    'order' 	    => 'الترتيب',
    'link'          => 'الرابط',
    'parent'        => 'تابع لـ',
];
