<?php

return array(
	'content-pages' => 'صفحات المحتوي',
	'pages-list' 	=> 'قائمة الصفحات',
	'create-page'   => 'إضافة صفحة جديدة',
	'update-page'	=> 'تعديل بيانات صفحة',
	'view-pages'	=> 'عرض الصفحات',
	'title'			=> 'عنوان الصفحة',
	'image'			=> 'صورة الصفحة',
	'page-data'		=> 'بيانات الصفحة',
	'content'		=> 'محتوي الصفحة',
);
