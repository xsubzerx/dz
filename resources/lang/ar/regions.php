<?php

return [
    'regions'       => 'المناطق',
    'region'        => 'المنطقة',
    'view-regions'  => 'عرض المناطق',
    'region-title'  => 'اسم المنطقة',
    'create-region' => 'إضافة منطقة جديدة',
    'update-region' => 'تحديث بيانات منطقة',
    'all-regions'   => 'جميع المناطق',
];
