<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'le mot de passe ne dois pas contenir moins de six lettres.',
    'reset' => 'le mot de passe a été réinitialiser!',
    'sent' => 'un lien vous a été envoyer pour la réinitialisation du mot de passe!',
    'token' => 'le code envoyer est incorrecte.',
    'user' => "Aucun compte n’a été trouvé avec cet email.",

];
