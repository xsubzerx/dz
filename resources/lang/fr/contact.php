<?php

return [
    'contact-us'        => 'Nous contacter',
    'contact-info'      => 'Informations pour nous contacter',
    'any-thoughts'      => 'Des idées à proposer?',
    'send'              => 'nous envoyer',
    'sent-success'      => '<div class="alert alert-success">envoie avec succès.</div>',
    'sent-fail'         => '<div class="alert alert-danger">l’envoie a échouer.</div>',
    'message'           => 'Message',
    'sender'            => 'transmetteur',
    'receiver'          => 'récepteur',
    'sent-at'           => 'date d’envoi',
    'inbox'             => 'boite de reception',
    'outbox'            => 'boite d’envoie',
    'sent-by'           => 'envoie par',
    'sent-you'          => 'vous a envoyé.',
    'contact-page-txt'  => '<p>We are ready to serve you very well, be free to contact us anytime!</p><br><p>We also encourage you to check our <a href=":link"> frequently asked questions (FAQ)</a>. This list of commonly-asked questions and answers could be just what you need to find a quick answer to your question.</p>',
    'subject'           => 'sujet',
	'newsletter'		=> 'Newsletter',

];