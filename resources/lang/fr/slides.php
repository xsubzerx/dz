<?php

return [
    'slides'        => 'Diapositifs',
    'slides-list' 	=> 'liste des diapositifs',
    'create-slide'  => 'Ajouter un diapositif',
    'update-slide'	=> 'mettre à jour un diapositif',
    'view-slides'	=> 'voir les diapositifs',
    'label'			=> 'description',
    'order' 	    => 'ordre',
    'link'          => 'lien',
    'description'   => 'Description',
    'image'         => 'Image'
];
