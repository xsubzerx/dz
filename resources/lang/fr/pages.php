<?php

return array(
	'content-pages' => 'pages du contenu',
	'pages-list' 	=> 'liste des pages',
	'create-page'   => 'ajouter une page',
	'update-page'	=> 'modifier une page',
	'view-pages'	=> 'voir les pages',
	'title'			=> 'Page titres',
	'image'			=> 'Page Photo',
	'page-data'		=> 'informations de la page',
	'content'		=> 'contenue de la page',
);
