<?php

return [
    'countries'         => 'pays',
    'country'           => 'pays',
    'view-countries'    => 'voir les villes',
    'title'             => 'titre',
    'flag'              => 'drapeau',
    'create-country'    => 'ajouter un pays',
    'update-country'    => 'mettre à jour un pays',
    'select'            => '-- selectionner pays --',
];