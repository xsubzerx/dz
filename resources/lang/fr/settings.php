<?php

return [
    'teb-title'             => 'paramètres du site',
    'update-Settings'       => 'mise à jour des paramètres',
    'site-name'             => 'nom du site',
    'site-metakey'          => 'les mots clefs',
    'site-metadesc'         => 'mots descriptifs',
    'site-email'            => 'E-Mail du site',
    'site-phone'            => 'numéro de téléphone du site',
    'currency-rate'         => 'taux de change',
    'global-settings'       => 'paramètres général',
    'terms'                 => 'termes et conditions',
    'visas-terms'           => 'termes et conditions du visa',
    'hotels-terms'          => 'termes et conditions dues hôtels',
    'trips-terms'           => 'termes et conditions des voyages organisés',
    'flights-terms'         => 'termes et conditions des vols',
    'agencies-terms'        => 'termes et conditions des agences',
    'contact-info'          => 'informations des contacte',
    'contact-page'          => 'page de contacte',

];
