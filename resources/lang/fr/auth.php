<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'            => 'Erreur vos coordonnée ne sont pas compatible.',
    'throttle'          => 'Trop de tentatives veuillez réessayer dans quelque secondes.',
    'register'          => 'se connecter',
    'agency-register'   => 'se connecter agences',
	'agency-register-box'=> 'se connecter agences cliquez ici',
    'register-now'      => 'Crée un compte!',
    'password'          => 'Mot de passe',
    'email'             => 'E-Mail',
    'remail'            => 'Confirmation E-Mail',
    'terms'             => 'J’accepte les termes et conditions.',
    'welcome'           => 'Bienvenue',
    'login'             => 'S’identifier',
    'signin'            => 'Se connecter',
    'sign-me-in'        => 'Me connecter',
    'forgot-password'   => 'Mot de passe oublier?',
    'remember'          => 'Se rappeler de moi',
    'don-have-account'  => 'Vous n’avez pas encore de compte?',
    'repass'            => 'Réécrivez votre mot de passe',
    'have-account'      => 'Vous n’avez réellement pas de compte?',
    'login-facebook'    => 'S’identifier avec Facebook',
    'cpanel-login'      => 'S’identifier dans le Panneau de contrôle',
    'password-reset'    => 'réinitialiser le mot de passe',
    'reset-email-msg1'  => 'Vous recevez cet e-mail parce que nous avons reçu une demande de réinitialisation de mot de passe de votre compte.',
    'reset-email-msg2'  => 'Si Vous n\'avez pas demandé de réinitialisation du mot de passe, aucune action supplémentaire n\'est requise.',
    'register-confirm-msg' => '<div class="alert alert-info">تم التسجيل بنجاح وسيتم مراجعه وتفعيل الحساب عن طريق مدير الموقع في اقرب وقت.</div>',
    'inactive-account'  => '<div class="alert alert-danger">Ce compte n\'est pas activé!</div>',
];
