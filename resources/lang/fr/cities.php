<?php

return [
    'cities'        => 'villes',
    'city'          => 'ville',
    'view-cities'   => 'Voir les villes',
    'city-title'    => 'Nom de la ville',
    'create-city'   => 'Ajouter une ville',
    'update-city'   => 'Mettre à jour une ville',
    'select-city'   => 'Sélectionner une ville',
    'all-cities'    => 'Toutes les villes',
    'select'        => '-- Sélectionner la ville --',
];
