<?php

return [
    'regions'       => 'Regions',
    'region'        => 'Region',
    'view-regions'  => 'View Regions',
    'region-title'  => 'Region Name',
    'create-region' => 'Create New Region',
    'update-region' => 'Update Region',
    'all-regions'   => 'All Regions',
];
