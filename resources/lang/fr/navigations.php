<?php

return [
    'navigations'   => 'menu',
    'items-list' 	=> 'liste des menu',
    'create-item'   => 'ajouter un composant',
    'update-item'	=> 'Mettre à jour un composant',
    'view-items'	=> 'Voir les composants',
    'title'			=> 'Titre',
    'order' 	    => 'ordre',
    'link'          => 'lien',
    'parent'        => 'suite à',
];
