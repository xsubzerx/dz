<?php

return [
    'flights'                   => 'vols',
    'country-flights'           => 'vols vers :country',
    'accept-terms'              => 'j’accepte vols <a href="#" data-toggle="modal" data-target="#terms">les termes et conditions</a>.',
    'flight-booking'            => 'réservations de vols',
    'types'                     => [
        'type'      => 'vols Type',
        'one-way'   => 'aller seulemetn',
        'return'    => 'aller- retour',
        'multiple'  => 'plusieurs destinations'
    ],
    'trips'                     => 'vols voyages',
    'from'                      => 'de',
    'to'                        => 'à',
    'checkin'                   => 'date de départ',
    'checkout'                  => 'date du retour',
    'adults-count'              => 'nombre des adultes',
    'childes-count'             => 'nombre des enfants',
    'title'                     => 'nom',
    'name'                      => 'prénom',
    'birthdate'                 => 'date de naissance',
    'note'                      => 'notes',
    'direct'                    => 'Vols directs uniquement?',
    'request-success'           => '<div class="alert alert-success">Envoyer vos donnée de voyage et on vous serez contacté dans quelque heurs pour vous communiquer les vols disponible, paiement via CCP ou main à main</div>',
    'statuses'                  => [
        'pending'  => ['label' => 'Vérification', 'style' => '<span class="label label-primary">Vérification</span>'],
        'confirmed' => ['label' => 'Réservation confirmée', 'style' => '<span class="label label-success">Réservation confirmée</span>'],
        'canceled'  => ['label' => 'Annulé', 'style' => '<span class="label label-danger">Annulé</span>'],
    ],
    'adults'                    => 'informations des adultes.',
    'childes'                   => 'information des enfants.',
    'flights-requests'          => 'Demandes de réservations de vol',

];