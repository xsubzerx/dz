<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codeuser_id');
            $table->integer('user_id');
            $table->enum('type', ['one-way', 'return', 'multiple']);
            $table->text('trips');
            $table->tinyInteger('direct')->default(0);
            $table->integer('adults_count');
            $table->text('adults');
            $table->integer('childes_count')->default(0);
            $table->text('childes')->nullable();
            $table->text('note')->nullable();
            $table->enum('status', ['pending', 'confirmed', 'canceled'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
