<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('title_ar');
            $table->string('title_fr');
            $table->text('description_ar');
            $table->text('description_fr');
            $table->text('has_ar')->nullable();
            $table->text('has_fr')->nullable();
            $table->text('has_not_ar')->nullable();
            $table->text('has_not_fr')->nullable();
            $table->string('single_price');
            $table->string('double_price');
            $table->string('triple_price');
            $table->string('quad_price')->default(0);
            $table->string('quinary_price')->default(0);
            $table->string('childes_first_price');
            $table->string('childes_second_price');
            $table->string('childes_third_price');
            $table->string('commission')->nullable();
            $table->tinyInteger('fixed')->default(0);
            $table->date('start');
            $table->date('end');
            $table->tinyInteger('status');
            $table->tinyInteger('featured')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by')->default(0);
            $table->timestamps();
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
