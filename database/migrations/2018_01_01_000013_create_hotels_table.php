<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();
            $table->string('name_ar');
            $table->string('name_fr');
            $table->text('description_ar')->nullable();
            $table->text('description_fr')->nullable();
            $table->string('address_ar')->nullable();
            $table->string('address_fr')->nullable();
            $table->string('map')->nullable();
            $table->string('phone')->nullable();
            $table->tinyInteger('stars');
            $table->text('nearby_ar')->nullable();
            $table->text('nearby_fr')->nullable();
            $table->text('amenities')->nullable();
            $table->integer('childes_phase1_age');
            $table->string('childes_phase1_price');
            $table->integer('childes_phase2_age');
            $table->string('childes_phase2_price');
            $table->tinyInteger('status');
            $table->tinyInteger('featured')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by')->default(0);
            $table->timestamps();
        });

        Schema::table('hotels', function (Blueprint $table) {
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
