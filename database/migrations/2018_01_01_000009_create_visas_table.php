<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->string('title_ar');
            $table->string('title_fr');
            $table->text('description_ar');
            $table->text('description_fr');
            $table->string('price');
            $table->tinyInteger('has_shipping')->default(0);
            $table->string('middle_price')->default(0);
            $table->string('east_west_price')->default(0);
            $table->string('south_price')->default(0);
            $table->tinyInteger('status');
            $table->tinyInteger('featured')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by')->default(0);
            $table->timestamps();
        });

        Schema::table('visas', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visas');
    }
}
