<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_name_ar');
            $table->string('site_name_fr');
            $table->string('site_metakey_ar')->nullable();
            $table->string('site_metakey_fr')->nullable();
            $table->text('site_metadesc_ar')->nullable();
            $table->text('site_metadesc_fr')->nullable();
            $table->string('site_email');
            $table->string('site_phone')->nullable();
            $table->string('address_ar')->nullable();
            $table->string('address_fr')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('youtube')->nullable();
            $table->string('instagram')->nullable();
            $table->string('currency_rate')->nullable();
            $table->text('contact_page_ar')->nullable();
            $table->text('contact_page_fr')->nullable();
            $table->text('visas_terms_ar')->nullable();
            $table->text('visas_terms_fr')->nullable();
            $table->text('hotels_terms_ar')->nullable();
            $table->text('hotels_terms_fr')->nullable();
            $table->text('trips_terms_ar')->nullable();
            $table->text('trips_terms_fr')->nullable();
            $table->text('flights_terms_ar')->nullable();
            $table->text('flights_terms_fr')->nullable();
            $table->text('agencies_terms_ar')->nullable();
            $table->text('agencies_terms_fr')->nullable();
            $table->tinyInteger('status');
            $table->integer('updated_by')->default(0);
            $table->timestamps();
        });

        DB::table('settings')->insert(
            [
                'id'            => '1',
                'site_name_ar'  => 'VoyageDZ',
                'site_name_fr'  => 'VoyageDZ',
                'site_email'    => 'info@voyagedz.com',
                'status'        => '1'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
