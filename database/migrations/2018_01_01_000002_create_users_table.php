<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname'); // Or Agency Name
            $table->string('lastname'); // Or Owner Name
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('passport')->nullable(); // User Only
            $table->string('address');
            $table->string('section')->nullable();
            $table->string('town')->nullable();
            $table->string('state');
            $table->string('website')->nullable(); // Agency Only
            $table->string('facebook')->nullable(); // Agency Only
            $table->string('license_number')->nullable(); // Agency Only
            $table->integer('license_year')->nullable(); // Agency Only
            $table->string('password');
            $table->string('photo')->nullable();
            $table->string('background_color')->nullable();
            $table->string('font_color')->nullable();
            $table->enum('type', ['user', 'agency'])->default('user');
            $table->tinyInteger('status')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
