<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('created_by');
            $table->integer('updated_by')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('admins')->insert(
            [
                'id'            => '1',
                'name'          => 'Administrator',
                'email'         => 'cs.ahmedos@gmail.com',
                'password'      => Hash::make('P@zzw0rd'),
                'created_by'    => '0'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
