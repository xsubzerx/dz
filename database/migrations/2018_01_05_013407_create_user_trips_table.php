<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_trips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->integer('user_id')->unsigned();
            $table->integer('trip_id')->unsigned();
            $table->integer('single')->default(0);
            $table->integer('double')->default(0);
            $table->integer('triple')->default(0);
            $table->integer('quad')->default(0);
            $table->integer('quinary')->default(0);
            $table->integer('childes_first')->default(0);
            $table->integer('childes_second')->default(0);
            $table->integer('childes_third')->default(0);
            $table->string('total');
            $table->enum('status', ['pending', 'progress', 'confirmed', 'canceled'])->default('pending');
            $table->timestamps();
        });

        Schema::table('user_trips', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('trip_id')->references('id')->on('trips')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_trips');
    }
}
