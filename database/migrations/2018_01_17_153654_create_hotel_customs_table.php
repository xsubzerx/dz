<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelCustomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_customs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->integer('user_id');
            $table->string('destination');
            $table->string('hotel')->nullable();
            $table->date('checkin');
            $table->date('checkout');
            $table->string('budget')->nullable();
            $table->integer('rooms_count');
            $table->integer('adults_count');
            $table->integer('childes_count')->default(0);
            $table->text('childes_ages')->nullable();
            $table->text('note')->nullable();
            $table->enum('status', ['pending', 'confirmed', 'canceled'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_customs');
    }
}
