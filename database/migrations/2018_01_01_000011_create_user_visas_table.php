<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVisasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_visas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->integer('user_id')->unsigned();
            $table->integer('visa_id')->unsigned();
            $table->integer('count');
            $table->enum('shipping_state', ['none','middle','east-west','south'])->default('none');
            $table->string('shipping_address')->nullable();
            $table->string('total')->default(0);
            $table->enum('status', ['pending', 'progress', 'completed', 'shipped', 'canceled'])->default('pending');
            $table->timestamps();
        });

        Schema::table('user_visas', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('visa_id')->references('id')->on('visas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_visas');
    }
}
