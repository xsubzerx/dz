<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'frontend'], function($route) {
	Route::get('countries', 'HomepageController@countriesApi');
	Route::get('cities/{country}', 'HomepageController@citiesApi');
    Route::post('register', 'Auth\RegisterController@registerApi')->name('api.register');
	Route::group(['namespace' => 'Auth'], function ($route) {
		Route::post('login/refresh', 'LoginController@refreshTokenApi');
		Route::post('login', 'LoginController@loginApi');
	});

    Route::get('visas', 'VisasApiController@index')->name('api.visas.index');
    Route::get('visa-request/{country}', 'VisasApiController@getCountryVisas')->name('api.visa.country');
	Route::get('visas/{id}', 'VisasApiController@getVisaDocs');
    Route::post('visa/documents', 'VisasApiController@getVisaDocs')->name('api.visa.documents');
    Route::post('visa/price', 'VisasApiController@getVisaPrice')->name('api.visa.price');

    Route::get('hotels', 'HotelsApiController@index')->name('api.hotels.index');
    Route::get('hotels/{hotel}', 'HotelsApiController@show')->name('api.hotels.show');

    Route::get('trips', 'TripsApiController@index')->name('api.trips.index');
	Route::get('trips/dates', 'TripsApiController@tripsDatesFilter')->name('api.trips.dates');
    Route::get('trips/{trip}', 'TripsApiController@show')->name('api.trips.show');

    Route::group(['middleware' => 'auth:api'], function($route) {
		Route::post('logout', 'Auth\LoginController@logoutApi');
        Route::get('user/authorized', function (){
            return response()->json(['user' => auth()->guard('api')->user()], 200);
        });
		Route::post('hotels', 'HotelsApiController@booking')->name('api.hotels.booking');
    });
});