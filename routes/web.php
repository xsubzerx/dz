<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('upload/temp', 'Controller@uploadTemp')->name('upload.temp');

Route::group(array('prefix' => 'image/{dir}/'), function() {
    Route::get('{filename}', 'Controller@getImage')->name('image');
    Route::get('large-{filename}', 'Controller@getImage')->name('image.large');
    Route::get('medium-{filename}', 'Controller@getImage')->name('image.medium');
    Route::get('small-{filename}', 'Controller@getImage')->name('image.small');
    Route::get('thumbnail-{filename}', 'Controller@getImage')->name('image.thumbnail');
});

Route::group([ 'prefix' => LaravelLocalization::setLocale(), 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ] ], function (){
    Route::group(['prefix' => 'backend', 'namespace' => 'backend'], function($route) {
        Route::group(['namespace' => 'Auth'], function () {
            //Admin Login
            Route::get('login', 'LoginController@showLoginForm')->name('admin.login');
            Route::post('login', 'LoginController@login')->name('admin.login');
            Route::get('logout', 'LoginController@logout')->name('admin.logout');
            //Admin Passwords
            Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.send.reset');
            Route::post('password/reset', 'ResetPasswordController@reset')->name('admin.reset');
            Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.reset');
            Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.reset.form');
        });

        Route::group(['middleware' => 'admin'], function () {
            Route::resource('navigations', 'NavigationsController')->except('show');
            Route::resource('slides', 'SlidesController')->except('show');
            Route::resource('countries', 'CountriesController')->except('show');
            Route::resource('cities', 'CitiesController')->except('show');
            Route::get('cities/country/{coid}/{ctid?}', 'CitiesController@country')->name('cities.country');
            Route::resource('pages', 'PagesController')->except('show');
            Route::resource('admins', 'AdminsController')->except('show');
            Route::resource('users', 'UsersController')->except('show');
            Route::get('users/generate', 'UsersController@generate')->name('users.generate');
            Route::resource('visa', 'VisasController')->except('show');
            Route::get('visa/requests', 'VisasController@requests')->name('visa.requests');
            Route::get('visa/request/{id}', 'VisasController@showRequest')->name('visa.request');
            Route::post('visa/request/edit', 'VisasController@updateRequest')->name('visa.request.edit');
            Route::get('documents/{visa}', 'DocumentsController@index')->name('documents.index');
            Route::get('documents/create/{visa}', 'DocumentsController@create')->name('documents.create');
            Route::resource('documents', 'DocumentsController')->except('index','create','show');

            Route::resource('hotels', 'HotelsController')->except('show');
            Route::match(['get','post'], 'hotels/album/{hotel}', 'HotelsController@album')->name('hotels.album');
            Route::get('hotels/album/makeFeatured/{photo}', 'HotelsController@makeFeatured')->name('hotels.album.featured');
            Route::delete('hotels/photo/{photo}', 'HotelsController@destroyPhoto')->name('hotels.photo.destroy');
            Route::get('hotels/requests', 'HotelsController@requests')->name('hotels.requests');
            Route::get('hotels/request/{id}', 'HotelsController@showRequest')->name('hotels.request');
            Route::post('hotels/request/edit', 'HotelsController@updateRequest')->name('hotels.request.edit');
            Route::get('hotels/custom/requests', 'HotelsController@requestsCustom')->name('hotels.requests-custom');
            Route::get('hotels/request/custom/{id}', 'HotelsController@showRequestCustom')->name('hotels.request.custom');
            Route::post('hotels/request/custom/edit', 'HotelsController@updateRequestCustom')->name('hotels.request.custom.edit');

            Route::resource('amenities', 'AmenitiesController')->except('show', 'create', 'edit', 'update');

            Route::post('rooms/removePhoto', 'RoomsController@removeImg')->name('room.photo.delete');
            Route::get('rooms/{hotel}', 'RoomsController@index')->name('rooms.index');
            Route::get('rooms/create/{hotel}', 'RoomsController@create')->name('rooms.create');
            Route::resource('rooms', 'RoomsController')->except('show', 'index', 'create');

            Route::resource('trips', 'TripsController')->except('show');
            Route::get('trips/requests', 'TripsController@requests')->name('trips.requests');
            Route::get('trips/request/{id}', 'TripsController@showRequest')->name('trips.request');
            Route::match(['get','post'], 'trips/album/{trip}', 'TripsController@album')->name('trips.album');
            Route::post('trips/request/edit', 'TripsController@updateRequest')->name('trips.request.edit');
            Route::get('trips/album/makeFeatured/{photo}', 'TripsController@makeFeatured')->name('trips.album.featured');
            Route::delete('trips/photo/{photo}', 'TripsController@destroyPhoto')->name('trips.photo.destroy');

            Route::get('flights/requests', 'FlightsController@requests')->name('flights.requests');
            Route::get('flights/request/{id}', 'FlightsController@showRequest')->name('flights.request');
            Route::post('flights/request/edit', 'FlightsController@updateRequest')->name('flights.request.edit');

            Route::match(['get', 'post'], 'settings', 'SettingsController@index')->name('settings');            // Site Settings
            Route::match(['get', 'post'], 'terms', 'SettingsController@terms')->name('terms');                  // Terms Settings
            Route::match(['get', 'post'], 'contacts', 'SettingsController@contacts')->name('contacts');         // Contacts Info Settings
            Route::get('contact-us', 'DashboardController@contact')->name('contact-us');                                    // Contact Us Emails
            Route::get('newsletter', 'NewsletterController@index')->name('newsletter');                                    // Backend Dashboard
            Route::post('newsletter', 'NewsletterController@send')->name('newsletter');                                    // Backend Dashboard
            Route::get('/', 'DashboardController@index')->name('dashboard');                                    // Backend Dashboard
        });
    });

    Route::group(['namespace' => 'frontend'], function($route) {
        Route::group(['middleware' => 'user'], function (){
            Route::get('profile', 'UsersController@profile')->name('site.profile');
            Route::put('profile', 'UsersController@profileHandle')->name('site.profile');

            Route::get('agency/trips', 'UsersController@trips')->name('agency.trips');
            Route::match(['get', 'post'], 'agency/trips/create', 'UsersController@createTrip')->name('agency.trips.create');
            Route::match(['get', 'put'], 'agency/trips/edit/{id}', 'UsersController@editTrip')->name('agency.trips.edit');
            Route::delete('agency/trips/destroy/{id}', 'UsersController@destroyTrip')->name('agency.trips.destroy');
            Route::match(['get', 'post'], 'agency/trips/album/{trip}', 'UsersController@tripAlbum')->name('agency.trips.album');
            Route::get('agency/trips/album/makeFeatured/{photo}', 'UsersController@makeFeatured')->name('agency.trips.album.featured');
            Route::delete('agency/trips/photo/{photo}', 'UsersController@destroyPhoto')->name('agency.trips.photo.destroy');

            Route::post('visa-request', 'VisasController@visaRequest')->name('site.visa.request');
            Route::get('my-visas', 'VisasController@myVisasRequests')->name('site.my-visas');

            Route::post('hotels/special', 'HotelsController@custom')->name('site.hotels.custom');
            Route::post('hotels/bookingConfirm', 'HotelsController@bookingConfirm')->name('site.hotels.booking.confirm');
            Route::post('hotels/{id}', 'HotelsController@booking')->name('site.hotels.booking');
            Route::get('my-hotels', 'HotelsController@myHotelsRequests')->name('site.my-hotels');
            Route::get('my-hotels/special', 'HotelsController@myHotelsRequestsCustom')->name('site.my-hotels.custom');

            Route::post('trips/{id}', 'TripsController@bookingConfirm')->name('site.trips.booking');
            Route::get('my-trips', 'TripsController@myTripsRequests')->name('site.my-trips');
            Route::get('my-trips-requests', 'TripsController@agencyTripsRequests')->name('site.agency-trips-requests');

            Route::post('flights/booking', 'FlightsController@booking')->name('site.flight.booking');
            Route::get('my-flights', 'FlightsController@myFlightsRequests')->name('site.my-flights');
            Route::get('flight/{id}', 'FlightsController@show')->name('site.flight.show');

            Route::get('payments/upload/{action}/{id}', 'PaymentsController@paymentUpload')->name('site.payments.upload');
            Route::post('payments/upload', 'PaymentsController@paymentUploadHandle')->name('site.payments.upload.post');
        });

        Auth::routes();
        Route::get('register/agency', 'Auth\RegisterController@showAgencyRegistrationForm')->name('register.agency');

        Route::get('visas', 'VisasController@index')->name('site.visas.index');
        Route::get('visa-request/{country}', 'VisasController@getCountryVisas')->name('site.visa.country');
        Route::post('visa/documents', 'VisasController@getVisaDocs')->name('site.visa.documents');
        Route::post('visa/price', 'VisasController@getVisaPrice')->name('site.visa.price');

        Route::get('hotels', 'HotelsController@index')->name('site.hotels.index');
        Route::get('hotels/special', 'HotelsController@custom')->name('site.hotels.custom');
        Route::get('hotels/{id}', 'HotelsController@show')->name('site.hotels.show');

        Route::get('trips', 'TripsController@index')->name('site.trips.index');
        Route::get('trips/{id}', 'TripsController@show')->name('site.trips.show');

        Route::get('flights', 'FlightsController@index')->name('site.flights');

        Route::get('getCountryCities/{country}/{city?}', 'HomepageController@getCountryCities');
        Route::get('getCountryCitiesById/{country}/{city?}', 'HomepageController@getCountryCitiesById');

        Route::get('contact-us.html', 'HomepageController@showContactUs')->name('site.contact-us');
        Route::post('contact-us.html', 'HomepageController@handleContactUs')->name('site.contact-us');

        Route::get('{slug}.html', 'HomepageController@showPage')->name('site.page');

        Route::get('/', 'HomepageController@index')->name('site.home');
    });
});